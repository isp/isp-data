import mlflow
import hydra
import hydra
from omegaconf import DictConfig, OmegaConf
import subprocess
import uuid
import json


mlflow.set_experiment("SNDlib dataset")
@hydra.main(version_base="1.3", config_path=".", config_name="config")
def run_algorithm(cfg):
    print(cfg)
    output_filename = f"/tmp/{uuid.uuid4()}"
    with mlflow.start_run():
        subprocess.run(["/home/nhuin/src/isp-algorithm/build/rl/src/Greedy",
                       cfg["instance"],
                        "--maxNbIterations", str(cfg["maxNbIterations"]),
                        "--output", output_filename])
        mlflow.log_params(cfg)

        with open(output_filename) as file_descriptor:
            results = json.load(file_descriptor)
            mlflow.log_artifact(output_filename)
            mlflow.log_metric("runningTime", results["runningTime"])
            mlflow.log_metric("nbTopologies", len(results['topologies']))


if __name__ == "__main__":
    run_algorithm()

