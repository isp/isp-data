<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="1" />
    <node id="3" />
    <node id="6" />
    <node id="2" />
    <node id="4" />
    <node id="5" />
    <node id="7" />
    <edge source="0" target="1">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="0" target="3">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="0" target="6">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="1" target="0">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="1" target="2">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="1" target="3">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="3" target="0">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="3" target="1">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="3" target="6">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="6" target="0">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="6" target="3">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="2" target="1">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="2" target="4">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="2" target="5">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="4" target="2">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="4" target="7">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="5" target="2">
      <data key="d0">10.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">5.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="5" target="7">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="7" target="4">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="7" target="5">
      <data key="d0">1.0</data>
      <data key="d1">1000.0</data>
    </edge>
  </graph>
</graphml>
