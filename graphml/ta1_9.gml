<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="19" />
    <node id="2" />
    <node id="3" />
    <node id="11" />
    <node id="14" />
    <node id="16" />
    <node id="21" />
    <node id="22" />
    <node id="20" />
    <node id="9" />
    <node id="8" />
    <node id="13" />
    <node id="23" />
    <node id="1" />
    <node id="7" />
    <node id="4" />
    <node id="5" />
    <node id="10" />
    <node id="15" />
    <node id="0" />
    <node id="17" />
    <node id="18" />
    <node id="6" />
    <node id="12" />
    <edge source="19" target="2">
      <data key="d0">162160.0</data>
      <data key="d1">9590.0</data>
    </edge>
    <edge source="19" target="3">
      <data key="d0">280095.0</data>
      <data key="d1">9580.0</data>
    </edge>
    <edge source="19" target="11">
      <data key="d0">398029.0</data>
      <data key="d1">19227.0</data>
    </edge>
    <edge source="19" target="14">
      <data key="d0">987702.0</data>
      <data key="d1">16336.0</data>
    </edge>
    <edge source="19" target="16">
      <data key="d0">88450.9</data>
      <data key="d1">11336.0</data>
    </edge>
    <edge source="19" target="21">
      <data key="d0">1105640.0</data>
      <data key="d1">12133.0</data>
    </edge>
    <edge source="19" target="22">
      <data key="d0">619156.0</data>
      <data key="d1">11549.0</data>
    </edge>
    <edge source="19" target="0">
      <data key="d0">427513.0</data>
      <data key="d1">9806.0</data>
    </edge>
    <edge source="19" target="7">
      <data key="d0">928735.0</data>
      <data key="d1">7159.0</data>
    </edge>
    <edge source="19" target="8">
      <data key="d0">1076150.0</data>
      <data key="d1">6248.0</data>
    </edge>
    <edge source="2" target="19">
      <data key="d0">162160.0</data>
      <data key="d1">9590.0</data>
    </edge>
    <edge source="2" target="13">
      <data key="d0">545447.0</data>
      <data key="d1">9426.0</data>
    </edge>
    <edge source="2" target="14">
      <data key="d0">840284.0</data>
      <data key="d1">13866.0</data>
    </edge>
    <edge source="2" target="4">
      <data key="d0">398029.0</data>
      <data key="d1">5415.0</data>
    </edge>
    <edge source="3" target="19">
      <data key="d0">280095.0</data>
      <data key="d1">9580.0</data>
    </edge>
    <edge source="3" target="7">
      <data key="d0">648640.0</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="3" target="11">
      <data key="d0">117935.0</data>
      <data key="d1">18694.0</data>
    </edge>
    <edge source="11" target="19">
      <data key="d0">398029.0</data>
      <data key="d1">19227.0</data>
    </edge>
    <edge source="11" target="1">
      <data key="d0">796058.0</data>
      <data key="d1">15048.0</data>
    </edge>
    <edge source="11" target="21">
      <data key="d0">751833.0</data>
      <data key="d1">9361.0</data>
    </edge>
    <edge source="11" target="4">
      <data key="d0">855026.0</data>
      <data key="d1">11673.0</data>
    </edge>
    <edge source="11" target="3">
      <data key="d0">117935.0</data>
      <data key="d1">18694.0</data>
    </edge>
    <edge source="11" target="12">
      <data key="d0">442255.0</data>
      <data key="d1">7762.0</data>
    </edge>
    <edge source="14" target="19">
      <data key="d0">987702.0</data>
      <data key="d1">16336.0</data>
    </edge>
    <edge source="14" target="1">
      <data key="d0">884509.0</data>
      <data key="d1">22074.0</data>
    </edge>
    <edge source="14" target="2">
      <data key="d0">840284.0</data>
      <data key="d1">13866.0</data>
    </edge>
    <edge source="14" target="4">
      <data key="d0">456996.0</data>
      <data key="d1">10959.0</data>
    </edge>
    <edge source="14" target="5">
      <data key="d0">250611.0</data>
      <data key="d1">8516.0</data>
    </edge>
    <edge source="14" target="9">
      <data key="d0">456996.0</data>
      <data key="d1">16273.0</data>
    </edge>
    <edge source="14" target="10">
      <data key="d0">368546.0</data>
      <data key="d1">5894.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">29483.6</data>
      <data key="d1">12468.0</data>
    </edge>
    <edge source="14" target="18">
      <data key="d0">486480.0</data>
      <data key="d1">21471.0</data>
    </edge>
    <edge source="14" target="17">
      <data key="d0">235869.0</data>
      <data key="d1">6745.0</data>
    </edge>
    <edge source="14" target="22">
      <data key="d0">1606860.0</data>
      <data key="d1">13280.0</data>
    </edge>
    <edge source="16" target="19">
      <data key="d0">88450.9</data>
      <data key="d1">11336.0</data>
    </edge>
    <edge source="16" target="0">
      <data key="d0">353804.0</data>
      <data key="d1">11666.0</data>
    </edge>
    <edge source="16" target="22">
      <data key="d0">530706.0</data>
      <data key="d1">7963.0</data>
    </edge>
    <edge source="21" target="19">
      <data key="d0">1105640.0</data>
      <data key="d1">12133.0</data>
    </edge>
    <edge source="21" target="7">
      <data key="d0">191644.0</data>
      <data key="d1">6316.0</data>
    </edge>
    <edge source="21" target="11">
      <data key="d0">751833.0</data>
      <data key="d1">9361.0</data>
    </edge>
    <edge source="22" target="19">
      <data key="d0">619156.0</data>
      <data key="d1">11549.0</data>
    </edge>
    <edge source="22" target="8">
      <data key="d0">456996.0</data>
      <data key="d1">12057.0</data>
    </edge>
    <edge source="22" target="13">
      <data key="d0">280095.0</data>
      <data key="d1">8102.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">117935.0</data>
      <data key="d1">6946.0</data>
    </edge>
    <edge source="22" target="14">
      <data key="d0">1606860.0</data>
      <data key="d1">13280.0</data>
    </edge>
    <edge source="22" target="16">
      <data key="d0">530706.0</data>
      <data key="d1">7963.0</data>
    </edge>
    <edge source="20" target="9">
      <data key="d0">309578.0</data>
      <data key="d1">7201.0</data>
    </edge>
    <edge source="20" target="17">
      <data key="d0">280095.0</data>
      <data key="d1">3581.0</data>
    </edge>
    <edge source="9" target="20">
      <data key="d0">309578.0</data>
      <data key="d1">7201.0</data>
    </edge>
    <edge source="9" target="14">
      <data key="d0">456996.0</data>
      <data key="d1">16273.0</data>
    </edge>
    <edge source="9" target="15">
      <data key="d0">427513.0</data>
      <data key="d1">17541.0</data>
    </edge>
    <edge source="9" target="6">
      <data key="d0">221127.0</data>
      <data key="d1">21932.0</data>
    </edge>
    <edge source="8" target="22">
      <data key="d0">456996.0</data>
      <data key="d1">12057.0</data>
    </edge>
    <edge source="8" target="0">
      <data key="d0">943476.0</data>
      <data key="d1">5200.0</data>
    </edge>
    <edge source="8" target="19">
      <data key="d0">1076150.0</data>
      <data key="d1">6248.0</data>
    </edge>
    <edge source="13" target="22">
      <data key="d0">280095.0</data>
      <data key="d1">8102.0</data>
    </edge>
    <edge source="13" target="23">
      <data key="d0">176902.0</data>
      <data key="d1">4254.0</data>
    </edge>
    <edge source="13" target="2">
      <data key="d0">545447.0</data>
      <data key="d1">9426.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">117935.0</data>
      <data key="d1">6946.0</data>
    </edge>
    <edge source="23" target="13">
      <data key="d0">176902.0</data>
      <data key="d1">4254.0</data>
    </edge>
    <edge source="1" target="11">
      <data key="d0">796058.0</data>
      <data key="d1">15048.0</data>
    </edge>
    <edge source="1" target="14">
      <data key="d0">884509.0</data>
      <data key="d1">22074.0</data>
    </edge>
    <edge source="1" target="18">
      <data key="d0">398029.0</data>
      <data key="d1">5590.0</data>
    </edge>
    <edge source="1" target="4">
      <data key="d0">781316.0</data>
      <data key="d1">15121.0</data>
    </edge>
    <edge source="1" target="12">
      <data key="d0">368546.0</data>
      <data key="d1">8154.0</data>
    </edge>
    <edge source="7" target="3">
      <data key="d0">648640.0</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="7" target="21">
      <data key="d0">191644.0</data>
      <data key="d1">6316.0</data>
    </edge>
    <edge source="7" target="19">
      <data key="d0">928735.0</data>
      <data key="d1">7159.0</data>
    </edge>
    <edge source="4" target="2">
      <data key="d0">398029.0</data>
      <data key="d1">5415.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">206385.0</data>
      <data key="d1">3625.0</data>
    </edge>
    <edge source="4" target="14">
      <data key="d0">456996.0</data>
      <data key="d1">10959.0</data>
    </edge>
    <edge source="4" target="1">
      <data key="d0">781316.0</data>
      <data key="d1">15121.0</data>
    </edge>
    <edge source="4" target="11">
      <data key="d0">855026.0</data>
      <data key="d1">11673.0</data>
    </edge>
    <edge source="4" target="12">
      <data key="d0">427513.0</data>
      <data key="d1">8683.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">206385.0</data>
      <data key="d1">3625.0</data>
    </edge>
    <edge source="5" target="14">
      <data key="d0">250611.0</data>
      <data key="d1">8516.0</data>
    </edge>
    <edge source="10" target="14">
      <data key="d0">368546.0</data>
      <data key="d1">5894.0</data>
    </edge>
    <edge source="10" target="17">
      <data key="d0">280095.0</data>
      <data key="d1">3396.0</data>
    </edge>
    <edge source="10" target="15">
      <data key="d0">339062.0</data>
      <data key="d1">7001.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">29483.6</data>
      <data key="d1">12468.0</data>
    </edge>
    <edge source="15" target="10">
      <data key="d0">339062.0</data>
      <data key="d1">7001.0</data>
    </edge>
    <edge source="15" target="17">
      <data key="d0">221127.0</data>
      <data key="d1">9213.0</data>
    </edge>
    <edge source="15" target="9">
      <data key="d0">427513.0</data>
      <data key="d1">17541.0</data>
    </edge>
    <edge source="15" target="18">
      <data key="d0">471738.0</data>
      <data key="d1">12619.0</data>
    </edge>
    <edge source="15" target="6">
      <data key="d0">206385.0</data>
      <data key="d1">10502.0</data>
    </edge>
    <edge source="0" target="16">
      <data key="d0">353804.0</data>
      <data key="d1">11666.0</data>
    </edge>
    <edge source="0" target="8">
      <data key="d0">943476.0</data>
      <data key="d1">5200.0</data>
    </edge>
    <edge source="0" target="19">
      <data key="d0">427513.0</data>
      <data key="d1">9806.0</data>
    </edge>
    <edge source="17" target="10">
      <data key="d0">280095.0</data>
      <data key="d1">3396.0</data>
    </edge>
    <edge source="17" target="20">
      <data key="d0">280095.0</data>
      <data key="d1">3581.0</data>
    </edge>
    <edge source="17" target="14">
      <data key="d0">235869.0</data>
      <data key="d1">6745.0</data>
    </edge>
    <edge source="17" target="15">
      <data key="d0">221127.0</data>
      <data key="d1">9213.0</data>
    </edge>
    <edge source="18" target="1">
      <data key="d0">398029.0</data>
      <data key="d1">5590.0</data>
    </edge>
    <edge source="18" target="14">
      <data key="d0">486480.0</data>
      <data key="d1">21471.0</data>
    </edge>
    <edge source="18" target="15">
      <data key="d0">471738.0</data>
      <data key="d1">12619.0</data>
    </edge>
    <edge source="18" target="6">
      <data key="d0">265353.0</data>
      <data key="d1">14860.0</data>
    </edge>
    <edge source="6" target="18">
      <data key="d0">265353.0</data>
      <data key="d1">14860.0</data>
    </edge>
    <edge source="6" target="15">
      <data key="d0">206385.0</data>
      <data key="d1">10502.0</data>
    </edge>
    <edge source="6" target="9">
      <data key="d0">221127.0</data>
      <data key="d1">21932.0</data>
    </edge>
    <edge source="12" target="11">
      <data key="d0">442255.0</data>
      <data key="d1">7762.0</data>
    </edge>
    <edge source="12" target="4">
      <data key="d0">427513.0</data>
      <data key="d1">8683.0</data>
    </edge>
    <edge source="12" target="1">
      <data key="d0">368546.0</data>
      <data key="d1">8154.0</data>
    </edge>
  </graph>
</graphml>
