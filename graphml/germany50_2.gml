<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="12" />
    <node id="14" />
    <node id="10" />
    <node id="48" />
    <node id="29" />
    <node id="0" />
    <node id="28" />
    <node id="35" />
    <node id="44" />
    <node id="25" />
    <node id="46" />
    <node id="4" />
    <node id="39" />
    <node id="16" />
    <node id="23" />
    <node id="19" />
    <node id="38" />
    <node id="36" />
    <node id="31" />
    <node id="3" />
    <node id="11" />
    <node id="43" />
    <node id="32" />
    <node id="20" />
    <node id="13" />
    <node id="2" />
    <node id="8" />
    <node id="49" />
    <node id="21" />
    <node id="27" />
    <node id="5" />
    <node id="22" />
    <node id="6" />
    <node id="7" />
    <node id="15" />
    <node id="18" />
    <node id="9" />
    <node id="33" />
    <node id="24" />
    <node id="42" />
    <node id="45" />
    <node id="47" />
    <node id="30" />
    <node id="17" />
    <node id="1" />
    <node id="26" />
    <node id="34" />
    <node id="40" />
    <node id="37" />
    <node id="41" />
    <edge source="12" target="14">
      <data key="d0">3290.0</data>
      <data key="d1">33.0</data>
    </edge>
    <edge source="12" target="29">
      <data key="d0">3290.0</data>
      <data key="d1">33.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">3290.0</data>
      <data key="d1">33.0</data>
    </edge>
    <edge source="14" target="10">
      <data key="d0">3290.0</data>
      <data key="d1">43.0</data>
    </edge>
    <edge source="14" target="48">
      <data key="d0">3290.0</data>
      <data key="d1">65.0</data>
    </edge>
    <edge source="10" target="14">
      <data key="d0">3290.0</data>
      <data key="d1">43.0</data>
    </edge>
    <edge source="10" target="35">
      <data key="d0">3290.0</data>
      <data key="d1">48.0</data>
    </edge>
    <edge source="10" target="44">
      <data key="d0">3720.0</data>
      <data key="d1">83.0</data>
    </edge>
    <edge source="10" target="25">
      <data key="d0">3720.0</data>
      <data key="d1">207.0</data>
    </edge>
    <edge source="48" target="14">
      <data key="d0">3290.0</data>
      <data key="d1">65.0</data>
    </edge>
    <edge source="48" target="0">
      <data key="d0">3720.0</data>
      <data key="d1">71.0</data>
    </edge>
    <edge source="48" target="38">
      <data key="d0">4150.0</data>
      <data key="d1">252.0</data>
    </edge>
    <edge source="48" target="36">
      <data key="d0">4150.0</data>
      <data key="d1">236.0</data>
    </edge>
    <edge source="29" target="12">
      <data key="d0">3290.0</data>
      <data key="d1">33.0</data>
    </edge>
    <edge source="29" target="0">
      <data key="d0">3290.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="29" target="28">
      <data key="d0">3290.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="0" target="29">
      <data key="d0">3290.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="0" target="48">
      <data key="d0">3720.0</data>
      <data key="d1">71.0</data>
    </edge>
    <edge source="0" target="46">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
    <edge source="28" target="29">
      <data key="d0">3290.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="28" target="44">
      <data key="d0">3290.0</data>
      <data key="d1">72.0</data>
    </edge>
    <edge source="28" target="16">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="28" target="23">
      <data key="d0">3720.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="28" target="46">
      <data key="d0">3720.0</data>
      <data key="d1">106.0</data>
    </edge>
    <edge source="35" target="10">
      <data key="d0">3290.0</data>
      <data key="d1">48.0</data>
    </edge>
    <edge source="35" target="4">
      <data key="d0">3290.0</data>
      <data key="d1">90.0</data>
    </edge>
    <edge source="35" target="39">
      <data key="d0">3290.0</data>
      <data key="d1">53.0</data>
    </edge>
    <edge source="44" target="10">
      <data key="d0">3720.0</data>
      <data key="d1">83.0</data>
    </edge>
    <edge source="44" target="28">
      <data key="d0">3290.0</data>
      <data key="d1">72.0</data>
    </edge>
    <edge source="44" target="4">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="44" target="19">
      <data key="d0">3290.0</data>
      <data key="d1">72.0</data>
    </edge>
    <edge source="25" target="10">
      <data key="d0">3720.0</data>
      <data key="d1">207.0</data>
    </edge>
    <edge source="25" target="13">
      <data key="d0">3720.0</data>
      <data key="d1">157.0</data>
    </edge>
    <edge source="25" target="5">
      <data key="d0">3720.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="25" target="19">
      <data key="d0">3720.0</data>
      <data key="d1">113.0</data>
    </edge>
    <edge source="25" target="18">
      <data key="d0">3720.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="46" target="0">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
    <edge source="46" target="28">
      <data key="d0">3720.0</data>
      <data key="d1">106.0</data>
    </edge>
    <edge source="46" target="42">
      <data key="d0">3290.0</data>
      <data key="d1">63.0</data>
    </edge>
    <edge source="4" target="35">
      <data key="d0">3290.0</data>
      <data key="d1">90.0</data>
    </edge>
    <edge source="4" target="44">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="4" target="22">
      <data key="d0">3720.0</data>
      <data key="d1">127.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">3720.0</data>
      <data key="d1">206.0</data>
    </edge>
    <edge source="39" target="35">
      <data key="d0">3290.0</data>
      <data key="d1">53.0</data>
    </edge>
    <edge source="39" target="38">
      <data key="d0">3720.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="39" target="22">
      <data key="d0">3720.0</data>
      <data key="d1">169.0</data>
    </edge>
    <edge source="16" target="28">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="16" target="9">
      <data key="d0">3290.0</data>
      <data key="d1">24.0</data>
    </edge>
    <edge source="16" target="19">
      <data key="d0">3290.0</data>
      <data key="d1">45.0</data>
    </edge>
    <edge source="16" target="18">
      <data key="d0">3720.0</data>
      <data key="d1">107.0</data>
    </edge>
    <edge source="23" target="28">
      <data key="d0">3720.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="23" target="9">
      <data key="d0">3720.0</data>
      <data key="d1">101.0</data>
    </edge>
    <edge source="23" target="42">
      <data key="d0">3290.0</data>
      <data key="d1">75.0</data>
    </edge>
    <edge source="23" target="24">
      <data key="d0">3290.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="19" target="44">
      <data key="d0">3290.0</data>
      <data key="d1">72.0</data>
    </edge>
    <edge source="19" target="25">
      <data key="d0">3720.0</data>
      <data key="d1">113.0</data>
    </edge>
    <edge source="19" target="16">
      <data key="d0">3290.0</data>
      <data key="d1">45.0</data>
    </edge>
    <edge source="19" target="18">
      <data key="d0">3290.0</data>
      <data key="d1">102.0</data>
    </edge>
    <edge source="38" target="48">
      <data key="d0">4150.0</data>
      <data key="d1">252.0</data>
    </edge>
    <edge source="38" target="6">
      <data key="d0">3290.0</data>
      <data key="d1">64.0</data>
    </edge>
    <edge source="38" target="36">
      <data key="d0">3720.0</data>
      <data key="d1">111.0</data>
    </edge>
    <edge source="38" target="39">
      <data key="d0">3720.0</data>
      <data key="d1">85.0</data>
    </edge>
    <edge source="36" target="48">
      <data key="d0">4150.0</data>
      <data key="d1">236.0</data>
    </edge>
    <edge source="36" target="38">
      <data key="d0">3720.0</data>
      <data key="d1">111.0</data>
    </edge>
    <edge source="31" target="3">
      <data key="d0">3720.0</data>
      <data key="d1">155.0</data>
    </edge>
    <edge source="31" target="11">
      <data key="d0">3720.0</data>
      <data key="d1">139.0</data>
    </edge>
    <edge source="31" target="13">
      <data key="d0">3720.0</data>
      <data key="d1">139.0</data>
    </edge>
    <edge source="31" target="32">
      <data key="d0">3720.0</data>
      <data key="d1">109.0</data>
    </edge>
    <edge source="31" target="2">
      <data key="d0">4150.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="3" target="31">
      <data key="d0">3720.0</data>
      <data key="d1">155.0</data>
    </edge>
    <edge source="3" target="11">
      <data key="d0">4150.0</data>
      <data key="d1">153.0</data>
    </edge>
    <edge source="3" target="43">
      <data key="d0">4150.0</data>
      <data key="d1">220.0</data>
    </edge>
    <edge source="3" target="32">
      <data key="d0">3720.0</data>
      <data key="d1">179.0</data>
    </edge>
    <edge source="3" target="20">
      <data key="d0">4150.0</data>
      <data key="d1">157.0</data>
    </edge>
    <edge source="11" target="3">
      <data key="d0">4150.0</data>
      <data key="d1">153.0</data>
    </edge>
    <edge source="11" target="31">
      <data key="d0">3720.0</data>
      <data key="d1">139.0</data>
    </edge>
    <edge source="11" target="13">
      <data key="d0">4150.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="11" target="8">
      <data key="d0">3290.0</data>
      <data key="d1">82.0</data>
    </edge>
    <edge source="43" target="3">
      <data key="d0">4150.0</data>
      <data key="d1">220.0</data>
    </edge>
    <edge source="43" target="32">
      <data key="d0">4150.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="43" target="20">
      <data key="d0">3720.0</data>
      <data key="d1">202.0</data>
    </edge>
    <edge source="43" target="21">
      <data key="d0">3720.0</data>
      <data key="d1">146.0</data>
    </edge>
    <edge source="43" target="27">
      <data key="d0">3720.0</data>
      <data key="d1">155.0</data>
    </edge>
    <edge source="32" target="3">
      <data key="d0">3720.0</data>
      <data key="d1">179.0</data>
    </edge>
    <edge source="32" target="31">
      <data key="d0">3720.0</data>
      <data key="d1">109.0</data>
    </edge>
    <edge source="32" target="43">
      <data key="d0">4150.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="32" target="5">
      <data key="d0">3290.0</data>
      <data key="d1">110.0</data>
    </edge>
    <edge source="20" target="3">
      <data key="d0">4150.0</data>
      <data key="d1">157.0</data>
    </edge>
    <edge source="20" target="43">
      <data key="d0">3720.0</data>
      <data key="d1">202.0</data>
    </edge>
    <edge source="13" target="31">
      <data key="d0">3720.0</data>
      <data key="d1">139.0</data>
    </edge>
    <edge source="13" target="11">
      <data key="d0">4150.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="13" target="8">
      <data key="d0">3720.0</data>
      <data key="d1">190.0</data>
    </edge>
    <edge source="13" target="25">
      <data key="d0">3720.0</data>
      <data key="d1">157.0</data>
    </edge>
    <edge source="13" target="49">
      <data key="d0">3720.0</data>
      <data key="d1">161.0</data>
    </edge>
    <edge source="2" target="31">
      <data key="d0">4150.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="2" target="8">
      <data key="d0">3720.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="2" target="37">
      <data key="d0">3290.0</data>
      <data key="d1">67.0</data>
    </edge>
    <edge source="8" target="11">
      <data key="d0">3290.0</data>
      <data key="d1">82.0</data>
    </edge>
    <edge source="8" target="13">
      <data key="d0">3720.0</data>
      <data key="d1">190.0</data>
    </edge>
    <edge source="8" target="2">
      <data key="d0">3720.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="49" target="13">
      <data key="d0">3720.0</data>
      <data key="d1">161.0</data>
    </edge>
    <edge source="49" target="18">
      <data key="d0">3720.0</data>
      <data key="d1">83.0</data>
    </edge>
    <edge source="49" target="45">
      <data key="d0">3720.0</data>
      <data key="d1">136.0</data>
    </edge>
    <edge source="49" target="1">
      <data key="d0">4150.0</data>
      <data key="d1">172.0</data>
    </edge>
    <edge source="49" target="37">
      <data key="d0">3720.0</data>
      <data key="d1">108.0</data>
    </edge>
    <edge source="21" target="43">
      <data key="d0">3720.0</data>
      <data key="d1">146.0</data>
    </edge>
    <edge source="21" target="27">
      <data key="d0">3720.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="21" target="22">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="21" target="5">
      <data key="d0">3720.0</data>
      <data key="d1">141.0</data>
    </edge>
    <edge source="27" target="43">
      <data key="d0">3720.0</data>
      <data key="d1">155.0</data>
    </edge>
    <edge source="27" target="21">
      <data key="d0">3720.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="27" target="15">
      <data key="d0">3290.0</data>
      <data key="d1">80.0</data>
    </edge>
    <edge source="5" target="32">
      <data key="d0">3290.0</data>
      <data key="d1">110.0</data>
    </edge>
    <edge source="5" target="21">
      <data key="d0">3720.0</data>
      <data key="d1">141.0</data>
    </edge>
    <edge source="5" target="22">
      <data key="d0">3720.0</data>
      <data key="d1">84.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">3720.0</data>
      <data key="d1">206.0</data>
    </edge>
    <edge source="5" target="25">
      <data key="d0">3720.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="22" target="21">
      <data key="d0">3720.0</data>
      <data key="d1">122.0</data>
    </edge>
    <edge source="22" target="6">
      <data key="d0">3290.0</data>
      <data key="d1">114.0</data>
    </edge>
    <edge source="22" target="4">
      <data key="d0">3720.0</data>
      <data key="d1">127.0</data>
    </edge>
    <edge source="22" target="5">
      <data key="d0">3720.0</data>
      <data key="d1">84.0</data>
    </edge>
    <edge source="22" target="39">
      <data key="d0">3720.0</data>
      <data key="d1">169.0</data>
    </edge>
    <edge source="6" target="38">
      <data key="d0">3290.0</data>
      <data key="d1">64.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">3290.0</data>
      <data key="d1">51.0</data>
    </edge>
    <edge source="6" target="22">
      <data key="d0">3290.0</data>
      <data key="d1">114.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">3290.0</data>
      <data key="d1">51.0</data>
    </edge>
    <edge source="7" target="15">
      <data key="d0">3720.0</data>
      <data key="d1">151.0</data>
    </edge>
    <edge source="15" target="27">
      <data key="d0">3290.0</data>
      <data key="d1">80.0</data>
    </edge>
    <edge source="15" target="7">
      <data key="d0">3720.0</data>
      <data key="d1">151.0</data>
    </edge>
    <edge source="18" target="25">
      <data key="d0">3720.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="18" target="16">
      <data key="d0">3720.0</data>
      <data key="d1">107.0</data>
    </edge>
    <edge source="18" target="19">
      <data key="d0">3290.0</data>
      <data key="d1">102.0</data>
    </edge>
    <edge source="18" target="49">
      <data key="d0">3720.0</data>
      <data key="d1">83.0</data>
    </edge>
    <edge source="9" target="16">
      <data key="d0">3290.0</data>
      <data key="d1">24.0</data>
    </edge>
    <edge source="9" target="33">
      <data key="d0">3290.0</data>
      <data key="d1">43.0</data>
    </edge>
    <edge source="9" target="23">
      <data key="d0">3720.0</data>
      <data key="d1">101.0</data>
    </edge>
    <edge source="33" target="9">
      <data key="d0">3290.0</data>
      <data key="d1">43.0</data>
    </edge>
    <edge source="33" target="24">
      <data key="d0">3290.0</data>
      <data key="d1">49.0</data>
    </edge>
    <edge source="24" target="33">
      <data key="d0">3290.0</data>
      <data key="d1">49.0</data>
    </edge>
    <edge source="24" target="23">
      <data key="d0">3290.0</data>
      <data key="d1">78.0</data>
    </edge>
    <edge source="24" target="42">
      <data key="d0">3720.0</data>
      <data key="d1">140.0</data>
    </edge>
    <edge source="24" target="45">
      <data key="d0">3290.0</data>
      <data key="d1">74.0</data>
    </edge>
    <edge source="24" target="17">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
    <edge source="42" target="23">
      <data key="d0">3290.0</data>
      <data key="d1">75.0</data>
    </edge>
    <edge source="42" target="46">
      <data key="d0">3290.0</data>
      <data key="d1">63.0</data>
    </edge>
    <edge source="42" target="24">
      <data key="d0">3720.0</data>
      <data key="d1">140.0</data>
    </edge>
    <edge source="45" target="24">
      <data key="d0">3290.0</data>
      <data key="d1">74.0</data>
    </edge>
    <edge source="45" target="47">
      <data key="d0">3290.0</data>
      <data key="d1">95.0</data>
    </edge>
    <edge source="45" target="30">
      <data key="d0">3720.0</data>
      <data key="d1">108.0</data>
    </edge>
    <edge source="45" target="49">
      <data key="d0">3720.0</data>
      <data key="d1">136.0</data>
    </edge>
    <edge source="47" target="45">
      <data key="d0">3290.0</data>
      <data key="d1">95.0</data>
    </edge>
    <edge source="47" target="1">
      <data key="d0">3290.0</data>
      <data key="d1">91.0</data>
    </edge>
    <edge source="30" target="45">
      <data key="d0">3720.0</data>
      <data key="d1">108.0</data>
    </edge>
    <edge source="30" target="17">
      <data key="d0">3720.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="30" target="26">
      <data key="d0">3720.0</data>
      <data key="d1">114.0</data>
    </edge>
    <edge source="17" target="24">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
    <edge source="17" target="30">
      <data key="d0">3720.0</data>
      <data key="d1">142.0</data>
    </edge>
    <edge source="1" target="47">
      <data key="d0">3290.0</data>
      <data key="d1">91.0</data>
    </edge>
    <edge source="1" target="34">
      <data key="d0">3290.0</data>
      <data key="d1">69.0</data>
    </edge>
    <edge source="1" target="49">
      <data key="d0">4150.0</data>
      <data key="d1">172.0</data>
    </edge>
    <edge source="26" target="30">
      <data key="d0">3720.0</data>
      <data key="d1">114.0</data>
    </edge>
    <edge source="26" target="34">
      <data key="d0">3720.0</data>
      <data key="d1">132.0</data>
    </edge>
    <edge source="34" target="1">
      <data key="d0">3290.0</data>
      <data key="d1">69.0</data>
    </edge>
    <edge source="34" target="26">
      <data key="d0">3720.0</data>
      <data key="d1">132.0</data>
    </edge>
    <edge source="34" target="40">
      <data key="d0">3720.0</data>
      <data key="d1">194.0</data>
    </edge>
    <edge source="34" target="37">
      <data key="d0">3720.0</data>
      <data key="d1">152.0</data>
    </edge>
    <edge source="34" target="41">
      <data key="d0">3720.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="40" target="34">
      <data key="d0">3720.0</data>
      <data key="d1">194.0</data>
    </edge>
    <edge source="40" target="41">
      <data key="d0">3720.0</data>
      <data key="d1">144.0</data>
    </edge>
    <edge source="37" target="34">
      <data key="d0">3720.0</data>
      <data key="d1">152.0</data>
    </edge>
    <edge source="37" target="2">
      <data key="d0">3290.0</data>
      <data key="d1">67.0</data>
    </edge>
    <edge source="37" target="49">
      <data key="d0">3720.0</data>
      <data key="d1">108.0</data>
    </edge>
    <edge source="37" target="41">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
    <edge source="41" target="34">
      <data key="d0">3720.0</data>
      <data key="d1">100.0</data>
    </edge>
    <edge source="41" target="40">
      <data key="d0">3720.0</data>
      <data key="d1">144.0</data>
    </edge>
    <edge source="41" target="37">
      <data key="d0">3720.0</data>
      <data key="d1">120.0</data>
    </edge>
  </graph>
</graphml>
