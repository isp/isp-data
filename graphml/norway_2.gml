<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="1" />
    <node id="2" />
    <node id="3" />
    <node id="4" />
    <node id="5" />
    <node id="6" />
    <node id="7" />
    <node id="8" />
    <node id="9" />
    <node id="11" />
    <node id="10" />
    <node id="25" />
    <node id="24" />
    <node id="22" />
    <node id="23" />
    <node id="21" />
    <node id="26" />
    <node id="20" />
    <node id="19" />
    <node id="18" />
    <node id="17" />
    <node id="15" />
    <node id="14" />
    <node id="12" />
    <node id="16" />
    <node id="13" />
    <edge source="0" target="1">
      <data key="d0">25550.0</data>
      <data key="d1">15672.0</data>
    </edge>
    <edge source="0" target="20">
      <data key="d0">21200.0</data>
      <data key="d1">12440.0</data>
    </edge>
    <edge source="0" target="19">
      <data key="d0">27050.0</data>
      <data key="d1">14058.0</data>
    </edge>
    <edge source="1" target="0">
      <data key="d0">25550.0</data>
      <data key="d1">15672.0</data>
    </edge>
    <edge source="1" target="2">
      <data key="d0">18340.0</data>
      <data key="d1">18976.0</data>
    </edge>
    <edge source="1" target="19">
      <data key="d0">27590.0</data>
      <data key="d1">11545.0</data>
    </edge>
    <edge source="2" target="1">
      <data key="d0">18340.0</data>
      <data key="d1">18976.0</data>
    </edge>
    <edge source="2" target="3">
      <data key="d0">28000.0</data>
      <data key="d1">15462.0</data>
    </edge>
    <edge source="3" target="2">
      <data key="d0">28000.0</data>
      <data key="d1">15462.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">33580.0</data>
      <data key="d1">20890.0</data>
    </edge>
    <edge source="3" target="18">
      <data key="d0">45540.0</data>
      <data key="d1">24452.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">33580.0</data>
      <data key="d1">20890.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">14810.0</data>
      <data key="d1">19504.0</data>
    </edge>
    <edge source="4" target="17">
      <data key="d0">32760.0</data>
      <data key="d1">15164.0</data>
    </edge>
    <edge source="4" target="13">
      <data key="d0">25690.0</data>
      <data key="d1">16407.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">14810.0</data>
      <data key="d1">19504.0</data>
    </edge>
    <edge source="5" target="6">
      <data key="d0">6920.0</data>
      <data key="d1">13777.0</data>
    </edge>
    <edge source="5" target="13">
      <data key="d0">19840.0</data>
      <data key="d1">13427.0</data>
    </edge>
    <edge source="6" target="5">
      <data key="d0">6920.0</data>
      <data key="d1">13777.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">33440.0</data>
      <data key="d1">16729.0</data>
    </edge>
    <edge source="6" target="13">
      <data key="d0">20660.0</data>
      <data key="d1">18432.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">33440.0</data>
      <data key="d1">16729.0</data>
    </edge>
    <edge source="7" target="8">
      <data key="d0">9500.0</data>
      <data key="d1">11834.0</data>
    </edge>
    <edge source="8" target="7">
      <data key="d0">9500.0</data>
      <data key="d1">11834.0</data>
    </edge>
    <edge source="8" target="9">
      <data key="d0">17800.0</data>
      <data key="d1">15728.0</data>
    </edge>
    <edge source="8" target="11">
      <data key="d0">16300.0</data>
      <data key="d1">8684.0</data>
    </edge>
    <edge source="9" target="8">
      <data key="d0">17800.0</data>
      <data key="d1">15728.0</data>
    </edge>
    <edge source="9" target="11">
      <data key="d0">4880.0</data>
      <data key="d1">11763.0</data>
    </edge>
    <edge source="9" target="10">
      <data key="d0">5830.0</data>
      <data key="d1">9964.0</data>
    </edge>
    <edge source="11" target="8">
      <data key="d0">16300.0</data>
      <data key="d1">8684.0</data>
    </edge>
    <edge source="11" target="9">
      <data key="d0">4880.0</data>
      <data key="d1">11763.0</data>
    </edge>
    <edge source="11" target="10">
      <data key="d0">10180.0</data>
      <data key="d1">18433.0</data>
    </edge>
    <edge source="11" target="12">
      <data key="d0">10460.0</data>
      <data key="d1">14492.0</data>
    </edge>
    <edge source="10" target="11">
      <data key="d0">10180.0</data>
      <data key="d1">18433.0</data>
    </edge>
    <edge source="10" target="9">
      <data key="d0">5830.0</data>
      <data key="d1">9964.0</data>
    </edge>
    <edge source="10" target="25">
      <data key="d0">11000.0</data>
      <data key="d1">8724.0</data>
    </edge>
    <edge source="10" target="14">
      <data key="d0">8140.0</data>
      <data key="d1">15792.0</data>
    </edge>
    <edge source="25" target="10">
      <data key="d0">11000.0</data>
      <data key="d1">8724.0</data>
    </edge>
    <edge source="25" target="24">
      <data key="d0">2430.0</data>
      <data key="d1">7931.0</data>
    </edge>
    <edge source="25" target="15">
      <data key="d0">7870.0</data>
      <data key="d1">22642.0</data>
    </edge>
    <edge source="24" target="25">
      <data key="d0">2430.0</data>
      <data key="d1">7931.0</data>
    </edge>
    <edge source="24" target="22">
      <data key="d0">17120.0</data>
      <data key="d1">9502.0</data>
    </edge>
    <edge source="24" target="23">
      <data key="d0">15080.0</data>
      <data key="d1">11336.0</data>
    </edge>
    <edge source="22" target="24">
      <data key="d0">17120.0</data>
      <data key="d1">9502.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">4200.0</data>
      <data key="d1">13399.0</data>
    </edge>
    <edge source="22" target="26">
      <data key="d0">11540.0</data>
      <data key="d1">17472.0</data>
    </edge>
    <edge source="22" target="21">
      <data key="d0">25420.0</data>
      <data key="d1">9166.0</data>
    </edge>
    <edge source="23" target="24">
      <data key="d0">15080.0</data>
      <data key="d1">11336.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">4200.0</data>
      <data key="d1">13399.0</data>
    </edge>
    <edge source="23" target="21">
      <data key="d0">29500.0</data>
      <data key="d1">19228.0</data>
    </edge>
    <edge source="23" target="18">
      <data key="d0">19300.0</data>
      <data key="d1">34440.0</data>
    </edge>
    <edge source="23" target="15">
      <data key="d0">25140.0</data>
      <data key="d1">14803.0</data>
    </edge>
    <edge source="23" target="26">
      <data key="d0">8010.0</data>
      <data key="d1">13200.0</data>
    </edge>
    <edge source="21" target="23">
      <data key="d0">29500.0</data>
      <data key="d1">19228.0</data>
    </edge>
    <edge source="21" target="22">
      <data key="d0">25420.0</data>
      <data key="d1">9166.0</data>
    </edge>
    <edge source="21" target="20">
      <data key="d0">7190.0</data>
      <data key="d1">13346.0</data>
    </edge>
    <edge source="21" target="19">
      <data key="d0">16850.0</data>
      <data key="d1">27496.0</data>
    </edge>
    <edge source="21" target="26">
      <data key="d0">29360.0</data>
      <data key="d1">16278.0</data>
    </edge>
    <edge source="26" target="22">
      <data key="d0">11540.0</data>
      <data key="d1">17472.0</data>
    </edge>
    <edge source="26" target="19">
      <data key="d0">21740.0</data>
      <data key="d1">18009.0</data>
    </edge>
    <edge source="26" target="21">
      <data key="d0">29360.0</data>
      <data key="d1">16278.0</data>
    </edge>
    <edge source="26" target="18">
      <data key="d0">15080.0</data>
      <data key="d1">23714.0</data>
    </edge>
    <edge source="26" target="23">
      <data key="d0">8010.0</data>
      <data key="d1">13200.0</data>
    </edge>
    <edge source="20" target="21">
      <data key="d0">7190.0</data>
      <data key="d1">13346.0</data>
    </edge>
    <edge source="20" target="0">
      <data key="d0">21200.0</data>
      <data key="d1">12440.0</data>
    </edge>
    <edge source="20" target="19">
      <data key="d0">24060.0</data>
      <data key="d1">20861.0</data>
    </edge>
    <edge source="19" target="20">
      <data key="d0">24060.0</data>
      <data key="d1">20861.0</data>
    </edge>
    <edge source="19" target="0">
      <data key="d0">27050.0</data>
      <data key="d1">14058.0</data>
    </edge>
    <edge source="19" target="21">
      <data key="d0">16850.0</data>
      <data key="d1">27496.0</data>
    </edge>
    <edge source="19" target="1">
      <data key="d0">27590.0</data>
      <data key="d1">11545.0</data>
    </edge>
    <edge source="19" target="26">
      <data key="d0">21740.0</data>
      <data key="d1">18009.0</data>
    </edge>
    <edge source="19" target="18">
      <data key="d0">26230.0</data>
      <data key="d1">14271.0</data>
    </edge>
    <edge source="18" target="19">
      <data key="d0">26230.0</data>
      <data key="d1">14271.0</data>
    </edge>
    <edge source="18" target="26">
      <data key="d0">15080.0</data>
      <data key="d1">23714.0</data>
    </edge>
    <edge source="18" target="3">
      <data key="d0">45540.0</data>
      <data key="d1">24452.0</data>
    </edge>
    <edge source="18" target="17">
      <data key="d0">14260.0</data>
      <data key="d1">16236.0</data>
    </edge>
    <edge source="18" target="15">
      <data key="d0">17660.0</data>
      <data key="d1">30865.0</data>
    </edge>
    <edge source="18" target="23">
      <data key="d0">19300.0</data>
      <data key="d1">34440.0</data>
    </edge>
    <edge source="17" target="18">
      <data key="d0">14260.0</data>
      <data key="d1">16236.0</data>
    </edge>
    <edge source="17" target="15">
      <data key="d0">25420.0</data>
      <data key="d1">34950.0</data>
    </edge>
    <edge source="17" target="16">
      <data key="d0">38200.0</data>
      <data key="d1">21374.0</data>
    </edge>
    <edge source="17" target="4">
      <data key="d0">32760.0</data>
      <data key="d1">15164.0</data>
    </edge>
    <edge source="15" target="18">
      <data key="d0">17660.0</data>
      <data key="d1">30865.0</data>
    </edge>
    <edge source="15" target="23">
      <data key="d0">25140.0</data>
      <data key="d1">14803.0</data>
    </edge>
    <edge source="15" target="25">
      <data key="d0">7870.0</data>
      <data key="d1">22642.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">10050.0</data>
      <data key="d1">5873.0</data>
    </edge>
    <edge source="15" target="16">
      <data key="d0">25420.0</data>
      <data key="d1">21000.0</data>
    </edge>
    <edge source="15" target="17">
      <data key="d0">25420.0</data>
      <data key="d1">34950.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">10050.0</data>
      <data key="d1">5873.0</data>
    </edge>
    <edge source="14" target="10">
      <data key="d0">8140.0</data>
      <data key="d1">15792.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">9780.0</data>
      <data key="d1">18376.0</data>
    </edge>
    <edge source="12" target="14">
      <data key="d0">9780.0</data>
      <data key="d1">18376.0</data>
    </edge>
    <edge source="12" target="16">
      <data key="d0">13180.0</data>
      <data key="d1">10555.0</data>
    </edge>
    <edge source="12" target="11">
      <data key="d0">10460.0</data>
      <data key="d1">14492.0</data>
    </edge>
    <edge source="16" target="15">
      <data key="d0">25420.0</data>
      <data key="d1">21000.0</data>
    </edge>
    <edge source="16" target="17">
      <data key="d0">38200.0</data>
      <data key="d1">21374.0</data>
    </edge>
    <edge source="16" target="13">
      <data key="d0">25280.0</data>
      <data key="d1">10226.0</data>
    </edge>
    <edge source="16" target="12">
      <data key="d0">13180.0</data>
      <data key="d1">10555.0</data>
    </edge>
    <edge source="13" target="16">
      <data key="d0">25280.0</data>
      <data key="d1">10226.0</data>
    </edge>
    <edge source="13" target="4">
      <data key="d0">25690.0</data>
      <data key="d1">16407.0</data>
    </edge>
    <edge source="13" target="5">
      <data key="d0">19840.0</data>
      <data key="d1">13427.0</data>
    </edge>
    <edge source="13" target="6">
      <data key="d0">20660.0</data>
      <data key="d1">18432.0</data>
    </edge>
  </graph>
</graphml>
