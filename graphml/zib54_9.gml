<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="22" />
    <node id="2" />
    <node id="20" />
    <node id="44" />
    <node id="5" />
    <node id="31" />
    <node id="39" />
    <node id="6" />
    <node id="25" />
    <node id="7" />
    <node id="37" />
    <node id="41" />
    <node id="9" />
    <node id="18" />
    <node id="10" />
    <node id="26" />
    <node id="36" />
    <node id="11" />
    <node id="49" />
    <node id="12" />
    <node id="4" />
    <node id="13" />
    <node id="3" />
    <node id="15" />
    <node id="14" />
    <node id="38" />
    <node id="52" />
    <node id="17" />
    <node id="48" />
    <node id="19" />
    <node id="42" />
    <node id="50" />
    <node id="21" />
    <node id="43" />
    <node id="24" />
    <node id="27" />
    <node id="23" />
    <node id="28" />
    <node id="29" />
    <node id="30" />
    <node id="46" />
    <node id="8" />
    <node id="32" />
    <node id="40" />
    <node id="33" />
    <node id="16" />
    <node id="34" />
    <node id="35" />
    <node id="47" />
    <node id="1" />
    <node id="45" />
    <node id="51" />
    <node id="53" />
    <edge source="0" target="22">
      <data key="d0">427513.0</data>
      <data key="d1">12700.0</data>
    </edge>
    <edge source="0" target="45">
      <data key="d0">73709.1</data>
      <data key="d1">6440.0</data>
    </edge>
    <edge source="22" target="0">
      <data key="d0">427513.0</data>
      <data key="d1">12700.0</data>
    </edge>
    <edge source="22" target="7">
      <data key="d0">353804.0</data>
      <data key="d1">22777.0</data>
    </edge>
    <edge source="22" target="24">
      <data key="d0">103193.0</data>
      <data key="d1">22178.0</data>
    </edge>
    <edge source="22" target="25">
      <data key="d0">103193.0</data>
      <data key="d1">26773.0</data>
    </edge>
    <edge source="22" target="26">
      <data key="d0">44225.5</data>
      <data key="d1">17367.0</data>
    </edge>
    <edge source="22" target="27">
      <data key="d0">73709.1</data>
      <data key="d1">10214.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">29483.6</data>
      <data key="d1">17608.0</data>
    </edge>
    <edge source="22" target="34">
      <data key="d0">221127.0</data>
      <data key="d1">16533.0</data>
    </edge>
    <edge source="22" target="40">
      <data key="d0">855026.0</data>
      <data key="d1">25003.0</data>
    </edge>
    <edge source="22" target="46">
      <data key="d0">692866.0</data>
      <data key="d1">30360.0</data>
    </edge>
    <edge source="2" target="20">
      <data key="d0">147418.0</data>
      <data key="d1">9791.0</data>
    </edge>
    <edge source="2" target="44">
      <data key="d0">147418.0</data>
      <data key="d1">10657.0</data>
    </edge>
    <edge source="20" target="2">
      <data key="d0">147418.0</data>
      <data key="d1">9791.0</data>
    </edge>
    <edge source="20" target="50">
      <data key="d0">117935.0</data>
      <data key="d1">4657.0</data>
    </edge>
    <edge source="20" target="51">
      <data key="d0">280095.0</data>
      <data key="d1">7495.0</data>
    </edge>
    <edge source="20" target="33">
      <data key="d0">250611.0</data>
      <data key="d1">3848.0</data>
    </edge>
    <edge source="44" target="2">
      <data key="d0">147418.0</data>
      <data key="d1">10657.0</data>
    </edge>
    <edge source="44" target="46">
      <data key="d0">280095.0</data>
      <data key="d1">21065.0</data>
    </edge>
    <edge source="5" target="31">
      <data key="d0">442255.0</data>
      <data key="d1">8999.0</data>
    </edge>
    <edge source="5" target="39">
      <data key="d0">633898.0</data>
      <data key="d1">6466.0</data>
    </edge>
    <edge source="31" target="5">
      <data key="d0">442255.0</data>
      <data key="d1">8999.0</data>
    </edge>
    <edge source="31" target="8">
      <data key="d0">206385.0</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="31" target="49">
      <data key="d0">486480.0</data>
      <data key="d1">9825.0</data>
    </edge>
    <edge source="31" target="39">
      <data key="d0">530706.0</data>
      <data key="d1">14396.0</data>
    </edge>
    <edge source="39" target="5">
      <data key="d0">633898.0</data>
      <data key="d1">6466.0</data>
    </edge>
    <edge source="39" target="15">
      <data key="d0">235869.0</data>
      <data key="d1">5215.0</data>
    </edge>
    <edge source="39" target="41">
      <data key="d0">530706.0</data>
      <data key="d1">7424.0</data>
    </edge>
    <edge source="39" target="37">
      <data key="d0">368546.0</data>
      <data key="d1">9302.0</data>
    </edge>
    <edge source="39" target="31">
      <data key="d0">530706.0</data>
      <data key="d1">14396.0</data>
    </edge>
    <edge source="6" target="25">
      <data key="d0">117935.0</data>
      <data key="d1">6329.0</data>
    </edge>
    <edge source="6" target="17">
      <data key="d0">58967.3</data>
      <data key="d1">3848.0</data>
    </edge>
    <edge source="25" target="6">
      <data key="d0">117935.0</data>
      <data key="d1">6329.0</data>
    </edge>
    <edge source="25" target="22">
      <data key="d0">103193.0</data>
      <data key="d1">26773.0</data>
    </edge>
    <edge source="25" target="24">
      <data key="d0">88450.9</data>
      <data key="d1">8402.0</data>
    </edge>
    <edge source="25" target="26">
      <data key="d0">117935.0</data>
      <data key="d1">18200.0</data>
    </edge>
    <edge source="25" target="28">
      <data key="d0">44225.5</data>
      <data key="d1">6601.0</data>
    </edge>
    <edge source="25" target="29">
      <data key="d0">265353.0</data>
      <data key="d1">17210.0</data>
    </edge>
    <edge source="25" target="35">
      <data key="d0">560189.0</data>
      <data key="d1">12256.0</data>
    </edge>
    <edge source="25" target="45">
      <data key="d0">221127.0</data>
      <data key="d1">8338.0</data>
    </edge>
    <edge source="25" target="48">
      <data key="d0">132676.0</data>
      <data key="d1">11896.0</data>
    </edge>
    <edge source="25" target="51">
      <data key="d0">398029.0</data>
      <data key="d1">25975.0</data>
    </edge>
    <edge source="7" target="22">
      <data key="d0">353804.0</data>
      <data key="d1">22777.0</data>
    </edge>
    <edge source="7" target="37">
      <data key="d0">530706.0</data>
      <data key="d1">10155.0</data>
    </edge>
    <edge source="7" target="41">
      <data key="d0">913993.0</data>
      <data key="d1">9519.0</data>
    </edge>
    <edge source="7" target="32">
      <data key="d0">383287.0</data>
      <data key="d1">4346.0</data>
    </edge>
    <edge source="37" target="7">
      <data key="d0">530706.0</data>
      <data key="d1">10155.0</data>
    </edge>
    <edge source="37" target="41">
      <data key="d0">383287.0</data>
      <data key="d1">4884.0</data>
    </edge>
    <edge source="37" target="39">
      <data key="d0">368546.0</data>
      <data key="d1">9302.0</data>
    </edge>
    <edge source="41" target="7">
      <data key="d0">913993.0</data>
      <data key="d1">9519.0</data>
    </edge>
    <edge source="41" target="37">
      <data key="d0">383287.0</data>
      <data key="d1">4884.0</data>
    </edge>
    <edge source="41" target="39">
      <data key="d0">530706.0</data>
      <data key="d1">7424.0</data>
    </edge>
    <edge source="9" target="18">
      <data key="d0">162160.0</data>
      <data key="d1">3636.0</data>
    </edge>
    <edge source="9" target="12">
      <data key="d0">633898.0</data>
      <data key="d1">5077.0</data>
    </edge>
    <edge source="9" target="35">
      <data key="d0">1297280.0</data>
      <data key="d1">5423.0</data>
    </edge>
    <edge source="9" target="53">
      <data key="d0">427513.0</data>
      <data key="d1">7529.0</data>
    </edge>
    <edge source="18" target="9">
      <data key="d0">162160.0</data>
      <data key="d1">3636.0</data>
    </edge>
    <edge source="18" target="36">
      <data key="d0">294836.0</data>
      <data key="d1">3330.0</data>
    </edge>
    <edge source="10" target="26">
      <data key="d0">324320.0</data>
      <data key="d1">3324.0</data>
    </edge>
    <edge source="10" target="36">
      <data key="d0">221127.0</data>
      <data key="d1">3384.0</data>
    </edge>
    <edge source="26" target="10">
      <data key="d0">324320.0</data>
      <data key="d1">3324.0</data>
    </edge>
    <edge source="26" target="22">
      <data key="d0">44225.5</data>
      <data key="d1">17367.0</data>
    </edge>
    <edge source="26" target="25">
      <data key="d0">117935.0</data>
      <data key="d1">18200.0</data>
    </edge>
    <edge source="26" target="27">
      <data key="d0">58967.3</data>
      <data key="d1">7543.0</data>
    </edge>
    <edge source="26" target="53">
      <data key="d0">324320.0</data>
      <data key="d1">8670.0</data>
    </edge>
    <edge source="36" target="10">
      <data key="d0">221127.0</data>
      <data key="d1">3384.0</data>
    </edge>
    <edge source="36" target="18">
      <data key="d0">294836.0</data>
      <data key="d1">3330.0</data>
    </edge>
    <edge source="11" target="49">
      <data key="d0">250611.0</data>
      <data key="d1">11402.0</data>
    </edge>
    <edge source="11" target="19">
      <data key="d0">678124.0</data>
      <data key="d1">21042.0</data>
    </edge>
    <edge source="49" target="11">
      <data key="d0">250611.0</data>
      <data key="d1">11402.0</data>
    </edge>
    <edge source="49" target="1">
      <data key="d0">103193.0</data>
      <data key="d1">3280.0</data>
    </edge>
    <edge source="49" target="31">
      <data key="d0">486480.0</data>
      <data key="d1">9825.0</data>
    </edge>
    <edge source="49" target="51">
      <data key="d0">574931.0</data>
      <data key="d1">10531.0</data>
    </edge>
    <edge source="12" target="4">
      <data key="d0">162160.0</data>
      <data key="d1">3712.0</data>
    </edge>
    <edge source="12" target="9">
      <data key="d0">633898.0</data>
      <data key="d1">5077.0</data>
    </edge>
    <edge source="12" target="35">
      <data key="d0">324320.0</data>
      <data key="d1">4662.0</data>
    </edge>
    <edge source="4" target="12">
      <data key="d0">162160.0</data>
      <data key="d1">3712.0</data>
    </edge>
    <edge source="4" target="35">
      <data key="d0">176902.0</data>
      <data key="d1">3640.0</data>
    </edge>
    <edge source="13" target="3">
      <data key="d0">132676.0</data>
      <data key="d1">6992.0</data>
    </edge>
    <edge source="13" target="15">
      <data key="d0">250611.0</data>
      <data key="d1">4916.0</data>
    </edge>
    <edge source="13" target="21">
      <data key="d0">324320.0</data>
      <data key="d1">4780.0</data>
    </edge>
    <edge source="3" target="13">
      <data key="d0">132676.0</data>
      <data key="d1">6992.0</data>
    </edge>
    <edge source="3" target="51">
      <data key="d0">221127.0</data>
      <data key="d1">16458.0</data>
    </edge>
    <edge source="15" target="13">
      <data key="d0">250611.0</data>
      <data key="d1">4916.0</data>
    </edge>
    <edge source="15" target="39">
      <data key="d0">235869.0</data>
      <data key="d1">5215.0</data>
    </edge>
    <edge source="14" target="38">
      <data key="d0">132676.0</data>
      <data key="d1">3801.0</data>
    </edge>
    <edge source="14" target="52">
      <data key="d0">206385.0</data>
      <data key="d1">3041.0</data>
    </edge>
    <edge source="38" target="14">
      <data key="d0">132676.0</data>
      <data key="d1">3801.0</data>
    </edge>
    <edge source="38" target="47">
      <data key="d0">221127.0</data>
      <data key="d1">3517.0</data>
    </edge>
    <edge source="52" target="14">
      <data key="d0">206385.0</data>
      <data key="d1">3041.0</data>
    </edge>
    <edge source="52" target="30">
      <data key="d0">368546.0</data>
      <data key="d1">3448.0</data>
    </edge>
    <edge source="17" target="6">
      <data key="d0">58967.3</data>
      <data key="d1">3848.0</data>
    </edge>
    <edge source="17" target="48">
      <data key="d0">132676.0</data>
      <data key="d1">4123.0</data>
    </edge>
    <edge source="48" target="17">
      <data key="d0">132676.0</data>
      <data key="d1">4123.0</data>
    </edge>
    <edge source="48" target="16">
      <data key="d0">73709.1</data>
      <data key="d1">3362.0</data>
    </edge>
    <edge source="48" target="25">
      <data key="d0">132676.0</data>
      <data key="d1">11896.0</data>
    </edge>
    <edge source="19" target="11">
      <data key="d0">678124.0</data>
      <data key="d1">21042.0</data>
    </edge>
    <edge source="19" target="42">
      <data key="d0">73709.1</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="19" target="46">
      <data key="d0">663382.0</data>
      <data key="d1">38882.0</data>
    </edge>
    <edge source="42" target="19">
      <data key="d0">73709.1</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="42" target="50">
      <data key="d0">117935.0</data>
      <data key="d1">4198.0</data>
    </edge>
    <edge source="50" target="20">
      <data key="d0">117935.0</data>
      <data key="d1">4657.0</data>
    </edge>
    <edge source="50" target="42">
      <data key="d0">117935.0</data>
      <data key="d1">4198.0</data>
    </edge>
    <edge source="21" target="43">
      <data key="d0">191644.0</data>
      <data key="d1">3680.0</data>
    </edge>
    <edge source="21" target="51">
      <data key="d0">235869.0</data>
      <data key="d1">17344.0</data>
    </edge>
    <edge source="21" target="13">
      <data key="d0">324320.0</data>
      <data key="d1">4780.0</data>
    </edge>
    <edge source="43" target="21">
      <data key="d0">191644.0</data>
      <data key="d1">3680.0</data>
    </edge>
    <edge source="43" target="1">
      <data key="d0">147418.0</data>
      <data key="d1">3818.0</data>
    </edge>
    <edge source="24" target="22">
      <data key="d0">103193.0</data>
      <data key="d1">22178.0</data>
    </edge>
    <edge source="24" target="23">
      <data key="d0">88450.9</data>
      <data key="d1">6708.0</data>
    </edge>
    <edge source="24" target="25">
      <data key="d0">88450.9</data>
      <data key="d1">8402.0</data>
    </edge>
    <edge source="24" target="28">
      <data key="d0">58967.3</data>
      <data key="d1">10730.0</data>
    </edge>
    <edge source="27" target="22">
      <data key="d0">73709.1</data>
      <data key="d1">10214.0</data>
    </edge>
    <edge source="27" target="26">
      <data key="d0">58967.3</data>
      <data key="d1">7543.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">29483.6</data>
      <data key="d1">17608.0</data>
    </edge>
    <edge source="23" target="24">
      <data key="d0">88450.9</data>
      <data key="d1">6708.0</data>
    </edge>
    <edge source="28" target="24">
      <data key="d0">58967.3</data>
      <data key="d1">10730.0</data>
    </edge>
    <edge source="28" target="25">
      <data key="d0">44225.5</data>
      <data key="d1">6601.0</data>
    </edge>
    <edge source="29" target="25">
      <data key="d0">265353.0</data>
      <data key="d1">17210.0</data>
    </edge>
    <edge source="29" target="51">
      <data key="d0">147418.0</data>
      <data key="d1">9775.0</data>
    </edge>
    <edge source="30" target="46">
      <data key="d0">427513.0</data>
      <data key="d1">4522.0</data>
    </edge>
    <edge source="30" target="52">
      <data key="d0">368546.0</data>
      <data key="d1">3448.0</data>
    </edge>
    <edge source="46" target="30">
      <data key="d0">427513.0</data>
      <data key="d1">4522.0</data>
    </edge>
    <edge source="46" target="19">
      <data key="d0">663382.0</data>
      <data key="d1">38882.0</data>
    </edge>
    <edge source="46" target="22">
      <data key="d0">692866.0</data>
      <data key="d1">30360.0</data>
    </edge>
    <edge source="46" target="44">
      <data key="d0">280095.0</data>
      <data key="d1">21065.0</data>
    </edge>
    <edge source="46" target="47">
      <data key="d0">250611.0</data>
      <data key="d1">8338.0</data>
    </edge>
    <edge source="8" target="31">
      <data key="d0">206385.0</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="32" target="40">
      <data key="d0">176902.0</data>
      <data key="d1">3895.0</data>
    </edge>
    <edge source="32" target="7">
      <data key="d0">383287.0</data>
      <data key="d1">4346.0</data>
    </edge>
    <edge source="40" target="32">
      <data key="d0">176902.0</data>
      <data key="d1">3895.0</data>
    </edge>
    <edge source="40" target="34">
      <data key="d0">515964.0</data>
      <data key="d1">9266.0</data>
    </edge>
    <edge source="40" target="22">
      <data key="d0">855026.0</data>
      <data key="d1">25003.0</data>
    </edge>
    <edge source="40" target="53">
      <data key="d0">589673.0</data>
      <data key="d1">3981.0</data>
    </edge>
    <edge source="33" target="16">
      <data key="d0">147418.0</data>
      <data key="d1">3401.0</data>
    </edge>
    <edge source="33" target="20">
      <data key="d0">250611.0</data>
      <data key="d1">3848.0</data>
    </edge>
    <edge source="16" target="33">
      <data key="d0">147418.0</data>
      <data key="d1">3401.0</data>
    </edge>
    <edge source="16" target="48">
      <data key="d0">73709.1</data>
      <data key="d1">3362.0</data>
    </edge>
    <edge source="34" target="22">
      <data key="d0">221127.0</data>
      <data key="d1">16533.0</data>
    </edge>
    <edge source="34" target="40">
      <data key="d0">515964.0</data>
      <data key="d1">9266.0</data>
    </edge>
    <edge source="35" target="4">
      <data key="d0">176902.0</data>
      <data key="d1">3640.0</data>
    </edge>
    <edge source="35" target="9">
      <data key="d0">1297280.0</data>
      <data key="d1">5423.0</data>
    </edge>
    <edge source="35" target="12">
      <data key="d0">324320.0</data>
      <data key="d1">4662.0</data>
    </edge>
    <edge source="35" target="25">
      <data key="d0">560189.0</data>
      <data key="d1">12256.0</data>
    </edge>
    <edge source="47" target="38">
      <data key="d0">221127.0</data>
      <data key="d1">3517.0</data>
    </edge>
    <edge source="47" target="46">
      <data key="d0">250611.0</data>
      <data key="d1">8338.0</data>
    </edge>
    <edge source="1" target="43">
      <data key="d0">147418.0</data>
      <data key="d1">3818.0</data>
    </edge>
    <edge source="1" target="49">
      <data key="d0">103193.0</data>
      <data key="d1">3280.0</data>
    </edge>
    <edge source="45" target="0">
      <data key="d0">73709.1</data>
      <data key="d1">6440.0</data>
    </edge>
    <edge source="45" target="25">
      <data key="d0">221127.0</data>
      <data key="d1">8338.0</data>
    </edge>
    <edge source="51" target="3">
      <data key="d0">221127.0</data>
      <data key="d1">16458.0</data>
    </edge>
    <edge source="51" target="20">
      <data key="d0">280095.0</data>
      <data key="d1">7495.0</data>
    </edge>
    <edge source="51" target="21">
      <data key="d0">235869.0</data>
      <data key="d1">17344.0</data>
    </edge>
    <edge source="51" target="25">
      <data key="d0">398029.0</data>
      <data key="d1">25975.0</data>
    </edge>
    <edge source="51" target="29">
      <data key="d0">147418.0</data>
      <data key="d1">9775.0</data>
    </edge>
    <edge source="51" target="49">
      <data key="d0">574931.0</data>
      <data key="d1">10531.0</data>
    </edge>
    <edge source="53" target="9">
      <data key="d0">427513.0</data>
      <data key="d1">7529.0</data>
    </edge>
    <edge source="53" target="40">
      <data key="d0">589673.0</data>
      <data key="d1">3981.0</data>
    </edge>
    <edge source="53" target="26">
      <data key="d0">324320.0</data>
      <data key="d1">8670.0</data>
    </edge>
  </graph>
</graphml>
