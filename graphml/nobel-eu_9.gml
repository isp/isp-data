<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="6" />
    <node id="11" />
    <node id="12" />
    <node id="13" />
    <node id="1" />
    <node id="3" />
    <node id="21" />
    <node id="2" />
    <node id="14" />
    <node id="15" />
    <node id="7" />
    <node id="26" />
    <node id="4" />
    <node id="8" />
    <node id="17" />
    <node id="20" />
    <node id="25" />
    <node id="5" />
    <node id="19" />
    <node id="10" />
    <node id="18" />
    <node id="9" />
    <node id="23" />
    <node id="16" />
    <node id="27" />
    <node id="24" />
    <node id="22" />
    <edge source="0" target="6">
      <data key="d0">3580.0</data>
      <data key="d1">174.0</data>
    </edge>
    <edge source="0" target="11">
      <data key="d0">7880.0</data>
      <data key="d1">928.0</data>
    </edge>
    <edge source="0" target="12">
      <data key="d0">4870.0</data>
      <data key="d1">561.0</data>
    </edge>
    <edge source="0" target="13">
      <data key="d0">4870.0</data>
      <data key="d1">467.0</data>
    </edge>
    <edge source="6" target="0">
      <data key="d0">3580.0</data>
      <data key="d1">174.0</data>
    </edge>
    <edge source="6" target="10">
      <data key="d0">4440.0</data>
      <data key="d1">420.0</data>
    </edge>
    <edge source="6" target="19">
      <data key="d0">6160.0</data>
      <data key="d1">281.0</data>
    </edge>
    <edge source="11" target="0">
      <data key="d0">7880.0</data>
      <data key="d1">928.0</data>
    </edge>
    <edge source="11" target="9">
      <data key="d0">4440.0</data>
      <data key="d1">304.0</data>
    </edge>
    <edge source="12" target="0">
      <data key="d0">4870.0</data>
      <data key="d1">561.0</data>
    </edge>
    <edge source="12" target="4">
      <data key="d0">4010.0</data>
      <data key="d1">337.0</data>
    </edge>
    <edge source="12" target="10">
      <data key="d0">5300.0</data>
      <data key="d1">364.0</data>
    </edge>
    <edge source="13" target="0">
      <data key="d0">4870.0</data>
      <data key="d1">467.0</data>
    </edge>
    <edge source="13" target="9">
      <data key="d0">5730.0</data>
      <data key="d1">637.0</data>
    </edge>
    <edge source="13" target="19">
      <data key="d0">4870.0</data>
      <data key="d1">360.0</data>
    </edge>
    <edge source="1" target="3">
      <data key="d0">8740.0</data>
      <data key="d1">758.0</data>
    </edge>
    <edge source="1" target="21">
      <data key="d0">10030.0</data>
      <data key="d1">1181.0</data>
    </edge>
    <edge source="3" target="1">
      <data key="d0">8740.0</data>
      <data key="d1">758.0</data>
    </edge>
    <edge source="3" target="7">
      <data key="d0">4440.0</data>
      <data key="d1">308.0</data>
    </edge>
    <edge source="3" target="26">
      <data key="d0">4870.0</data>
      <data key="d1">479.0</data>
    </edge>
    <edge source="21" target="1">
      <data key="d0">10030.0</data>
      <data key="d1">1181.0</data>
    </edge>
    <edge source="21" target="16">
      <data key="d0">6160.0</data>
      <data key="d1">492.0</data>
    </edge>
    <edge source="21" target="26">
      <data key="d0">6160.0</data>
      <data key="d1">513.0</data>
    </edge>
    <edge source="2" target="14">
      <data key="d0">6160.0</data>
      <data key="d1">486.0</data>
    </edge>
    <edge source="2" target="15">
      <data key="d0">6160.0</data>
      <data key="d1">558.0</data>
    </edge>
    <edge source="14" target="2">
      <data key="d0">6160.0</data>
      <data key="d1">486.0</data>
    </edge>
    <edge source="14" target="19">
      <data key="d0">5300.0</data>
      <data key="d1">384.0</data>
    </edge>
    <edge source="14" target="27">
      <data key="d0">4870.0</data>
      <data key="d1">423.0</data>
    </edge>
    <edge source="15" target="2">
      <data key="d0">6160.0</data>
      <data key="d1">558.0</data>
    </edge>
    <edge source="15" target="5">
      <data key="d0">6590.0</data>
      <data key="d1">525.0</data>
    </edge>
    <edge source="7" target="3">
      <data key="d0">4440.0</data>
      <data key="d1">308.0</data>
    </edge>
    <edge source="7" target="20">
      <data key="d0">5730.0</data>
      <data key="d1">551.0</data>
    </edge>
    <edge source="7" target="25">
      <data key="d0">6590.0</data>
      <data key="d1">510.0</data>
    </edge>
    <edge source="26" target="3">
      <data key="d0">4870.0</data>
      <data key="d1">479.0</data>
    </edge>
    <edge source="26" target="21">
      <data key="d0">6160.0</data>
      <data key="d1">513.0</data>
    </edge>
    <edge source="26" target="24">
      <data key="d0">4440.0</data>
      <data key="d1">272.0</data>
    </edge>
    <edge source="4" target="8">
      <data key="d0">4870.0</data>
      <data key="d1">323.0</data>
    </edge>
    <edge source="4" target="12">
      <data key="d0">4010.0</data>
      <data key="d1">337.0</data>
    </edge>
    <edge source="4" target="17">
      <data key="d0">6160.0</data>
      <data key="d1">464.0</data>
    </edge>
    <edge source="4" target="20">
      <data key="d0">4440.0</data>
      <data key="d1">250.0</data>
    </edge>
    <edge source="4" target="25">
      <data key="d0">6160.0</data>
      <data key="d1">738.0</data>
    </edge>
    <edge source="8" target="4">
      <data key="d0">4870.0</data>
      <data key="d1">323.0</data>
    </edge>
    <edge source="8" target="18">
      <data key="d0">6160.0</data>
      <data key="d1">453.0</data>
    </edge>
    <edge source="17" target="4">
      <data key="d0">6160.0</data>
      <data key="d1">464.0</data>
    </edge>
    <edge source="17" target="10">
      <data key="d0">4440.0</data>
      <data key="d1">356.0</data>
    </edge>
    <edge source="17" target="16">
      <data key="d0">4870.0</data>
      <data key="d1">357.0</data>
    </edge>
    <edge source="17" target="24">
      <data key="d0">4870.0</data>
      <data key="d1">488.0</data>
    </edge>
    <edge source="20" target="4">
      <data key="d0">4440.0</data>
      <data key="d1">250.0</data>
    </edge>
    <edge source="20" target="7">
      <data key="d0">5730.0</data>
      <data key="d1">551.0</data>
    </edge>
    <edge source="20" target="24">
      <data key="d0">4010.0</data>
      <data key="d1">274.0</data>
    </edge>
    <edge source="25" target="4">
      <data key="d0">6160.0</data>
      <data key="d1">738.0</data>
    </edge>
    <edge source="25" target="7">
      <data key="d0">6590.0</data>
      <data key="d1">510.0</data>
    </edge>
    <edge source="25" target="22">
      <data key="d0">8740.0</data>
      <data key="d1">750.0</data>
    </edge>
    <edge source="5" target="15">
      <data key="d0">6590.0</data>
      <data key="d1">525.0</data>
    </edge>
    <edge source="5" target="19">
      <data key="d0">6160.0</data>
      <data key="d1">474.0</data>
    </edge>
    <edge source="19" target="5">
      <data key="d0">6160.0</data>
      <data key="d1">474.0</data>
    </edge>
    <edge source="19" target="6">
      <data key="d0">6160.0</data>
      <data key="d1">281.0</data>
    </edge>
    <edge source="19" target="13">
      <data key="d0">4870.0</data>
      <data key="d1">360.0</data>
    </edge>
    <edge source="19" target="14">
      <data key="d0">5300.0</data>
      <data key="d1">384.0</data>
    </edge>
    <edge source="19" target="23">
      <data key="d0">5300.0</data>
      <data key="d1">525.0</data>
    </edge>
    <edge source="10" target="6">
      <data key="d0">4440.0</data>
      <data key="d1">420.0</data>
    </edge>
    <edge source="10" target="12">
      <data key="d0">5300.0</data>
      <data key="d1">364.0</data>
    </edge>
    <edge source="10" target="17">
      <data key="d0">4440.0</data>
      <data key="d1">356.0</data>
    </edge>
    <edge source="10" target="23">
      <data key="d0">3580.0</data>
      <data key="d1">196.0</data>
    </edge>
    <edge source="18" target="8">
      <data key="d0">6160.0</data>
      <data key="d1">453.0</data>
    </edge>
    <edge source="18" target="22">
      <data key="d0">5300.0</data>
      <data key="d1">759.0</data>
    </edge>
    <edge source="9" target="11">
      <data key="d0">4440.0</data>
      <data key="d1">304.0</data>
    </edge>
    <edge source="9" target="13">
      <data key="d0">5730.0</data>
      <data key="d1">637.0</data>
    </edge>
    <edge source="23" target="10">
      <data key="d0">3580.0</data>
      <data key="d1">196.0</data>
    </edge>
    <edge source="23" target="19">
      <data key="d0">5300.0</data>
      <data key="d1">525.0</data>
    </edge>
    <edge source="23" target="27">
      <data key="d0">3150.0</data>
      <data key="d1">143.0</data>
    </edge>
    <edge source="16" target="17">
      <data key="d0">4870.0</data>
      <data key="d1">357.0</data>
    </edge>
    <edge source="16" target="21">
      <data key="d0">6160.0</data>
      <data key="d1">492.0</data>
    </edge>
    <edge source="16" target="27">
      <data key="d0">4010.0</data>
      <data key="d1">209.0</data>
    </edge>
    <edge source="27" target="16">
      <data key="d0">4010.0</data>
      <data key="d1">209.0</data>
    </edge>
    <edge source="27" target="14">
      <data key="d0">4870.0</data>
      <data key="d1">423.0</data>
    </edge>
    <edge source="27" target="23">
      <data key="d0">3150.0</data>
      <data key="d1">143.0</data>
    </edge>
    <edge source="24" target="17">
      <data key="d0">4870.0</data>
      <data key="d1">488.0</data>
    </edge>
    <edge source="24" target="20">
      <data key="d0">4010.0</data>
      <data key="d1">274.0</data>
    </edge>
    <edge source="24" target="26">
      <data key="d0">4440.0</data>
      <data key="d1">272.0</data>
    </edge>
    <edge source="22" target="18">
      <data key="d0">5300.0</data>
      <data key="d1">759.0</data>
    </edge>
    <edge source="22" target="25">
      <data key="d0">8740.0</data>
      <data key="d1">750.0</data>
    </edge>
  </graph>
</graphml>
