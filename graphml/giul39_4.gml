<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="1" />
    <node id="2" />
    <node id="3" />
    <node id="4" />
    <node id="6" />
    <node id="7" />
    <node id="9" />
    <node id="5" />
    <node id="8" />
    <node id="17" />
    <node id="10" />
    <node id="11" />
    <node id="24" />
    <node id="12" />
    <node id="13" />
    <node id="14" />
    <node id="23" />
    <node id="16" />
    <node id="18" />
    <node id="19" />
    <node id="25" />
    <node id="15" />
    <node id="22" />
    <node id="30" />
    <node id="20" />
    <node id="35" />
    <node id="21" />
    <node id="26" />
    <node id="27" />
    <node id="33" />
    <node id="28" />
    <node id="29" />
    <node id="34" />
    <node id="31" />
    <node id="32" />
    <node id="38" />
    <node id="36" />
    <node id="37" />
    <edge source="0" target="1">
      <data key="d0">10.0</data>
      <data key="d1">21409.0</data>
    </edge>
    <edge source="0" target="2">
      <data key="d0">20.0</data>
      <data key="d1">18299.0</data>
    </edge>
    <edge source="0" target="3">
      <data key="d0">50.0</data>
      <data key="d1">8298.0</data>
    </edge>
    <edge source="0" target="4">
      <data key="d0">65.0</data>
      <data key="d1">9928.0</data>
    </edge>
    <edge source="0" target="6">
      <data key="d0">30.0</data>
      <data key="d1">18409.0</data>
    </edge>
    <edge source="1" target="0">
      <data key="d0">10.0</data>
      <data key="d1">21409.0</data>
    </edge>
    <edge source="1" target="2">
      <data key="d0">20.0</data>
      <data key="d1">5331.0</data>
    </edge>
    <edge source="1" target="7">
      <data key="d0">18.0</data>
      <data key="d1">14677.0</data>
    </edge>
    <edge source="2" target="0">
      <data key="d0">20.0</data>
      <data key="d1">18299.0</data>
    </edge>
    <edge source="2" target="1">
      <data key="d0">20.0</data>
      <data key="d1">5331.0</data>
    </edge>
    <edge source="2" target="3">
      <data key="d0">32.0</data>
      <data key="d1">15201.0</data>
    </edge>
    <edge source="2" target="6">
      <data key="d0">17.0</data>
      <data key="d1">6207.0</data>
    </edge>
    <edge source="3" target="0">
      <data key="d0">50.0</data>
      <data key="d1">8298.0</data>
    </edge>
    <edge source="3" target="2">
      <data key="d0">32.0</data>
      <data key="d1">15201.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">15.0</data>
      <data key="d1">10139.0</data>
    </edge>
    <edge source="3" target="6">
      <data key="d0">45.0</data>
      <data key="d1">12659.0</data>
    </edge>
    <edge source="3" target="9">
      <data key="d0">25.0</data>
      <data key="d1">18173.0</data>
    </edge>
    <edge source="4" target="0">
      <data key="d0">65.0</data>
      <data key="d1">9928.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">15.0</data>
      <data key="d1">10139.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">30.0</data>
      <data key="d1">16269.0</data>
    </edge>
    <edge source="4" target="8">
      <data key="d0">25.0</data>
      <data key="d1">6297.0</data>
    </edge>
    <edge source="6" target="0">
      <data key="d0">30.0</data>
      <data key="d1">18409.0</data>
    </edge>
    <edge source="6" target="2">
      <data key="d0">17.0</data>
      <data key="d1">6207.0</data>
    </edge>
    <edge source="6" target="3">
      <data key="d0">45.0</data>
      <data key="d1">12659.0</data>
    </edge>
    <edge source="6" target="9">
      <data key="d0">40.0</data>
      <data key="d1">7106.0</data>
    </edge>
    <edge source="6" target="10">
      <data key="d0">19.0</data>
      <data key="d1">10316.0</data>
    </edge>
    <edge source="7" target="1">
      <data key="d0">18.0</data>
      <data key="d1">14677.0</data>
    </edge>
    <edge source="7" target="11">
      <data key="d0">20.0</data>
      <data key="d1">7529.0</data>
    </edge>
    <edge source="7" target="24">
      <data key="d0">40.0</data>
      <data key="d1">15852.0</data>
    </edge>
    <edge source="9" target="3">
      <data key="d0">25.0</data>
      <data key="d1">18173.0</data>
    </edge>
    <edge source="9" target="6">
      <data key="d0">40.0</data>
      <data key="d1">7106.0</data>
    </edge>
    <edge source="9" target="8">
      <data key="d0">40.0</data>
      <data key="d1">22771.0</data>
    </edge>
    <edge source="9" target="10">
      <data key="d0">40.0</data>
      <data key="d1">8609.0</data>
    </edge>
    <edge source="9" target="14">
      <data key="d0">24.0</data>
      <data key="d1">7654.0</data>
    </edge>
    <edge source="9" target="23">
      <data key="d0">40.0</data>
      <data key="d1">10966.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">30.0</data>
      <data key="d1">16269.0</data>
    </edge>
    <edge source="5" target="8">
      <data key="d0">25.0</data>
      <data key="d1">14494.0</data>
    </edge>
    <edge source="5" target="17">
      <data key="d0">45.0</data>
      <data key="d1">15195.0</data>
    </edge>
    <edge source="8" target="4">
      <data key="d0">25.0</data>
      <data key="d1">6297.0</data>
    </edge>
    <edge source="8" target="5">
      <data key="d0">25.0</data>
      <data key="d1">14494.0</data>
    </edge>
    <edge source="8" target="9">
      <data key="d0">40.0</data>
      <data key="d1">22771.0</data>
    </edge>
    <edge source="8" target="12">
      <data key="d0">20.0</data>
      <data key="d1">10018.0</data>
    </edge>
    <edge source="8" target="13">
      <data key="d0">18.0</data>
      <data key="d1">8415.0</data>
    </edge>
    <edge source="8" target="14">
      <data key="d0">40.0</data>
      <data key="d1">15801.0</data>
    </edge>
    <edge source="17" target="5">
      <data key="d0">45.0</data>
      <data key="d1">15195.0</data>
    </edge>
    <edge source="17" target="12">
      <data key="d0">30.0</data>
      <data key="d1">18457.0</data>
    </edge>
    <edge source="17" target="18">
      <data key="d0">23.0</data>
      <data key="d1">12140.0</data>
    </edge>
    <edge source="17" target="20">
      <data key="d0">18.0</data>
      <data key="d1">7284.0</data>
    </edge>
    <edge source="17" target="35">
      <data key="d0">40.0</data>
      <data key="d1">17024.0</data>
    </edge>
    <edge source="10" target="6">
      <data key="d0">19.0</data>
      <data key="d1">10316.0</data>
    </edge>
    <edge source="10" target="9">
      <data key="d0">40.0</data>
      <data key="d1">8609.0</data>
    </edge>
    <edge source="10" target="11">
      <data key="d0">10.0</data>
      <data key="d1">9313.0</data>
    </edge>
    <edge source="10" target="16">
      <data key="d0">20.0</data>
      <data key="d1">7527.0</data>
    </edge>
    <edge source="11" target="7">
      <data key="d0">20.0</data>
      <data key="d1">7529.0</data>
    </edge>
    <edge source="11" target="10">
      <data key="d0">10.0</data>
      <data key="d1">9313.0</data>
    </edge>
    <edge source="11" target="24">
      <data key="d0">20.0</data>
      <data key="d1">10481.0</data>
    </edge>
    <edge source="24" target="7">
      <data key="d0">40.0</data>
      <data key="d1">15852.0</data>
    </edge>
    <edge source="24" target="11">
      <data key="d0">20.0</data>
      <data key="d1">10481.0</data>
    </edge>
    <edge source="24" target="16">
      <data key="d0">20.0</data>
      <data key="d1">17322.0</data>
    </edge>
    <edge source="24" target="30">
      <data key="d0">40.0</data>
      <data key="d1">8806.0</data>
    </edge>
    <edge source="12" target="8">
      <data key="d0">20.0</data>
      <data key="d1">10018.0</data>
    </edge>
    <edge source="12" target="13">
      <data key="d0">18.0</data>
      <data key="d1">7632.0</data>
    </edge>
    <edge source="12" target="17">
      <data key="d0">30.0</data>
      <data key="d1">18457.0</data>
    </edge>
    <edge source="12" target="18">
      <data key="d0">14.0</data>
      <data key="d1">8665.0</data>
    </edge>
    <edge source="13" target="8">
      <data key="d0">18.0</data>
      <data key="d1">8415.0</data>
    </edge>
    <edge source="13" target="12">
      <data key="d0">18.0</data>
      <data key="d1">7632.0</data>
    </edge>
    <edge source="13" target="14">
      <data key="d0">20.0</data>
      <data key="d1">11474.0</data>
    </edge>
    <edge source="13" target="19">
      <data key="d0">18.0</data>
      <data key="d1">9415.0</data>
    </edge>
    <edge source="13" target="25">
      <data key="d0">30.0</data>
      <data key="d1">13393.0</data>
    </edge>
    <edge source="14" target="8">
      <data key="d0">40.0</data>
      <data key="d1">15801.0</data>
    </edge>
    <edge source="14" target="9">
      <data key="d0">24.0</data>
      <data key="d1">7654.0</data>
    </edge>
    <edge source="14" target="13">
      <data key="d0">20.0</data>
      <data key="d1">11474.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">10.0</data>
      <data key="d1">6080.0</data>
    </edge>
    <edge source="23" target="9">
      <data key="d0">40.0</data>
      <data key="d1">10966.0</data>
    </edge>
    <edge source="23" target="15">
      <data key="d0">15.0</data>
      <data key="d1">7728.0</data>
    </edge>
    <edge source="23" target="16">
      <data key="d0">25.0</data>
      <data key="d1">10576.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">15.0</data>
      <data key="d1">13363.0</data>
    </edge>
    <edge source="23" target="27">
      <data key="d0">11.0</data>
      <data key="d1">13650.0</data>
    </edge>
    <edge source="23" target="28">
      <data key="d0">9.0</data>
      <data key="d1">8006.0</data>
    </edge>
    <edge source="16" target="10">
      <data key="d0">20.0</data>
      <data key="d1">7527.0</data>
    </edge>
    <edge source="16" target="15">
      <data key="d0">30.0</data>
      <data key="d1">13387.0</data>
    </edge>
    <edge source="16" target="23">
      <data key="d0">25.0</data>
      <data key="d1">10576.0</data>
    </edge>
    <edge source="16" target="24">
      <data key="d0">20.0</data>
      <data key="d1">17322.0</data>
    </edge>
    <edge source="16" target="30">
      <data key="d0">35.0</data>
      <data key="d1">19767.0</data>
    </edge>
    <edge source="18" target="12">
      <data key="d0">14.0</data>
      <data key="d1">8665.0</data>
    </edge>
    <edge source="18" target="17">
      <data key="d0">23.0</data>
      <data key="d1">12140.0</data>
    </edge>
    <edge source="18" target="21">
      <data key="d0">11.0</data>
      <data key="d1">5797.0</data>
    </edge>
    <edge source="18" target="25">
      <data key="d0">24.0</data>
      <data key="d1">9001.0</data>
    </edge>
    <edge source="19" target="13">
      <data key="d0">18.0</data>
      <data key="d1">9415.0</data>
    </edge>
    <edge source="19" target="15">
      <data key="d0">23.0</data>
      <data key="d1">5187.0</data>
    </edge>
    <edge source="19" target="22">
      <data key="d0">17.0</data>
      <data key="d1">15862.0</data>
    </edge>
    <edge source="19" target="25">
      <data key="d0">10.0</data>
      <data key="d1">11014.0</data>
    </edge>
    <edge source="25" target="13">
      <data key="d0">30.0</data>
      <data key="d1">13393.0</data>
    </edge>
    <edge source="25" target="18">
      <data key="d0">24.0</data>
      <data key="d1">9001.0</data>
    </edge>
    <edge source="25" target="19">
      <data key="d0">10.0</data>
      <data key="d1">11014.0</data>
    </edge>
    <edge source="25" target="22">
      <data key="d0">18.0</data>
      <data key="d1">14577.0</data>
    </edge>
    <edge source="25" target="26">
      <data key="d0">17.0</data>
      <data key="d1">16053.0</data>
    </edge>
    <edge source="25" target="29">
      <data key="d0">35.0</data>
      <data key="d1">23982.0</data>
    </edge>
    <edge source="25" target="33">
      <data key="d0">20.0</data>
      <data key="d1">16182.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">10.0</data>
      <data key="d1">6080.0</data>
    </edge>
    <edge source="15" target="16">
      <data key="d0">30.0</data>
      <data key="d1">13387.0</data>
    </edge>
    <edge source="15" target="19">
      <data key="d0">23.0</data>
      <data key="d1">5187.0</data>
    </edge>
    <edge source="15" target="22">
      <data key="d0">12.0</data>
      <data key="d1">15993.0</data>
    </edge>
    <edge source="15" target="23">
      <data key="d0">15.0</data>
      <data key="d1">7728.0</data>
    </edge>
    <edge source="22" target="15">
      <data key="d0">12.0</data>
      <data key="d1">15993.0</data>
    </edge>
    <edge source="22" target="19">
      <data key="d0">17.0</data>
      <data key="d1">15862.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">15.0</data>
      <data key="d1">13363.0</data>
    </edge>
    <edge source="22" target="25">
      <data key="d0">18.0</data>
      <data key="d1">14577.0</data>
    </edge>
    <edge source="22" target="27">
      <data key="d0">9.0</data>
      <data key="d1">10262.0</data>
    </edge>
    <edge source="22" target="33">
      <data key="d0">40.0</data>
      <data key="d1">8570.0</data>
    </edge>
    <edge source="30" target="16">
      <data key="d0">35.0</data>
      <data key="d1">19767.0</data>
    </edge>
    <edge source="30" target="24">
      <data key="d0">40.0</data>
      <data key="d1">8806.0</data>
    </edge>
    <edge source="30" target="27">
      <data key="d0">34.0</data>
      <data key="d1">20771.0</data>
    </edge>
    <edge source="30" target="28">
      <data key="d0">18.0</data>
      <data key="d1">15603.0</data>
    </edge>
    <edge source="30" target="29">
      <data key="d0">22.0</data>
      <data key="d1">17459.0</data>
    </edge>
    <edge source="30" target="32">
      <data key="d0">20.0</data>
      <data key="d1">21932.0</data>
    </edge>
    <edge source="30" target="38">
      <data key="d0">22.0</data>
      <data key="d1">16311.0</data>
    </edge>
    <edge source="20" target="17">
      <data key="d0">18.0</data>
      <data key="d1">7284.0</data>
    </edge>
    <edge source="20" target="21">
      <data key="d0">13.0</data>
      <data key="d1">5441.0</data>
    </edge>
    <edge source="20" target="35">
      <data key="d0">35.0</data>
      <data key="d1">12735.0</data>
    </edge>
    <edge source="35" target="17">
      <data key="d0">40.0</data>
      <data key="d1">17024.0</data>
    </edge>
    <edge source="35" target="20">
      <data key="d0">35.0</data>
      <data key="d1">12735.0</data>
    </edge>
    <edge source="35" target="34">
      <data key="d0">15.0</data>
      <data key="d1">13735.0</data>
    </edge>
    <edge source="35" target="37">
      <data key="d0">40.0</data>
      <data key="d1">24418.0</data>
    </edge>
    <edge source="21" target="18">
      <data key="d0">11.0</data>
      <data key="d1">5797.0</data>
    </edge>
    <edge source="21" target="20">
      <data key="d0">13.0</data>
      <data key="d1">5441.0</data>
    </edge>
    <edge source="21" target="26">
      <data key="d0">13.0</data>
      <data key="d1">12194.0</data>
    </edge>
    <edge source="26" target="21">
      <data key="d0">13.0</data>
      <data key="d1">12194.0</data>
    </edge>
    <edge source="26" target="25">
      <data key="d0">17.0</data>
      <data key="d1">16053.0</data>
    </edge>
    <edge source="26" target="34">
      <data key="d0">17.0</data>
      <data key="d1">7616.0</data>
    </edge>
    <edge source="27" target="22">
      <data key="d0">9.0</data>
      <data key="d1">10262.0</data>
    </edge>
    <edge source="27" target="23">
      <data key="d0">11.0</data>
      <data key="d1">13650.0</data>
    </edge>
    <edge source="27" target="29">
      <data key="d0">6.0</data>
      <data key="d1">7350.0</data>
    </edge>
    <edge source="27" target="30">
      <data key="d0">34.0</data>
      <data key="d1">20771.0</data>
    </edge>
    <edge source="27" target="31">
      <data key="d0">16.0</data>
      <data key="d1">10720.0</data>
    </edge>
    <edge source="27" target="33">
      <data key="d0">25.0</data>
      <data key="d1">16055.0</data>
    </edge>
    <edge source="33" target="22">
      <data key="d0">40.0</data>
      <data key="d1">8570.0</data>
    </edge>
    <edge source="33" target="25">
      <data key="d0">20.0</data>
      <data key="d1">16182.0</data>
    </edge>
    <edge source="33" target="27">
      <data key="d0">25.0</data>
      <data key="d1">16055.0</data>
    </edge>
    <edge source="33" target="31">
      <data key="d0">13.0</data>
      <data key="d1">13209.0</data>
    </edge>
    <edge source="33" target="32">
      <data key="d0">20.0</data>
      <data key="d1">17224.0</data>
    </edge>
    <edge source="33" target="34">
      <data key="d0">23.0</data>
      <data key="d1">9263.0</data>
    </edge>
    <edge source="33" target="36">
      <data key="d0">12.0</data>
      <data key="d1">22516.0</data>
    </edge>
    <edge source="33" target="37">
      <data key="d0">10.0</data>
      <data key="d1">16515.0</data>
    </edge>
    <edge source="28" target="23">
      <data key="d0">9.0</data>
      <data key="d1">8006.0</data>
    </edge>
    <edge source="28" target="29">
      <data key="d0">20.0</data>
      <data key="d1">8828.0</data>
    </edge>
    <edge source="28" target="30">
      <data key="d0">18.0</data>
      <data key="d1">15603.0</data>
    </edge>
    <edge source="29" target="25">
      <data key="d0">35.0</data>
      <data key="d1">23982.0</data>
    </edge>
    <edge source="29" target="27">
      <data key="d0">6.0</data>
      <data key="d1">7350.0</data>
    </edge>
    <edge source="29" target="28">
      <data key="d0">20.0</data>
      <data key="d1">8828.0</data>
    </edge>
    <edge source="29" target="30">
      <data key="d0">22.0</data>
      <data key="d1">17459.0</data>
    </edge>
    <edge source="34" target="26">
      <data key="d0">17.0</data>
      <data key="d1">7616.0</data>
    </edge>
    <edge source="34" target="33">
      <data key="d0">23.0</data>
      <data key="d1">9263.0</data>
    </edge>
    <edge source="34" target="35">
      <data key="d0">15.0</data>
      <data key="d1">13735.0</data>
    </edge>
    <edge source="31" target="27">
      <data key="d0">16.0</data>
      <data key="d1">10720.0</data>
    </edge>
    <edge source="31" target="33">
      <data key="d0">13.0</data>
      <data key="d1">13209.0</data>
    </edge>
    <edge source="31" target="36">
      <data key="d0">13.0</data>
      <data key="d1">11245.0</data>
    </edge>
    <edge source="32" target="30">
      <data key="d0">20.0</data>
      <data key="d1">21932.0</data>
    </edge>
    <edge source="32" target="33">
      <data key="d0">20.0</data>
      <data key="d1">17224.0</data>
    </edge>
    <edge source="32" target="36">
      <data key="d0">11.0</data>
      <data key="d1">15673.0</data>
    </edge>
    <edge source="32" target="38">
      <data key="d0">9.0</data>
      <data key="d1">10288.0</data>
    </edge>
    <edge source="38" target="30">
      <data key="d0">22.0</data>
      <data key="d1">16311.0</data>
    </edge>
    <edge source="38" target="32">
      <data key="d0">9.0</data>
      <data key="d1">10288.0</data>
    </edge>
    <edge source="38" target="36">
      <data key="d0">13.0</data>
      <data key="d1">18711.0</data>
    </edge>
    <edge source="36" target="31">
      <data key="d0">13.0</data>
      <data key="d1">11245.0</data>
    </edge>
    <edge source="36" target="32">
      <data key="d0">11.0</data>
      <data key="d1">15673.0</data>
    </edge>
    <edge source="36" target="33">
      <data key="d0">12.0</data>
      <data key="d1">22516.0</data>
    </edge>
    <edge source="36" target="37">
      <data key="d0">16.0</data>
      <data key="d1">17624.0</data>
    </edge>
    <edge source="36" target="38">
      <data key="d0">13.0</data>
      <data key="d1">18711.0</data>
    </edge>
    <edge source="37" target="33">
      <data key="d0">10.0</data>
      <data key="d1">16515.0</data>
    </edge>
    <edge source="37" target="35">
      <data key="d0">40.0</data>
      <data key="d1">24418.0</data>
    </edge>
    <edge source="37" target="36">
      <data key="d0">16.0</data>
      <data key="d1">17624.0</data>
    </edge>
  </graph>
</graphml>
