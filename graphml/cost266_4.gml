<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="7" />
    <node id="13" />
    <node id="14" />
    <node id="18" />
    <node id="1" />
    <node id="25" />
    <node id="30" />
    <node id="35" />
    <node id="2" />
    <node id="20" />
    <node id="21" />
    <node id="29" />
    <node id="3" />
    <node id="8" />
    <node id="4" />
    <node id="9" />
    <node id="23" />
    <node id="27" />
    <node id="34" />
    <node id="5" />
    <node id="6" />
    <node id="26" />
    <node id="11" />
    <node id="16" />
    <node id="24" />
    <node id="31" />
    <node id="10" />
    <node id="12" />
    <node id="32" />
    <node id="15" />
    <node id="17" />
    <node id="19" />
    <node id="36" />
    <node id="28" />
    <node id="22" />
    <node id="33" />
    <edge source="0" target="7">
      <data key="d0">23310.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="0" target="13">
      <data key="d0">96030.0</data>
      <data key="d1">980.0</data>
    </edge>
    <edge source="0" target="14">
      <data key="d0">49680.0</data>
      <data key="d1">526.0</data>
    </edge>
    <edge source="0" target="18">
      <data key="d0">48600.0</data>
      <data key="d1">514.0</data>
    </edge>
    <edge source="7" target="0">
      <data key="d0">23310.0</data>
      <data key="d1">162.0</data>
    </edge>
    <edge source="7" target="11">
      <data key="d0">23760.0</data>
      <data key="d1">246.0</data>
    </edge>
    <edge source="7" target="26">
      <data key="d0">35370.0</data>
      <data key="d1">281.0</data>
    </edge>
    <edge source="13" target="0">
      <data key="d0">96030.0</data>
      <data key="d1">980.0</data>
    </edge>
    <edge source="13" target="5">
      <data key="d0">54900.0</data>
      <data key="d1">413.0</data>
    </edge>
    <edge source="13" target="10">
      <data key="d0">41580.0</data>
      <data key="d1">322.0</data>
    </edge>
    <edge source="14" target="0">
      <data key="d0">49680.0</data>
      <data key="d1">526.0</data>
    </edge>
    <edge source="14" target="4">
      <data key="d0">34290.0</data>
      <data key="d1">353.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">53280.0</data>
      <data key="d1">370.0</data>
    </edge>
    <edge source="18" target="0">
      <data key="d0">48600.0</data>
      <data key="d1">514.0</data>
    </edge>
    <edge source="18" target="5">
      <data key="d0">21510.0</data>
      <data key="d1">197.0</data>
    </edge>
    <edge source="18" target="10">
      <data key="d0">62100.0</data>
      <data key="d1">635.0</data>
    </edge>
    <edge source="18" target="17">
      <data key="d0">177930.0</data>
      <data key="d1">1560.0</data>
    </edge>
    <edge source="18" target="26">
      <data key="d0">46260.0</data>
      <data key="d1">363.0</data>
    </edge>
    <edge source="1" target="25">
      <data key="d0">122670.0</data>
      <data key="d1">1038.0</data>
    </edge>
    <edge source="1" target="30">
      <data key="d0">71460.0</data>
      <data key="d1">477.0</data>
    </edge>
    <edge source="1" target="35">
      <data key="d0">135000.0</data>
      <data key="d1">1099.0</data>
    </edge>
    <edge source="25" target="1">
      <data key="d0">122670.0</data>
      <data key="d1">1038.0</data>
    </edge>
    <edge source="25" target="28">
      <data key="d0">57420.0</data>
      <data key="d1">385.0</data>
    </edge>
    <edge source="30" target="1">
      <data key="d0">71460.0</data>
      <data key="d1">477.0</data>
    </edge>
    <edge source="30" target="3">
      <data key="d0">43830.0</data>
      <data key="d1">351.0</data>
    </edge>
    <edge source="35" target="1">
      <data key="d0">135000.0</data>
      <data key="d1">1099.0</data>
    </edge>
    <edge source="35" target="3">
      <data key="d0">49590.0</data>
      <data key="d1">459.0</data>
    </edge>
    <edge source="35" target="28">
      <data key="d0">70470.0</data>
      <data key="d1">529.0</data>
    </edge>
    <edge source="35" target="33">
      <data key="d0">36000.0</data>
      <data key="d1">242.0</data>
    </edge>
    <edge source="2" target="20">
      <data key="d0">68400.0</data>
      <data key="d1">598.0</data>
    </edge>
    <edge source="2" target="21">
      <data key="d0">45720.0</data>
      <data key="d1">373.0</data>
    </edge>
    <edge source="2" target="29">
      <data key="d0">111960.0</data>
      <data key="d1">908.0</data>
    </edge>
    <edge source="20" target="2">
      <data key="d0">68400.0</data>
      <data key="d1">598.0</data>
    </edge>
    <edge source="20" target="6">
      <data key="d0">75060.0</data>
      <data key="d1">544.0</data>
    </edge>
    <edge source="20" target="17">
      <data key="d0">67590.0</data>
      <data key="d1">567.0</data>
    </edge>
    <edge source="21" target="2">
      <data key="d0">45720.0</data>
      <data key="d1">373.0</data>
    </edge>
    <edge source="21" target="6">
      <data key="d0">68040.0</data>
      <data key="d1">614.0</data>
    </edge>
    <edge source="21" target="19">
      <data key="d0">36990.0</data>
      <data key="d1">249.0</data>
    </edge>
    <edge source="21" target="28">
      <data key="d0">81630.0</data>
      <data key="d1">727.0</data>
    </edge>
    <edge source="29" target="2">
      <data key="d0">111960.0</data>
      <data key="d1">908.0</data>
    </edge>
    <edge source="29" target="17">
      <data key="d0">42390.0</data>
      <data key="d1">343.0</data>
    </edge>
    <edge source="3" target="8">
      <data key="d0">42660.0</data>
      <data key="d1">302.0</data>
    </edge>
    <edge source="3" target="30">
      <data key="d0">43830.0</data>
      <data key="d1">351.0</data>
    </edge>
    <edge source="3" target="35">
      <data key="d0">49590.0</data>
      <data key="d1">459.0</data>
    </edge>
    <edge source="8" target="3">
      <data key="d0">42660.0</data>
      <data key="d1">302.0</data>
    </edge>
    <edge source="8" target="16">
      <data key="d0">39240.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="8" target="27">
      <data key="d0">60120.0</data>
      <data key="d1">532.0</data>
    </edge>
    <edge source="4" target="9">
      <data key="d0">48600.0</data>
      <data key="d1">331.0</data>
    </edge>
    <edge source="4" target="14">
      <data key="d0">34290.0</data>
      <data key="d1">353.0</data>
    </edge>
    <edge source="4" target="23">
      <data key="d0">68130.0</data>
      <data key="d1">476.0</data>
    </edge>
    <edge source="4" target="27">
      <data key="d0">37800.0</data>
      <data key="d1">265.0</data>
    </edge>
    <edge source="4" target="34">
      <data key="d0">69750.0</data>
      <data key="d1">760.0</data>
    </edge>
    <edge source="9" target="4">
      <data key="d0">48600.0</data>
      <data key="d1">331.0</data>
    </edge>
    <edge source="9" target="24">
      <data key="d0">64980.0</data>
      <data key="d1">459.0</data>
    </edge>
    <edge source="9" target="31">
      <data key="d0">69930.0</data>
      <data key="d1">656.0</data>
    </edge>
    <edge source="23" target="4">
      <data key="d0">68130.0</data>
      <data key="d1">476.0</data>
    </edge>
    <edge source="23" target="12">
      <data key="d0">41040.0</data>
      <data key="d1">351.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">46980.0</data>
      <data key="d1">358.0</data>
    </edge>
    <edge source="23" target="33">
      <data key="d0">48060.0</data>
      <data key="d1">480.0</data>
    </edge>
    <edge source="27" target="4">
      <data key="d0">37800.0</data>
      <data key="d1">265.0</data>
    </edge>
    <edge source="27" target="8">
      <data key="d0">60120.0</data>
      <data key="d1">532.0</data>
    </edge>
    <edge source="27" target="33">
      <data key="d0">33840.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="34" target="4">
      <data key="d0">69750.0</data>
      <data key="d1">760.0</data>
    </edge>
    <edge source="34" target="15">
      <data key="d0">123300.0</data>
      <data key="d1">886.0</data>
    </edge>
    <edge source="34" target="16">
      <data key="d0">34470.0</data>
      <data key="d1">244.0</data>
    </edge>
    <edge source="5" target="13">
      <data key="d0">54900.0</data>
      <data key="d1">413.0</data>
    </edge>
    <edge source="5" target="18">
      <data key="d0">21510.0</data>
      <data key="d1">197.0</data>
    </edge>
    <edge source="6" target="20">
      <data key="d0">75060.0</data>
      <data key="d1">544.0</data>
    </edge>
    <edge source="6" target="21">
      <data key="d0">68040.0</data>
      <data key="d1">614.0</data>
    </edge>
    <edge source="6" target="26">
      <data key="d0">67230.0</data>
      <data key="d1">496.0</data>
    </edge>
    <edge source="26" target="6">
      <data key="d0">67230.0</data>
      <data key="d1">496.0</data>
    </edge>
    <edge source="26" target="7">
      <data key="d0">35370.0</data>
      <data key="d1">281.0</data>
    </edge>
    <edge source="26" target="18">
      <data key="d0">46260.0</data>
      <data key="d1">363.0</data>
    </edge>
    <edge source="26" target="19">
      <data key="d0">53460.0</data>
      <data key="d1">401.0</data>
    </edge>
    <edge source="26" target="32">
      <data key="d0">54000.0</data>
      <data key="d1">545.0</data>
    </edge>
    <edge source="11" target="7">
      <data key="d0">23760.0</data>
      <data key="d1">246.0</data>
    </edge>
    <edge source="11" target="12">
      <data key="d0">24750.0</data>
      <data key="d1">220.0</data>
    </edge>
    <edge source="16" target="8">
      <data key="d0">39240.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="16" target="34">
      <data key="d0">34470.0</data>
      <data key="d1">244.0</data>
    </edge>
    <edge source="24" target="9">
      <data key="d0">64980.0</data>
      <data key="d1">459.0</data>
    </edge>
    <edge source="24" target="15">
      <data key="d0">106380.0</data>
      <data key="d1">1422.0</data>
    </edge>
    <edge source="31" target="9">
      <data key="d0">69930.0</data>
      <data key="d1">656.0</data>
    </edge>
    <edge source="31" target="15">
      <data key="d0">53730.0</data>
      <data key="d1">697.0</data>
    </edge>
    <edge source="10" target="13">
      <data key="d0">41580.0</data>
      <data key="d1">322.0</data>
    </edge>
    <edge source="10" target="18">
      <data key="d0">62100.0</data>
      <data key="d1">635.0</data>
    </edge>
    <edge source="12" target="11">
      <data key="d0">24750.0</data>
      <data key="d1">220.0</data>
    </edge>
    <edge source="12" target="14">
      <data key="d0">53280.0</data>
      <data key="d1">370.0</data>
    </edge>
    <edge source="12" target="23">
      <data key="d0">41040.0</data>
      <data key="d1">351.0</data>
    </edge>
    <edge source="12" target="32">
      <data key="d0">24390.0</data>
      <data key="d1">177.0</data>
    </edge>
    <edge source="32" target="12">
      <data key="d0">24390.0</data>
      <data key="d1">177.0</data>
    </edge>
    <edge source="32" target="26">
      <data key="d0">54000.0</data>
      <data key="d1">545.0</data>
    </edge>
    <edge source="32" target="36">
      <data key="d0">19620.0</data>
      <data key="d1">143.0</data>
    </edge>
    <edge source="15" target="24">
      <data key="d0">106380.0</data>
      <data key="d1">1422.0</data>
    </edge>
    <edge source="15" target="31">
      <data key="d0">53730.0</data>
      <data key="d1">697.0</data>
    </edge>
    <edge source="15" target="34">
      <data key="d0">123300.0</data>
      <data key="d1">886.0</data>
    </edge>
    <edge source="17" target="18">
      <data key="d0">177930.0</data>
      <data key="d1">1560.0</data>
    </edge>
    <edge source="17" target="20">
      <data key="d0">67590.0</data>
      <data key="d1">567.0</data>
    </edge>
    <edge source="17" target="29">
      <data key="d0">42390.0</data>
      <data key="d1">343.0</data>
    </edge>
    <edge source="19" target="21">
      <data key="d0">36990.0</data>
      <data key="d1">249.0</data>
    </edge>
    <edge source="19" target="26">
      <data key="d0">53460.0</data>
      <data key="d1">401.0</data>
    </edge>
    <edge source="19" target="36">
      <data key="d0">45630.0</data>
      <data key="d1">407.0</data>
    </edge>
    <edge source="36" target="19">
      <data key="d0">45630.0</data>
      <data key="d1">407.0</data>
    </edge>
    <edge source="36" target="22">
      <data key="d0">29430.0</data>
      <data key="d1">201.0</data>
    </edge>
    <edge source="36" target="32">
      <data key="d0">19620.0</data>
      <data key="d1">143.0</data>
    </edge>
    <edge source="28" target="21">
      <data key="d0">81630.0</data>
      <data key="d1">727.0</data>
    </edge>
    <edge source="28" target="22">
      <data key="d0">64800.0</data>
      <data key="d1">490.0</data>
    </edge>
    <edge source="28" target="25">
      <data key="d0">57420.0</data>
      <data key="d1">385.0</data>
    </edge>
    <edge source="28" target="35">
      <data key="d0">70470.0</data>
      <data key="d1">529.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">46980.0</data>
      <data key="d1">358.0</data>
    </edge>
    <edge source="22" target="28">
      <data key="d0">64800.0</data>
      <data key="d1">490.0</data>
    </edge>
    <edge source="22" target="36">
      <data key="d0">29430.0</data>
      <data key="d1">201.0</data>
    </edge>
    <edge source="33" target="23">
      <data key="d0">48060.0</data>
      <data key="d1">480.0</data>
    </edge>
    <edge source="33" target="27">
      <data key="d0">33840.0</data>
      <data key="d1">269.0</data>
    </edge>
    <edge source="33" target="35">
      <data key="d0">36000.0</data>
      <data key="d1">242.0</data>
    </edge>
  </graph>
</graphml>
