<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="12" />
    <node id="13" />
    <node id="16" />
    <node id="19" />
    <node id="28" />
    <node id="1" />
    <node id="3" />
    <node id="4" />
    <node id="31" />
    <node id="39" />
    <node id="2" />
    <node id="17" />
    <node id="21" />
    <node id="23" />
    <node id="25" />
    <node id="5" />
    <node id="10" />
    <node id="36" />
    <node id="33" />
    <node id="37" />
    <node id="29" />
    <node id="6" />
    <node id="9" />
    <node id="24" />
    <node id="35" />
    <node id="7" />
    <node id="8" />
    <node id="14" />
    <node id="15" />
    <node id="32" />
    <node id="18" />
    <node id="22" />
    <node id="11" />
    <node id="38" />
    <node id="27" />
    <node id="26" />
    <node id="30" />
    <node id="20" />
    <node id="34" />
    <edge source="0" target="12">
      <data key="d0">178.0</data>
      <data key="d1">3859.0</data>
    </edge>
    <edge source="0" target="13">
      <data key="d0">84.0</data>
      <data key="d1">10762.0</data>
    </edge>
    <edge source="0" target="16">
      <data key="d0">56.0</data>
      <data key="d1">10702.0</data>
    </edge>
    <edge source="0" target="19">
      <data key="d0">169.0</data>
      <data key="d1">10155.0</data>
    </edge>
    <edge source="0" target="28">
      <data key="d0">181.0</data>
      <data key="d1">6627.0</data>
    </edge>
    <edge source="12" target="0">
      <data key="d0">178.0</data>
      <data key="d1">3859.0</data>
    </edge>
    <edge source="12" target="10">
      <data key="d0">148.0</data>
      <data key="d1">7473.0</data>
    </edge>
    <edge source="12" target="19">
      <data key="d0">42.0</data>
      <data key="d1">8631.0</data>
    </edge>
    <edge source="12" target="28">
      <data key="d0">123.0</data>
      <data key="d1">6031.0</data>
    </edge>
    <edge source="13" target="0">
      <data key="d0">84.0</data>
      <data key="d1">10762.0</data>
    </edge>
    <edge source="13" target="3">
      <data key="d0">168.0</data>
      <data key="d1">5334.0</data>
    </edge>
    <edge source="13" target="5">
      <data key="d0">192.0</data>
      <data key="d1">26138.0</data>
    </edge>
    <edge source="13" target="28">
      <data key="d0">234.0</data>
      <data key="d1">5818.0</data>
    </edge>
    <edge source="13" target="36">
      <data key="d0">136.0</data>
      <data key="d1">12567.0</data>
    </edge>
    <edge source="16" target="0">
      <data key="d0">56.0</data>
      <data key="d1">10702.0</data>
    </edge>
    <edge source="16" target="19">
      <data key="d0">223.0</data>
      <data key="d1">20318.0</data>
    </edge>
    <edge source="16" target="28">
      <data key="d0">237.0</data>
      <data key="d1">12176.0</data>
    </edge>
    <edge source="16" target="36">
      <data key="d0">150.0</data>
      <data key="d1">20512.0</data>
    </edge>
    <edge source="19" target="0">
      <data key="d0">169.0</data>
      <data key="d1">10155.0</data>
    </edge>
    <edge source="19" target="10">
      <data key="d0">185.0</data>
      <data key="d1">14264.0</data>
    </edge>
    <edge source="19" target="12">
      <data key="d0">42.0</data>
      <data key="d1">8631.0</data>
    </edge>
    <edge source="19" target="16">
      <data key="d0">223.0</data>
      <data key="d1">20318.0</data>
    </edge>
    <edge source="28" target="0">
      <data key="d0">181.0</data>
      <data key="d1">6627.0</data>
    </edge>
    <edge source="28" target="12">
      <data key="d0">123.0</data>
      <data key="d1">6031.0</data>
    </edge>
    <edge source="28" target="13">
      <data key="d0">234.0</data>
      <data key="d1">5818.0</data>
    </edge>
    <edge source="28" target="16">
      <data key="d0">237.0</data>
      <data key="d1">12176.0</data>
    </edge>
    <edge source="28" target="37">
      <data key="d0">457.0</data>
      <data key="d1">11032.0</data>
    </edge>
    <edge source="1" target="3">
      <data key="d0">262.0</data>
      <data key="d1">15909.0</data>
    </edge>
    <edge source="1" target="4">
      <data key="d0">259.0</data>
      <data key="d1">9589.0</data>
    </edge>
    <edge source="1" target="31">
      <data key="d0">226.0</data>
      <data key="d1">11511.0</data>
    </edge>
    <edge source="1" target="39">
      <data key="d0">186.0</data>
      <data key="d1">16611.0</data>
    </edge>
    <edge source="3" target="1">
      <data key="d0">262.0</data>
      <data key="d1">15909.0</data>
    </edge>
    <edge source="3" target="5">
      <data key="d0">152.0</data>
      <data key="d1">23618.0</data>
    </edge>
    <edge source="3" target="10">
      <data key="d0">83.0</data>
      <data key="d1">6648.0</data>
    </edge>
    <edge source="3" target="13">
      <data key="d0">168.0</data>
      <data key="d1">5334.0</data>
    </edge>
    <edge source="3" target="36">
      <data key="d0">199.0</data>
      <data key="d1">13265.0</data>
    </edge>
    <edge source="4" target="1">
      <data key="d0">259.0</data>
      <data key="d1">9589.0</data>
    </edge>
    <edge source="4" target="31">
      <data key="d0">82.0</data>
      <data key="d1">5192.0</data>
    </edge>
    <edge source="4" target="33">
      <data key="d0">137.0</data>
      <data key="d1">14521.0</data>
    </edge>
    <edge source="4" target="37">
      <data key="d0">96.0</data>
      <data key="d1">3342.0</data>
    </edge>
    <edge source="31" target="1">
      <data key="d0">226.0</data>
      <data key="d1">11511.0</data>
    </edge>
    <edge source="31" target="4">
      <data key="d0">82.0</data>
      <data key="d1">5192.0</data>
    </edge>
    <edge source="31" target="24">
      <data key="d0">761.0</data>
      <data key="d1">19809.0</data>
    </edge>
    <edge source="31" target="33">
      <data key="d0">116.0</data>
      <data key="d1">12526.0</data>
    </edge>
    <edge source="39" target="1">
      <data key="d0">186.0</data>
      <data key="d1">16611.0</data>
    </edge>
    <edge source="39" target="9">
      <data key="d0">234.0</data>
      <data key="d1">19420.0</data>
    </edge>
    <edge source="39" target="22">
      <data key="d0">376.0</data>
      <data key="d1">14552.0</data>
    </edge>
    <edge source="39" target="26">
      <data key="d0">462.0</data>
      <data key="d1">12479.0</data>
    </edge>
    <edge source="2" target="17">
      <data key="d0">62.0</data>
      <data key="d1">9526.0</data>
    </edge>
    <edge source="2" target="21">
      <data key="d0">95.0</data>
      <data key="d1">8683.0</data>
    </edge>
    <edge source="2" target="23">
      <data key="d0">216.0</data>
      <data key="d1">5544.0</data>
    </edge>
    <edge source="2" target="25">
      <data key="d0">156.0</data>
      <data key="d1">6161.0</data>
    </edge>
    <edge source="17" target="2">
      <data key="d0">62.0</data>
      <data key="d1">9526.0</data>
    </edge>
    <edge source="17" target="21">
      <data key="d0">114.0</data>
      <data key="d1">8472.0</data>
    </edge>
    <edge source="17" target="23">
      <data key="d0">210.0</data>
      <data key="d1">14455.0</data>
    </edge>
    <edge source="17" target="25">
      <data key="d0">217.0</data>
      <data key="d1">12510.0</data>
    </edge>
    <edge source="21" target="2">
      <data key="d0">95.0</data>
      <data key="d1">8683.0</data>
    </edge>
    <edge source="21" target="17">
      <data key="d0">114.0</data>
      <data key="d1">8472.0</data>
    </edge>
    <edge source="21" target="22">
      <data key="d0">213.0</data>
      <data key="d1">8288.0</data>
    </edge>
    <edge source="21" target="23">
      <data key="d0">125.0</data>
      <data key="d1">10359.0</data>
    </edge>
    <edge source="23" target="2">
      <data key="d0">216.0</data>
      <data key="d1">5544.0</data>
    </edge>
    <edge source="23" target="17">
      <data key="d0">210.0</data>
      <data key="d1">14455.0</data>
    </edge>
    <edge source="23" target="21">
      <data key="d0">125.0</data>
      <data key="d1">10359.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">91.0</data>
      <data key="d1">14151.0</data>
    </edge>
    <edge source="23" target="25">
      <data key="d0">267.0</data>
      <data key="d1">9618.0</data>
    </edge>
    <edge source="25" target="2">
      <data key="d0">156.0</data>
      <data key="d1">6161.0</data>
    </edge>
    <edge source="25" target="17">
      <data key="d0">217.0</data>
      <data key="d1">12510.0</data>
    </edge>
    <edge source="25" target="18">
      <data key="d0">167.0</data>
      <data key="d1">12599.0</data>
    </edge>
    <edge source="25" target="23">
      <data key="d0">267.0</data>
      <data key="d1">9618.0</data>
    </edge>
    <edge source="25" target="26">
      <data key="d0">185.0</data>
      <data key="d1">10912.0</data>
    </edge>
    <edge source="5" target="3">
      <data key="d0">152.0</data>
      <data key="d1">23618.0</data>
    </edge>
    <edge source="5" target="10">
      <data key="d0">213.0</data>
      <data key="d1">27459.0</data>
    </edge>
    <edge source="5" target="13">
      <data key="d0">192.0</data>
      <data key="d1">26138.0</data>
    </edge>
    <edge source="5" target="29">
      <data key="d0">218.0</data>
      <data key="d1">18896.0</data>
    </edge>
    <edge source="5" target="36">
      <data key="d0">104.0</data>
      <data key="d1">16329.0</data>
    </edge>
    <edge source="10" target="3">
      <data key="d0">83.0</data>
      <data key="d1">6648.0</data>
    </edge>
    <edge source="10" target="5">
      <data key="d0">213.0</data>
      <data key="d1">27459.0</data>
    </edge>
    <edge source="10" target="12">
      <data key="d0">148.0</data>
      <data key="d1">7473.0</data>
    </edge>
    <edge source="10" target="19">
      <data key="d0">185.0</data>
      <data key="d1">14264.0</data>
    </edge>
    <edge source="36" target="3">
      <data key="d0">199.0</data>
      <data key="d1">13265.0</data>
    </edge>
    <edge source="36" target="5">
      <data key="d0">104.0</data>
      <data key="d1">16329.0</data>
    </edge>
    <edge source="36" target="13">
      <data key="d0">136.0</data>
      <data key="d1">12567.0</data>
    </edge>
    <edge source="36" target="16">
      <data key="d0">150.0</data>
      <data key="d1">20512.0</data>
    </edge>
    <edge source="36" target="24">
      <data key="d0">323.0</data>
      <data key="d1">19022.0</data>
    </edge>
    <edge source="33" target="4">
      <data key="d0">137.0</data>
      <data key="d1">14521.0</data>
    </edge>
    <edge source="33" target="20">
      <data key="d0">346.0</data>
      <data key="d1">10878.0</data>
    </edge>
    <edge source="33" target="31">
      <data key="d0">116.0</data>
      <data key="d1">12526.0</data>
    </edge>
    <edge source="33" target="37">
      <data key="d0">41.0</data>
      <data key="d1">12938.0</data>
    </edge>
    <edge source="33" target="38">
      <data key="d0">244.0</data>
      <data key="d1">9489.0</data>
    </edge>
    <edge source="37" target="4">
      <data key="d0">96.0</data>
      <data key="d1">3342.0</data>
    </edge>
    <edge source="37" target="24">
      <data key="d0">814.0</data>
      <data key="d1">18528.0</data>
    </edge>
    <edge source="37" target="28">
      <data key="d0">457.0</data>
      <data key="d1">11032.0</data>
    </edge>
    <edge source="37" target="33">
      <data key="d0">41.0</data>
      <data key="d1">12938.0</data>
    </edge>
    <edge source="29" target="5">
      <data key="d0">218.0</data>
      <data key="d1">18896.0</data>
    </edge>
    <edge source="29" target="6">
      <data key="d0">325.0</data>
      <data key="d1">11365.0</data>
    </edge>
    <edge source="29" target="9">
      <data key="d0">208.0</data>
      <data key="d1">13829.0</data>
    </edge>
    <edge source="29" target="35">
      <data key="d0">46.0</data>
      <data key="d1">8809.0</data>
    </edge>
    <edge source="29" target="38">
      <data key="d0">560.0</data>
      <data key="d1">37594.0</data>
    </edge>
    <edge source="6" target="9">
      <data key="d0">321.0</data>
      <data key="d1">10002.0</data>
    </edge>
    <edge source="6" target="24">
      <data key="d0">253.0</data>
      <data key="d1">14957.0</data>
    </edge>
    <edge source="6" target="29">
      <data key="d0">325.0</data>
      <data key="d1">11365.0</data>
    </edge>
    <edge source="6" target="35">
      <data key="d0">280.0</data>
      <data key="d1">19526.0</data>
    </edge>
    <edge source="9" target="6">
      <data key="d0">321.0</data>
      <data key="d1">10002.0</data>
    </edge>
    <edge source="9" target="22">
      <data key="d0">182.0</data>
      <data key="d1">7178.0</data>
    </edge>
    <edge source="9" target="29">
      <data key="d0">208.0</data>
      <data key="d1">13829.0</data>
    </edge>
    <edge source="9" target="35">
      <data key="d0">201.0</data>
      <data key="d1">18233.0</data>
    </edge>
    <edge source="9" target="39">
      <data key="d0">234.0</data>
      <data key="d1">19420.0</data>
    </edge>
    <edge source="24" target="6">
      <data key="d0">253.0</data>
      <data key="d1">14957.0</data>
    </edge>
    <edge source="24" target="31">
      <data key="d0">761.0</data>
      <data key="d1">19809.0</data>
    </edge>
    <edge source="24" target="35">
      <data key="d0">373.0</data>
      <data key="d1">12269.0</data>
    </edge>
    <edge source="24" target="36">
      <data key="d0">323.0</data>
      <data key="d1">19022.0</data>
    </edge>
    <edge source="24" target="37">
      <data key="d0">814.0</data>
      <data key="d1">18528.0</data>
    </edge>
    <edge source="35" target="6">
      <data key="d0">280.0</data>
      <data key="d1">19526.0</data>
    </edge>
    <edge source="35" target="9">
      <data key="d0">201.0</data>
      <data key="d1">18233.0</data>
    </edge>
    <edge source="35" target="24">
      <data key="d0">373.0</data>
      <data key="d1">12269.0</data>
    </edge>
    <edge source="35" target="29">
      <data key="d0">46.0</data>
      <data key="d1">8809.0</data>
    </edge>
    <edge source="35" target="30">
      <data key="d0">605.0</data>
      <data key="d1">23122.0</data>
    </edge>
    <edge source="7" target="8">
      <data key="d0">41.0</data>
      <data key="d1">10830.0</data>
    </edge>
    <edge source="7" target="14">
      <data key="d0">159.0</data>
      <data key="d1">7100.0</data>
    </edge>
    <edge source="7" target="15">
      <data key="d0">144.0</data>
      <data key="d1">14016.0</data>
    </edge>
    <edge source="7" target="32">
      <data key="d0">164.0</data>
      <data key="d1">5707.0</data>
    </edge>
    <edge source="8" target="7">
      <data key="d0">41.0</data>
      <data key="d1">10830.0</data>
    </edge>
    <edge source="8" target="14">
      <data key="d0">164.0</data>
      <data key="d1">7355.0</data>
    </edge>
    <edge source="8" target="15">
      <data key="d0">154.0</data>
      <data key="d1">14808.0</data>
    </edge>
    <edge source="8" target="18">
      <data key="d0">182.0</data>
      <data key="d1">13189.0</data>
    </edge>
    <edge source="8" target="32">
      <data key="d0">164.0</data>
      <data key="d1">14947.0</data>
    </edge>
    <edge source="14" target="7">
      <data key="d0">159.0</data>
      <data key="d1">7100.0</data>
    </edge>
    <edge source="14" target="8">
      <data key="d0">164.0</data>
      <data key="d1">7355.0</data>
    </edge>
    <edge source="14" target="11">
      <data key="d0">161.0</data>
      <data key="d1">8387.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">26.0</data>
      <data key="d1">8580.0</data>
    </edge>
    <edge source="15" target="7">
      <data key="d0">144.0</data>
      <data key="d1">14016.0</data>
    </edge>
    <edge source="15" target="8">
      <data key="d0">154.0</data>
      <data key="d1">14808.0</data>
    </edge>
    <edge source="15" target="11">
      <data key="d0">186.0</data>
      <data key="d1">11710.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">26.0</data>
      <data key="d1">8580.0</data>
    </edge>
    <edge source="15" target="27">
      <data key="d0">72.0</data>
      <data key="d1">6364.0</data>
    </edge>
    <edge source="32" target="7">
      <data key="d0">164.0</data>
      <data key="d1">5707.0</data>
    </edge>
    <edge source="32" target="8">
      <data key="d0">164.0</data>
      <data key="d1">14947.0</data>
    </edge>
    <edge source="32" target="11">
      <data key="d0">140.0</data>
      <data key="d1">8911.0</data>
    </edge>
    <edge source="32" target="34">
      <data key="d0">278.0</data>
      <data key="d1">6859.0</data>
    </edge>
    <edge source="18" target="8">
      <data key="d0">182.0</data>
      <data key="d1">13189.0</data>
    </edge>
    <edge source="18" target="25">
      <data key="d0">167.0</data>
      <data key="d1">12599.0</data>
    </edge>
    <edge source="18" target="26">
      <data key="d0">136.0</data>
      <data key="d1">13309.0</data>
    </edge>
    <edge source="18" target="27">
      <data key="d0">392.0</data>
      <data key="d1">27331.0</data>
    </edge>
    <edge source="18" target="30">
      <data key="d0">95.0</data>
      <data key="d1">13393.0</data>
    </edge>
    <edge source="22" target="9">
      <data key="d0">182.0</data>
      <data key="d1">7178.0</data>
    </edge>
    <edge source="22" target="21">
      <data key="d0">213.0</data>
      <data key="d1">8288.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">91.0</data>
      <data key="d1">14151.0</data>
    </edge>
    <edge source="22" target="39">
      <data key="d0">376.0</data>
      <data key="d1">14552.0</data>
    </edge>
    <edge source="11" target="14">
      <data key="d0">161.0</data>
      <data key="d1">8387.0</data>
    </edge>
    <edge source="11" target="15">
      <data key="d0">186.0</data>
      <data key="d1">11710.0</data>
    </edge>
    <edge source="11" target="32">
      <data key="d0">140.0</data>
      <data key="d1">8911.0</data>
    </edge>
    <edge source="11" target="38">
      <data key="d0">175.0</data>
      <data key="d1">14558.0</data>
    </edge>
    <edge source="38" target="11">
      <data key="d0">175.0</data>
      <data key="d1">14558.0</data>
    </edge>
    <edge source="38" target="20">
      <data key="d0">217.0</data>
      <data key="d1">6601.0</data>
    </edge>
    <edge source="38" target="27">
      <data key="d0">299.0</data>
      <data key="d1">27487.0</data>
    </edge>
    <edge source="38" target="29">
      <data key="d0">560.0</data>
      <data key="d1">37594.0</data>
    </edge>
    <edge source="38" target="33">
      <data key="d0">244.0</data>
      <data key="d1">9489.0</data>
    </edge>
    <edge source="27" target="15">
      <data key="d0">72.0</data>
      <data key="d1">6364.0</data>
    </edge>
    <edge source="27" target="18">
      <data key="d0">392.0</data>
      <data key="d1">27331.0</data>
    </edge>
    <edge source="27" target="20">
      <data key="d0">230.0</data>
      <data key="d1">27412.0</data>
    </edge>
    <edge source="27" target="38">
      <data key="d0">299.0</data>
      <data key="d1">27487.0</data>
    </edge>
    <edge source="26" target="18">
      <data key="d0">136.0</data>
      <data key="d1">13309.0</data>
    </edge>
    <edge source="26" target="25">
      <data key="d0">185.0</data>
      <data key="d1">10912.0</data>
    </edge>
    <edge source="26" target="30">
      <data key="d0">67.0</data>
      <data key="d1">4903.0</data>
    </edge>
    <edge source="26" target="34">
      <data key="d0">554.0</data>
      <data key="d1">17925.0</data>
    </edge>
    <edge source="26" target="39">
      <data key="d0">462.0</data>
      <data key="d1">12479.0</data>
    </edge>
    <edge source="30" target="18">
      <data key="d0">95.0</data>
      <data key="d1">13393.0</data>
    </edge>
    <edge source="30" target="26">
      <data key="d0">67.0</data>
      <data key="d1">4903.0</data>
    </edge>
    <edge source="30" target="34">
      <data key="d0">608.0</data>
      <data key="d1">16868.0</data>
    </edge>
    <edge source="30" target="35">
      <data key="d0">605.0</data>
      <data key="d1">23122.0</data>
    </edge>
    <edge source="20" target="27">
      <data key="d0">230.0</data>
      <data key="d1">27412.0</data>
    </edge>
    <edge source="20" target="33">
      <data key="d0">346.0</data>
      <data key="d1">10878.0</data>
    </edge>
    <edge source="20" target="34">
      <data key="d0">34.0</data>
      <data key="d1">13900.0</data>
    </edge>
    <edge source="20" target="38">
      <data key="d0">217.0</data>
      <data key="d1">6601.0</data>
    </edge>
    <edge source="34" target="20">
      <data key="d0">34.0</data>
      <data key="d1">13900.0</data>
    </edge>
    <edge source="34" target="26">
      <data key="d0">554.0</data>
      <data key="d1">17925.0</data>
    </edge>
    <edge source="34" target="30">
      <data key="d0">608.0</data>
      <data key="d1">16868.0</data>
    </edge>
    <edge source="34" target="32">
      <data key="d0">278.0</data>
      <data key="d1">6859.0</data>
    </edge>
  </graph>
</graphml>
