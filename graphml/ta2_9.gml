<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="30" />
    <node id="2" />
    <node id="29" />
    <node id="4" />
    <node id="50" />
    <node id="24" />
    <node id="7" />
    <node id="34" />
    <node id="44" />
    <node id="8" />
    <node id="9" />
    <node id="41" />
    <node id="1" />
    <node id="12" />
    <node id="22" />
    <node id="61" />
    <node id="13" />
    <node id="21" />
    <node id="14" />
    <node id="58" />
    <node id="15" />
    <node id="6" />
    <node id="16" />
    <node id="18" />
    <node id="5" />
    <node id="17" />
    <node id="63" />
    <node id="43" />
    <node id="20" />
    <node id="57" />
    <node id="40" />
    <node id="23" />
    <node id="47" />
    <node id="25" />
    <node id="60" />
    <node id="26" />
    <node id="48" />
    <node id="27" />
    <node id="31" />
    <node id="28" />
    <node id="32" />
    <node id="37" />
    <node id="33" />
    <node id="54" />
    <node id="11" />
    <node id="10" />
    <node id="35" />
    <node id="49" />
    <node id="36" />
    <node id="53" />
    <node id="45" />
    <node id="19" />
    <node id="38" />
    <node id="39" />
    <node id="42" />
    <node id="55" />
    <node id="46" />
    <node id="3" />
    <node id="51" />
    <node id="52" />
    <node id="56" />
    <node id="59" />
    <node id="62" />
    <node id="64" />
    <edge source="0" target="30">
      <data key="d0">162160.0</data>
      <data key="d1">4662.0</data>
    </edge>
    <edge source="0" target="13">
      <data key="d0">132676.0</data>
      <data key="d1">3828.0</data>
    </edge>
    <edge source="0" target="42">
      <data key="d0">191644.0</data>
      <data key="d1">4016.0</data>
    </edge>
    <edge source="30" target="0">
      <data key="d0">162160.0</data>
      <data key="d1">4662.0</data>
    </edge>
    <edge source="30" target="27">
      <data key="d0">44225.5</data>
      <data key="d1">9242.0</data>
    </edge>
    <edge source="30" target="31">
      <data key="d0">58967.3</data>
      <data key="d1">5179.0</data>
    </edge>
    <edge source="30" target="38">
      <data key="d0">162160.0</data>
      <data key="d1">6297.0</data>
    </edge>
    <edge source="30" target="13">
      <data key="d0">280095.0</data>
      <data key="d1">7506.0</data>
    </edge>
    <edge source="2" target="29">
      <data key="d0">250611.0</data>
      <data key="d1">14287.0</data>
    </edge>
    <edge source="2" target="51">
      <data key="d0">73709.1</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="2" target="27">
      <data key="d0">339062.0</data>
      <data key="d1">10090.0</data>
    </edge>
    <edge source="29" target="2">
      <data key="d0">250611.0</data>
      <data key="d1">14287.0</data>
    </edge>
    <edge source="29" target="8">
      <data key="d0">117935.0</data>
      <data key="d1">4901.0</data>
    </edge>
    <edge source="29" target="9">
      <data key="d0">353804.0</data>
      <data key="d1">6963.0</data>
    </edge>
    <edge source="29" target="27">
      <data key="d0">1031930.0</data>
      <data key="d1">9437.0</data>
    </edge>
    <edge source="29" target="28">
      <data key="d0">88450.9</data>
      <data key="d1">2759.0</data>
    </edge>
    <edge source="29" target="32">
      <data key="d0">265353.0</data>
      <data key="d1">7169.0</data>
    </edge>
    <edge source="29" target="51">
      <data key="d0">221127.0</data>
      <data key="d1">16717.0</data>
    </edge>
    <edge source="29" target="57">
      <data key="d0">265353.0</data>
      <data key="d1">14596.0</data>
    </edge>
    <edge source="29" target="39">
      <data key="d0">1238310.0</data>
      <data key="d1">20196.0</data>
    </edge>
    <edge source="29" target="62">
      <data key="d0">398029.0</data>
      <data key="d1">11673.0</data>
    </edge>
    <edge source="4" target="50">
      <data key="d0">147418.0</data>
      <data key="d1">4368.0</data>
    </edge>
    <edge source="4" target="24">
      <data key="d0">147418.0</data>
      <data key="d1">4686.0</data>
    </edge>
    <edge source="50" target="4">
      <data key="d0">147418.0</data>
      <data key="d1">4368.0</data>
    </edge>
    <edge source="50" target="23">
      <data key="d0">398029.0</data>
      <data key="d1">5981.0</data>
    </edge>
    <edge source="50" target="54">
      <data key="d0">280095.0</data>
      <data key="d1">15301.0</data>
    </edge>
    <edge source="24" target="4">
      <data key="d0">147418.0</data>
      <data key="d1">4686.0</data>
    </edge>
    <edge source="24" target="60">
      <data key="d0">117935.0</data>
      <data key="d1">3253.0</data>
    </edge>
    <edge source="24" target="37">
      <data key="d0">250611.0</data>
      <data key="d1">3812.0</data>
    </edge>
    <edge source="24" target="62">
      <data key="d0">280095.0</data>
      <data key="d1">10823.0</data>
    </edge>
    <edge source="7" target="34">
      <data key="d0">884509.0</data>
      <data key="d1">9278.0</data>
    </edge>
    <edge source="7" target="44">
      <data key="d0">1267800.0</data>
      <data key="d1">8786.0</data>
    </edge>
    <edge source="34" target="7">
      <data key="d0">884509.0</data>
      <data key="d1">9278.0</data>
    </edge>
    <edge source="34" target="11">
      <data key="d0">456996.0</data>
      <data key="d1">6203.0</data>
    </edge>
    <edge source="34" target="10">
      <data key="d0">206385.0</data>
      <data key="d1">3828.0</data>
    </edge>
    <edge source="34" target="44">
      <data key="d0">530706.0</data>
      <data key="d1">16456.0</data>
    </edge>
    <edge source="34" target="58">
      <data key="d0">2004890.0</data>
      <data key="d1">8112.0</data>
    </edge>
    <edge source="44" target="7">
      <data key="d0">1267800.0</data>
      <data key="d1">8786.0</data>
    </edge>
    <edge source="44" target="18">
      <data key="d0">235869.0</data>
      <data key="d1">5456.0</data>
    </edge>
    <edge source="44" target="41">
      <data key="d0">368546.0</data>
      <data key="d1">16400.0</data>
    </edge>
    <edge source="44" target="34">
      <data key="d0">530706.0</data>
      <data key="d1">16456.0</data>
    </edge>
    <edge source="44" target="46">
      <data key="d0">1592120.0</data>
      <data key="d1">11647.0</data>
    </edge>
    <edge source="44" target="58">
      <data key="d0">515964.0</data>
      <data key="d1">20044.0</data>
    </edge>
    <edge source="8" target="29">
      <data key="d0">117935.0</data>
      <data key="d1">4901.0</data>
    </edge>
    <edge source="8" target="20">
      <data key="d0">58967.3</data>
      <data key="d1">5805.0</data>
    </edge>
    <edge source="8" target="32">
      <data key="d0">162160.0</data>
      <data key="d1">7021.0</data>
    </edge>
    <edge source="9" target="29">
      <data key="d0">353804.0</data>
      <data key="d1">6963.0</data>
    </edge>
    <edge source="9" target="41">
      <data key="d0">530706.0</data>
      <data key="d1">4301.0</data>
    </edge>
    <edge source="9" target="1">
      <data key="d0">132676.0</data>
      <data key="d1">5819.0</data>
    </edge>
    <edge source="9" target="32">
      <data key="d0">280095.0</data>
      <data key="d1">8889.0</data>
    </edge>
    <edge source="9" target="27">
      <data key="d0">1326760.0</data>
      <data key="d1">13918.0</data>
    </edge>
    <edge source="9" target="46">
      <data key="d0">913993.0</data>
      <data key="d1">7011.0</data>
    </edge>
    <edge source="41" target="9">
      <data key="d0">530706.0</data>
      <data key="d1">4301.0</data>
    </edge>
    <edge source="41" target="44">
      <data key="d0">368546.0</data>
      <data key="d1">16400.0</data>
    </edge>
    <edge source="41" target="46">
      <data key="d0">766574.0</data>
      <data key="d1">7262.0</data>
    </edge>
    <edge source="1" target="9">
      <data key="d0">132676.0</data>
      <data key="d1">5819.0</data>
    </edge>
    <edge source="1" target="38">
      <data key="d0">235869.0</data>
      <data key="d1">12207.0</data>
    </edge>
    <edge source="1" target="53">
      <data key="d0">103193.0</data>
      <data key="d1">4469.0</data>
    </edge>
    <edge source="12" target="22">
      <data key="d0">162160.0</data>
      <data key="d1">6512.0</data>
    </edge>
    <edge source="12" target="61">
      <data key="d0">339062.0</data>
      <data key="d1">8782.0</data>
    </edge>
    <edge source="12" target="42">
      <data key="d0">250611.0</data>
      <data key="d1">11107.0</data>
    </edge>
    <edge source="12" target="15">
      <data key="d0">899251.0</data>
      <data key="d1">13080.0</data>
    </edge>
    <edge source="12" target="39">
      <data key="d0">1238310.0</data>
      <data key="d1">21352.0</data>
    </edge>
    <edge source="12" target="64">
      <data key="d0">737091.0</data>
      <data key="d1">13896.0</data>
    </edge>
    <edge source="22" target="12">
      <data key="d0">162160.0</data>
      <data key="d1">6512.0</data>
    </edge>
    <edge source="22" target="40">
      <data key="d0">294836.0</data>
      <data key="d1">4314.0</data>
    </edge>
    <edge source="61" target="12">
      <data key="d0">339062.0</data>
      <data key="d1">8782.0</data>
    </edge>
    <edge source="61" target="15">
      <data key="d0">560189.0</data>
      <data key="d1">6841.0</data>
    </edge>
    <edge source="13" target="21">
      <data key="d0">103193.0</data>
      <data key="d1">6398.0</data>
    </edge>
    <edge source="13" target="0">
      <data key="d0">132676.0</data>
      <data key="d1">3828.0</data>
    </edge>
    <edge source="13" target="30">
      <data key="d0">280095.0</data>
      <data key="d1">7506.0</data>
    </edge>
    <edge source="13" target="40">
      <data key="d0">221127.0</data>
      <data key="d1">4144.0</data>
    </edge>
    <edge source="21" target="13">
      <data key="d0">103193.0</data>
      <data key="d1">6398.0</data>
    </edge>
    <edge source="21" target="40">
      <data key="d0">117935.0</data>
      <data key="d1">4519.0</data>
    </edge>
    <edge source="21" target="52">
      <data key="d0">117935.0</data>
      <data key="d1">3538.0</data>
    </edge>
    <edge source="14" target="58">
      <data key="d0">501222.0</data>
      <data key="d1">8927.0</data>
    </edge>
    <edge source="14" target="25">
      <data key="d0">147418.0</data>
      <data key="d1">6218.0</data>
    </edge>
    <edge source="14" target="23">
      <data key="d0">1238310.0</data>
      <data key="d1">9519.0</data>
    </edge>
    <edge source="58" target="14">
      <data key="d0">501222.0</data>
      <data key="d1">8927.0</data>
    </edge>
    <edge source="58" target="44">
      <data key="d0">515964.0</data>
      <data key="d1">20044.0</data>
    </edge>
    <edge source="58" target="3">
      <data key="d0">103193.0</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="58" target="56">
      <data key="d0">250611.0</data>
      <data key="d1">7046.0</data>
    </edge>
    <edge source="58" target="34">
      <data key="d0">2004890.0</data>
      <data key="d1">8112.0</data>
    </edge>
    <edge source="58" target="59">
      <data key="d0">855025.0</data>
      <data key="d1">8637.0</data>
    </edge>
    <edge source="58" target="62">
      <data key="d0">2653530.0</data>
      <data key="d1">11618.0</data>
    </edge>
    <edge source="15" target="6">
      <data key="d0">162160.0</data>
      <data key="d1">5021.0</data>
    </edge>
    <edge source="15" target="61">
      <data key="d0">560189.0</data>
      <data key="d1">6841.0</data>
    </edge>
    <edge source="15" target="12">
      <data key="d0">899251.0</data>
      <data key="d1">13080.0</data>
    </edge>
    <edge source="15" target="39">
      <data key="d0">339062.0</data>
      <data key="d1">10512.0</data>
    </edge>
    <edge source="6" target="15">
      <data key="d0">162160.0</data>
      <data key="d1">5021.0</data>
    </edge>
    <edge source="6" target="39">
      <data key="d0">176902.0</data>
      <data key="d1">7524.0</data>
    </edge>
    <edge source="16" target="18">
      <data key="d0">250611.0</data>
      <data key="d1">5092.0</data>
    </edge>
    <edge source="16" target="5">
      <data key="d0">265353.0</data>
      <data key="d1">3936.0</data>
    </edge>
    <edge source="16" target="26">
      <data key="d0">324320.0</data>
      <data key="d1">8402.0</data>
    </edge>
    <edge source="18" target="16">
      <data key="d0">250611.0</data>
      <data key="d1">5092.0</data>
    </edge>
    <edge source="18" target="44">
      <data key="d0">235869.0</data>
      <data key="d1">5456.0</data>
    </edge>
    <edge source="5" target="16">
      <data key="d0">265353.0</data>
      <data key="d1">3936.0</data>
    </edge>
    <edge source="5" target="62">
      <data key="d0">442254.0</data>
      <data key="d1">4140.0</data>
    </edge>
    <edge source="17" target="63">
      <data key="d0">206385.0</data>
      <data key="d1">4000.0</data>
    </edge>
    <edge source="17" target="43">
      <data key="d0">132676.0</data>
      <data key="d1">6603.0</data>
    </edge>
    <edge source="63" target="17">
      <data key="d0">206385.0</data>
      <data key="d1">4000.0</data>
    </edge>
    <edge source="63" target="33">
      <data key="d0">368546.0</data>
      <data key="d1">9925.0</data>
    </edge>
    <edge source="43" target="17">
      <data key="d0">132676.0</data>
      <data key="d1">6603.0</data>
    </edge>
    <edge source="43" target="55">
      <data key="d0">221127.0</data>
      <data key="d1">6303.0</data>
    </edge>
    <edge source="20" target="57">
      <data key="d0">132676.0</data>
      <data key="d1">4892.0</data>
    </edge>
    <edge source="20" target="8">
      <data key="d0">58967.3</data>
      <data key="d1">5805.0</data>
    </edge>
    <edge source="57" target="20">
      <data key="d0">132676.0</data>
      <data key="d1">4892.0</data>
    </edge>
    <edge source="57" target="29">
      <data key="d0">265353.0</data>
      <data key="d1">14596.0</data>
    </edge>
    <edge source="57" target="19">
      <data key="d0">73709.1</data>
      <data key="d1">3812.0</data>
    </edge>
    <edge source="40" target="21">
      <data key="d0">117935.0</data>
      <data key="d1">4519.0</data>
    </edge>
    <edge source="40" target="22">
      <data key="d0">294836.0</data>
      <data key="d1">4314.0</data>
    </edge>
    <edge source="40" target="13">
      <data key="d0">221127.0</data>
      <data key="d1">4144.0</data>
    </edge>
    <edge source="23" target="50">
      <data key="d0">398029.0</data>
      <data key="d1">5981.0</data>
    </edge>
    <edge source="23" target="47">
      <data key="d0">73709.1</data>
      <data key="d1">4016.0</data>
    </edge>
    <edge source="23" target="25">
      <data key="d0">486480.0</data>
      <data key="d1">5315.0</data>
    </edge>
    <edge source="23" target="14">
      <data key="d0">1238310.0</data>
      <data key="d1">9519.0</data>
    </edge>
    <edge source="23" target="54">
      <data key="d0">1356250.0</data>
      <data key="d1">20986.0</data>
    </edge>
    <edge source="47" target="23">
      <data key="d0">73709.1</data>
      <data key="d1">4016.0</data>
    </edge>
    <edge source="47" target="60">
      <data key="d0">117935.0</data>
      <data key="d1">3206.0</data>
    </edge>
    <edge source="25" target="23">
      <data key="d0">486480.0</data>
      <data key="d1">5315.0</data>
    </edge>
    <edge source="25" target="14">
      <data key="d0">147418.0</data>
      <data key="d1">6218.0</data>
    </edge>
    <edge source="25" target="48">
      <data key="d0">162160.0</data>
      <data key="d1">4455.0</data>
    </edge>
    <edge source="60" target="24">
      <data key="d0">117935.0</data>
      <data key="d1">3253.0</data>
    </edge>
    <edge source="60" target="47">
      <data key="d0">117935.0</data>
      <data key="d1">3206.0</data>
    </edge>
    <edge source="26" target="48">
      <data key="d0">191644.0</data>
      <data key="d1">4427.0</data>
    </edge>
    <edge source="26" target="16">
      <data key="d0">324320.0</data>
      <data key="d1">8402.0</data>
    </edge>
    <edge source="26" target="62">
      <data key="d0">235869.0</data>
      <data key="d1">5162.0</data>
    </edge>
    <edge source="48" target="26">
      <data key="d0">191644.0</data>
      <data key="d1">4427.0</data>
    </edge>
    <edge source="48" target="3">
      <data key="d0">147418.0</data>
      <data key="d1">3650.0</data>
    </edge>
    <edge source="48" target="25">
      <data key="d0">162160.0</data>
      <data key="d1">4455.0</data>
    </edge>
    <edge source="27" target="31">
      <data key="d0">73709.1</data>
      <data key="d1">4920.0</data>
    </edge>
    <edge source="27" target="29">
      <data key="d0">1031930.0</data>
      <data key="d1">9437.0</data>
    </edge>
    <edge source="27" target="28">
      <data key="d0">103193.0</data>
      <data key="d1">7306.0</data>
    </edge>
    <edge source="27" target="30">
      <data key="d0">44225.5</data>
      <data key="d1">9242.0</data>
    </edge>
    <edge source="27" target="52">
      <data key="d0">235869.0</data>
      <data key="d1">11486.0</data>
    </edge>
    <edge source="27" target="2">
      <data key="d0">339062.0</data>
      <data key="d1">10090.0</data>
    </edge>
    <edge source="27" target="9">
      <data key="d0">1326760.0</data>
      <data key="d1">13918.0</data>
    </edge>
    <edge source="27" target="38">
      <data key="d0">191644.0</data>
      <data key="d1">7767.0</data>
    </edge>
    <edge source="27" target="45">
      <data key="d0">855026.0</data>
      <data key="d1">8945.0</data>
    </edge>
    <edge source="27" target="54">
      <data key="d0">1945920.0</data>
      <data key="d1">18663.0</data>
    </edge>
    <edge source="31" target="27">
      <data key="d0">73709.1</data>
      <data key="d1">4920.0</data>
    </edge>
    <edge source="31" target="30">
      <data key="d0">58967.3</data>
      <data key="d1">5179.0</data>
    </edge>
    <edge source="28" target="27">
      <data key="d0">103193.0</data>
      <data key="d1">7306.0</data>
    </edge>
    <edge source="28" target="29">
      <data key="d0">88450.9</data>
      <data key="d1">2759.0</data>
    </edge>
    <edge source="32" target="8">
      <data key="d0">162160.0</data>
      <data key="d1">7021.0</data>
    </edge>
    <edge source="32" target="9">
      <data key="d0">280095.0</data>
      <data key="d1">8889.0</data>
    </edge>
    <edge source="32" target="37">
      <data key="d0">206385.0</data>
      <data key="d1">9140.0</data>
    </edge>
    <edge source="32" target="29">
      <data key="d0">265353.0</data>
      <data key="d1">7169.0</data>
    </edge>
    <edge source="32" target="62">
      <data key="d0">147418.0</data>
      <data key="d1">5220.0</data>
    </edge>
    <edge source="37" target="32">
      <data key="d0">206385.0</data>
      <data key="d1">9140.0</data>
    </edge>
    <edge source="37" target="24">
      <data key="d0">250611.0</data>
      <data key="d1">3812.0</data>
    </edge>
    <edge source="37" target="19">
      <data key="d0">147418.0</data>
      <data key="d1">5441.0</data>
    </edge>
    <edge source="33" target="54">
      <data key="d0">427513.0</data>
      <data key="d1">4123.0</data>
    </edge>
    <edge source="33" target="63">
      <data key="d0">368546.0</data>
      <data key="d1">9925.0</data>
    </edge>
    <edge source="54" target="33">
      <data key="d0">427513.0</data>
      <data key="d1">4123.0</data>
    </edge>
    <edge source="54" target="51">
      <data key="d0">339062.0</data>
      <data key="d1">6574.0</data>
    </edge>
    <edge source="54" target="55">
      <data key="d0">250611.0</data>
      <data key="d1">2907.0</data>
    </edge>
    <edge source="54" target="50">
      <data key="d0">280095.0</data>
      <data key="d1">15301.0</data>
    </edge>
    <edge source="54" target="23">
      <data key="d0">1356250.0</data>
      <data key="d1">20986.0</data>
    </edge>
    <edge source="54" target="27">
      <data key="d0">1945920.0</data>
      <data key="d1">18663.0</data>
    </edge>
    <edge source="11" target="34">
      <data key="d0">456996.0</data>
      <data key="d1">6203.0</data>
    </edge>
    <edge source="11" target="59">
      <data key="d0">132676.0</data>
      <data key="d1">4415.0</data>
    </edge>
    <edge source="10" target="34">
      <data key="d0">206385.0</data>
      <data key="d1">3828.0</data>
    </edge>
    <edge source="35" target="49">
      <data key="d0">88450.9</data>
      <data key="d1">3635.0</data>
    </edge>
    <edge source="35" target="52">
      <data key="d0">132676.0</data>
      <data key="d1">3962.0</data>
    </edge>
    <edge source="49" target="35">
      <data key="d0">88450.9</data>
      <data key="d1">3635.0</data>
    </edge>
    <edge source="49" target="39">
      <data key="d0">88450.9</data>
      <data key="d1">6389.0</data>
    </edge>
    <edge source="36" target="53">
      <data key="d0">162160.0</data>
      <data key="d1">3015.0</data>
    </edge>
    <edge source="36" target="45">
      <data key="d0">176902.0</data>
      <data key="d1">2319.0</data>
    </edge>
    <edge source="53" target="36">
      <data key="d0">162160.0</data>
      <data key="d1">3015.0</data>
    </edge>
    <edge source="53" target="1">
      <data key="d0">103193.0</data>
      <data key="d1">4469.0</data>
    </edge>
    <edge source="45" target="36">
      <data key="d0">176902.0</data>
      <data key="d1">2319.0</data>
    </edge>
    <edge source="45" target="64">
      <data key="d0">1179350.0</data>
      <data key="d1">15101.0</data>
    </edge>
    <edge source="45" target="38">
      <data key="d0">663382.0</data>
      <data key="d1">4301.0</data>
    </edge>
    <edge source="45" target="27">
      <data key="d0">855026.0</data>
      <data key="d1">8945.0</data>
    </edge>
    <edge source="19" target="37">
      <data key="d0">147418.0</data>
      <data key="d1">5441.0</data>
    </edge>
    <edge source="19" target="57">
      <data key="d0">73709.1</data>
      <data key="d1">3812.0</data>
    </edge>
    <edge source="38" target="30">
      <data key="d0">162160.0</data>
      <data key="d1">6297.0</data>
    </edge>
    <edge source="38" target="1">
      <data key="d0">235869.0</data>
      <data key="d1">12207.0</data>
    </edge>
    <edge source="38" target="27">
      <data key="d0">191644.0</data>
      <data key="d1">7767.0</data>
    </edge>
    <edge source="38" target="45">
      <data key="d0">663382.0</data>
      <data key="d1">4301.0</data>
    </edge>
    <edge source="39" target="6">
      <data key="d0">176902.0</data>
      <data key="d1">7524.0</data>
    </edge>
    <edge source="39" target="49">
      <data key="d0">88450.9</data>
      <data key="d1">6389.0</data>
    </edge>
    <edge source="39" target="12">
      <data key="d0">1238310.0</data>
      <data key="d1">21352.0</data>
    </edge>
    <edge source="39" target="15">
      <data key="d0">339062.0</data>
      <data key="d1">10512.0</data>
    </edge>
    <edge source="39" target="29">
      <data key="d0">1238310.0</data>
      <data key="d1">20196.0</data>
    </edge>
    <edge source="42" target="0">
      <data key="d0">191644.0</data>
      <data key="d1">4016.0</data>
    </edge>
    <edge source="42" target="12">
      <data key="d0">250611.0</data>
      <data key="d1">11107.0</data>
    </edge>
    <edge source="42" target="64">
      <data key="d0">117935.0</data>
      <data key="d1">4052.0</data>
    </edge>
    <edge source="55" target="43">
      <data key="d0">221127.0</data>
      <data key="d1">6303.0</data>
    </edge>
    <edge source="55" target="54">
      <data key="d0">250611.0</data>
      <data key="d1">2907.0</data>
    </edge>
    <edge source="46" target="41">
      <data key="d0">766574.0</data>
      <data key="d1">7262.0</data>
    </edge>
    <edge source="46" target="44">
      <data key="d0">1592120.0</data>
      <data key="d1">11647.0</data>
    </edge>
    <edge source="46" target="9">
      <data key="d0">913993.0</data>
      <data key="d1">7011.0</data>
    </edge>
    <edge source="3" target="48">
      <data key="d0">147418.0</data>
      <data key="d1">3650.0</data>
    </edge>
    <edge source="3" target="58">
      <data key="d0">103193.0</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="51" target="2">
      <data key="d0">73709.1</data>
      <data key="d1">4342.0</data>
    </edge>
    <edge source="51" target="29">
      <data key="d0">221127.0</data>
      <data key="d1">16717.0</data>
    </edge>
    <edge source="51" target="54">
      <data key="d0">339062.0</data>
      <data key="d1">6574.0</data>
    </edge>
    <edge source="52" target="27">
      <data key="d0">235869.0</data>
      <data key="d1">11486.0</data>
    </edge>
    <edge source="52" target="21">
      <data key="d0">117935.0</data>
      <data key="d1">3538.0</data>
    </edge>
    <edge source="52" target="35">
      <data key="d0">132676.0</data>
      <data key="d1">3962.0</data>
    </edge>
    <edge source="56" target="58">
      <data key="d0">250611.0</data>
      <data key="d1">7046.0</data>
    </edge>
    <edge source="56" target="59">
      <data key="d0">176902.0</data>
      <data key="d1">6627.0</data>
    </edge>
    <edge source="59" target="56">
      <data key="d0">176902.0</data>
      <data key="d1">6627.0</data>
    </edge>
    <edge source="59" target="11">
      <data key="d0">132676.0</data>
      <data key="d1">4415.0</data>
    </edge>
    <edge source="59" target="58">
      <data key="d0">855025.0</data>
      <data key="d1">8637.0</data>
    </edge>
    <edge source="62" target="24">
      <data key="d0">280095.0</data>
      <data key="d1">10823.0</data>
    </edge>
    <edge source="62" target="26">
      <data key="d0">235869.0</data>
      <data key="d1">5162.0</data>
    </edge>
    <edge source="62" target="32">
      <data key="d0">147418.0</data>
      <data key="d1">5220.0</data>
    </edge>
    <edge source="62" target="5">
      <data key="d0">442254.0</data>
      <data key="d1">4140.0</data>
    </edge>
    <edge source="62" target="29">
      <data key="d0">398029.0</data>
      <data key="d1">11673.0</data>
    </edge>
    <edge source="62" target="58">
      <data key="d0">2653530.0</data>
      <data key="d1">11618.0</data>
    </edge>
    <edge source="64" target="45">
      <data key="d0">1179350.0</data>
      <data key="d1">15101.0</data>
    </edge>
    <edge source="64" target="42">
      <data key="d0">117935.0</data>
      <data key="d1">4052.0</data>
    </edge>
    <edge source="64" target="12">
      <data key="d0">737091.0</data>
      <data key="d1">13896.0</data>
    </edge>
  </graph>
</graphml>
