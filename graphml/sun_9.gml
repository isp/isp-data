<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="1" />
    <node id="5" />
    <node id="4" />
    <node id="12" />
    <node id="11" />
    <node id="23" />
    <node id="26" />
    <node id="6" />
    <node id="7" />
    <node id="8" />
    <node id="9" />
    <node id="10" />
    <node id="25" />
    <node id="24" />
    <node id="2" />
    <node id="22" />
    <node id="21" />
    <node id="20" />
    <node id="19" />
    <node id="3" />
    <node id="18" />
    <node id="17" />
    <node id="15" />
    <node id="14" />
    <node id="16" />
    <node id="13" />
    <edge source="0" target="1">
      <data key="d0">25.55</data>
      <data key="d1">13039.0</data>
    </edge>
    <edge source="0" target="20">
      <data key="d0">21.2</data>
      <data key="d1">7566.0</data>
    </edge>
    <edge source="0" target="19">
      <data key="d0">27.05</data>
      <data key="d1">7593.0</data>
    </edge>
    <edge source="1" target="0">
      <data key="d0">25.55</data>
      <data key="d1">13039.0</data>
    </edge>
    <edge source="1" target="2">
      <data key="d0">18.34</data>
      <data key="d1">10310.0</data>
    </edge>
    <edge source="1" target="19">
      <data key="d0">27.59</data>
      <data key="d1">11104.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">14.81</data>
      <data key="d1">7479.0</data>
    </edge>
    <edge source="5" target="6">
      <data key="d0">6.92</data>
      <data key="d1">8448.0</data>
    </edge>
    <edge source="5" target="13">
      <data key="d0">19.84</data>
      <data key="d1">17070.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">14.81</data>
      <data key="d1">7479.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">33.58</data>
      <data key="d1">10480.0</data>
    </edge>
    <edge source="4" target="17">
      <data key="d0">32.76</data>
      <data key="d1">22322.0</data>
    </edge>
    <edge source="4" target="13">
      <data key="d0">25.69</data>
      <data key="d1">17839.0</data>
    </edge>
    <edge source="12" target="11">
      <data key="d0">10.46</data>
      <data key="d1">7655.0</data>
    </edge>
    <edge source="12" target="14">
      <data key="d0">9.78</data>
      <data key="d1">12360.0</data>
    </edge>
    <edge source="12" target="16">
      <data key="d0">13.18</data>
      <data key="d1">7976.0</data>
    </edge>
    <edge source="11" target="12">
      <data key="d0">10.46</data>
      <data key="d1">7655.0</data>
    </edge>
    <edge source="11" target="8">
      <data key="d0">16.3</data>
      <data key="d1">12141.0</data>
    </edge>
    <edge source="11" target="9">
      <data key="d0">4.88</data>
      <data key="d1">12841.0</data>
    </edge>
    <edge source="11" target="10">
      <data key="d0">10.18</data>
      <data key="d1">17342.0</data>
    </edge>
    <edge source="23" target="26">
      <data key="d0">8.01</data>
      <data key="d1">11543.0</data>
    </edge>
    <edge source="23" target="24">
      <data key="d0">15.08</data>
      <data key="d1">18035.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">4.2</data>
      <data key="d1">10149.0</data>
    </edge>
    <edge source="23" target="21">
      <data key="d0">29.5</data>
      <data key="d1">14756.0</data>
    </edge>
    <edge source="23" target="18">
      <data key="d0">19.3</data>
      <data key="d1">25690.0</data>
    </edge>
    <edge source="23" target="15">
      <data key="d0">25.14</data>
      <data key="d1">8654.0</data>
    </edge>
    <edge source="26" target="23">
      <data key="d0">8.01</data>
      <data key="d1">11543.0</data>
    </edge>
    <edge source="26" target="22">
      <data key="d0">11.54</data>
      <data key="d1">18298.0</data>
    </edge>
    <edge source="26" target="19">
      <data key="d0">21.74</data>
      <data key="d1">10414.0</data>
    </edge>
    <edge source="26" target="21">
      <data key="d0">29.36</data>
      <data key="d1">12170.0</data>
    </edge>
    <edge source="26" target="18">
      <data key="d0">15.08</data>
      <data key="d1">16440.0</data>
    </edge>
    <edge source="6" target="5">
      <data key="d0">6.92</data>
      <data key="d1">8448.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">33.44</data>
      <data key="d1">5215.0</data>
    </edge>
    <edge source="6" target="13">
      <data key="d0">20.66</data>
      <data key="d1">16687.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">33.44</data>
      <data key="d1">5215.0</data>
    </edge>
    <edge source="7" target="8">
      <data key="d0">9.5</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="8" target="7">
      <data key="d0">9.5</data>
      <data key="d1">4851.0</data>
    </edge>
    <edge source="8" target="9">
      <data key="d0">17.8</data>
      <data key="d1">8220.0</data>
    </edge>
    <edge source="8" target="11">
      <data key="d0">16.3</data>
      <data key="d1">12141.0</data>
    </edge>
    <edge source="9" target="8">
      <data key="d0">17.8</data>
      <data key="d1">8220.0</data>
    </edge>
    <edge source="9" target="11">
      <data key="d0">4.88</data>
      <data key="d1">12841.0</data>
    </edge>
    <edge source="9" target="10">
      <data key="d0">5.83</data>
      <data key="d1">17863.0</data>
    </edge>
    <edge source="10" target="11">
      <data key="d0">10.18</data>
      <data key="d1">17342.0</data>
    </edge>
    <edge source="10" target="9">
      <data key="d0">5.83</data>
      <data key="d1">17863.0</data>
    </edge>
    <edge source="10" target="25">
      <data key="d0">11.0</data>
      <data key="d1">20664.0</data>
    </edge>
    <edge source="10" target="14">
      <data key="d0">8.14</data>
      <data key="d1">14012.0</data>
    </edge>
    <edge source="25" target="10">
      <data key="d0">11.0</data>
      <data key="d1">20664.0</data>
    </edge>
    <edge source="25" target="24">
      <data key="d0">2.43</data>
      <data key="d1">13200.0</data>
    </edge>
    <edge source="25" target="15">
      <data key="d0">7.87</data>
      <data key="d1">20947.0</data>
    </edge>
    <edge source="24" target="25">
      <data key="d0">2.43</data>
      <data key="d1">13200.0</data>
    </edge>
    <edge source="24" target="22">
      <data key="d0">17.12</data>
      <data key="d1">18655.0</data>
    </edge>
    <edge source="24" target="23">
      <data key="d0">15.08</data>
      <data key="d1">18035.0</data>
    </edge>
    <edge source="2" target="1">
      <data key="d0">18.34</data>
      <data key="d1">10310.0</data>
    </edge>
    <edge source="2" target="3">
      <data key="d0">28.0</data>
      <data key="d1">11516.0</data>
    </edge>
    <edge source="22" target="24">
      <data key="d0">17.12</data>
      <data key="d1">18655.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">4.2</data>
      <data key="d1">10149.0</data>
    </edge>
    <edge source="22" target="26">
      <data key="d0">11.54</data>
      <data key="d1">18298.0</data>
    </edge>
    <edge source="22" target="21">
      <data key="d0">25.42</data>
      <data key="d1">13322.0</data>
    </edge>
    <edge source="21" target="23">
      <data key="d0">29.5</data>
      <data key="d1">14756.0</data>
    </edge>
    <edge source="21" target="22">
      <data key="d0">25.42</data>
      <data key="d1">13322.0</data>
    </edge>
    <edge source="21" target="20">
      <data key="d0">7.19</data>
      <data key="d1">10177.0</data>
    </edge>
    <edge source="21" target="19">
      <data key="d0">16.85</data>
      <data key="d1">16759.0</data>
    </edge>
    <edge source="21" target="26">
      <data key="d0">29.36</data>
      <data key="d1">12170.0</data>
    </edge>
    <edge source="20" target="21">
      <data key="d0">7.19</data>
      <data key="d1">10177.0</data>
    </edge>
    <edge source="20" target="0">
      <data key="d0">21.2</data>
      <data key="d1">7566.0</data>
    </edge>
    <edge source="20" target="19">
      <data key="d0">24.06</data>
      <data key="d1">10312.0</data>
    </edge>
    <edge source="19" target="20">
      <data key="d0">24.06</data>
      <data key="d1">10312.0</data>
    </edge>
    <edge source="19" target="0">
      <data key="d0">27.05</data>
      <data key="d1">7593.0</data>
    </edge>
    <edge source="19" target="21">
      <data key="d0">16.85</data>
      <data key="d1">16759.0</data>
    </edge>
    <edge source="19" target="1">
      <data key="d0">27.59</data>
      <data key="d1">11104.0</data>
    </edge>
    <edge source="19" target="26">
      <data key="d0">21.74</data>
      <data key="d1">10414.0</data>
    </edge>
    <edge source="19" target="18">
      <data key="d0">26.23</data>
      <data key="d1">13516.0</data>
    </edge>
    <edge source="3" target="2">
      <data key="d0">28.0</data>
      <data key="d1">11516.0</data>
    </edge>
    <edge source="3" target="18">
      <data key="d0">45.54</data>
      <data key="d1">16697.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">33.58</data>
      <data key="d1">10480.0</data>
    </edge>
    <edge source="18" target="19">
      <data key="d0">26.23</data>
      <data key="d1">13516.0</data>
    </edge>
    <edge source="18" target="26">
      <data key="d0">15.08</data>
      <data key="d1">16440.0</data>
    </edge>
    <edge source="18" target="3">
      <data key="d0">45.54</data>
      <data key="d1">16697.0</data>
    </edge>
    <edge source="18" target="17">
      <data key="d0">14.26</data>
      <data key="d1">10096.0</data>
    </edge>
    <edge source="18" target="15">
      <data key="d0">17.66</data>
      <data key="d1">23394.0</data>
    </edge>
    <edge source="18" target="23">
      <data key="d0">19.3</data>
      <data key="d1">25690.0</data>
    </edge>
    <edge source="17" target="18">
      <data key="d0">14.26</data>
      <data key="d1">10096.0</data>
    </edge>
    <edge source="17" target="15">
      <data key="d0">25.42</data>
      <data key="d1">17570.0</data>
    </edge>
    <edge source="17" target="16">
      <data key="d0">38.2</data>
      <data key="d1">14239.0</data>
    </edge>
    <edge source="17" target="4">
      <data key="d0">32.76</data>
      <data key="d1">22322.0</data>
    </edge>
    <edge source="15" target="18">
      <data key="d0">17.66</data>
      <data key="d1">23394.0</data>
    </edge>
    <edge source="15" target="23">
      <data key="d0">25.14</data>
      <data key="d1">8654.0</data>
    </edge>
    <edge source="15" target="25">
      <data key="d0">7.87</data>
      <data key="d1">20947.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">10.05</data>
      <data key="d1">18181.0</data>
    </edge>
    <edge source="15" target="16">
      <data key="d0">25.42</data>
      <data key="d1">11498.0</data>
    </edge>
    <edge source="15" target="17">
      <data key="d0">25.42</data>
      <data key="d1">17570.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">10.05</data>
      <data key="d1">18181.0</data>
    </edge>
    <edge source="14" target="10">
      <data key="d0">8.14</data>
      <data key="d1">14012.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">9.78</data>
      <data key="d1">12360.0</data>
    </edge>
    <edge source="16" target="15">
      <data key="d0">25.42</data>
      <data key="d1">11498.0</data>
    </edge>
    <edge source="16" target="17">
      <data key="d0">38.2</data>
      <data key="d1">14239.0</data>
    </edge>
    <edge source="16" target="13">
      <data key="d0">25.28</data>
      <data key="d1">14463.0</data>
    </edge>
    <edge source="16" target="12">
      <data key="d0">13.18</data>
      <data key="d1">7976.0</data>
    </edge>
    <edge source="13" target="16">
      <data key="d0">25.28</data>
      <data key="d1">14463.0</data>
    </edge>
    <edge source="13" target="4">
      <data key="d0">25.69</data>
      <data key="d1">17839.0</data>
    </edge>
    <edge source="13" target="5">
      <data key="d0">19.84</data>
      <data key="d1">17070.0</data>
    </edge>
    <edge source="13" target="6">
      <data key="d0">20.66</data>
      <data key="d1">16687.0</data>
    </edge>
  </graph>
</graphml>
