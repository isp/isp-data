<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd"><key id="d1" for="edge" attr.name=" latency" attr.type="double"/>
<key id="d0" for="edge" attr.name=" cost" attr.type="double"/>
<graph edgedefault="directed"><node id="0"/>
<node id="6"/>
<node id="1"/>
<node id="7"/>
<node id="2"/>
<node id="11"/>
<node id="10"/>
<node id="14"/>
<node id="15"/>
<node id="18"/>
<node id="19"/>
<node id="3"/>
<node id="8"/>
<node id="4"/>
<node id="9"/>
<node id="5"/>
<node id="13"/>
<node id="12"/>
<node id="16"/>
<node id="17"/>
<node id="20"/>
<node id="21"/>
<edge source="0" target="6">
  <data key="d0">1.143550047</data>
  <data key="d1">1.59890296</data>
</edge>
<edge source="6" target="0">
  <data key="d0">1.143550047</data>
  <data key="d1">1.59890296</data>
</edge>
<edge source="6" target="1">
  <data key="d0">1.099552333</data>
  <data key="d1">1.42608342</data>
</edge>
<edge source="6" target="7">
  <data key="d0">1.6613957</data>
  <data key="d1">1.61928819</data>
</edge>
<edge source="6" target="11">
  <data key="d0">1.075611633</data>
  <data key="d1">1.463546816</data>
</edge>
<edge source="6" target="10">
  <data key="d0">1.674182028</data>
  <data key="d1">1.906100181</data>
</edge>
<edge source="1" target="6">
  <data key="d0">1.099552333</data>
  <data key="d1">1.42608342</data>
</edge>
<edge source="1" target="7">
  <data key="d0">1.507158088</data>
  <data key="d1">1.993657255</data>
</edge>
<edge source="7" target="1">
  <data key="d0">1.507158088</data>
  <data key="d1">1.993657255</data>
</edge>
<edge source="7" target="2">
  <data key="d0">1.132063799</data>
  <data key="d1">1.519549916</data>
</edge>
<edge source="7" target="6">
  <data key="d0">1.6613957</data>
  <data key="d1">1.61928819</data>
</edge>
<edge source="7" target="10">
  <data key="d0">1.241001135</data>
  <data key="d1">1.781774642</data>
</edge>
<edge source="7" target="11">
  <data key="d0">1.491912832</data>
  <data key="d1">1.801327786</data>
</edge>
<edge source="2" target="7">
  <data key="d0">1.132063799</data>
  <data key="d1">1.519549916</data>
</edge>
<edge source="11" target="6">
  <data key="d0">1.075611633</data>
  <data key="d1">1.463546816</data>
</edge>
<edge source="11" target="7">
  <data key="d0">1.491912832</data>
  <data key="d1">1.801327786</data>
</edge>
<edge source="11" target="10">
  <data key="d0">1.598047426</data>
  <data key="d1">1.294233895</data>
</edge>
<edge source="11" target="15">
  <data key="d0">1.548373089</data>
  <data key="d1">1.176864567</data>
</edge>
<edge source="10" target="6">
  <data key="d0">1.674182028</data>
  <data key="d1">1.906100181</data>
</edge>
<edge source="10" target="7">
  <data key="d0">1.241001135</data>
  <data key="d1">1.781774642</data>
</edge>
<edge source="10" target="11">
  <data key="d0">1.598047426</data>
  <data key="d1">1.294233895</data>
</edge>
<edge source="10" target="14">
  <data key="d0">1.031112579</data>
  <data key="d1">1.747631568</data>
</edge>
<edge source="14" target="10">
  <data key="d0">1.031112579</data>
  <data key="d1">1.747631568</data>
</edge>
<edge source="14" target="18">
  <data key="d0">1.24740481</data>
  <data key="d1">1.599842915</data>
</edge>
<edge source="14" target="19">
  <data key="d0">1.34601476</data>
  <data key="d1">1.078396326</data>
</edge>
<edge source="15" target="11">
  <data key="d0">1.548373089</data>
  <data key="d1">1.176864567</data>
</edge>
<edge source="15" target="19">
  <data key="d0">1.144591597</data>
  <data key="d1">1.356672774</data>
</edge>
<edge source="18" target="14">
  <data key="d0">1.24740481</data>
  <data key="d1">1.599842915</data>
</edge>
<edge source="18" target="17">
  <data key="d0">1.397663676</data>
  <data key="d1">1.07453267</data>
</edge>
<edge source="18" target="21">
  <data key="d0">1.70611157</data>
  <data key="d1">1.698453775</data>
</edge>
<edge source="19" target="14">
  <data key="d0">1.34601476</data>
  <data key="d1">1.078396326</data>
</edge>
<edge source="19" target="15">
  <data key="d0">1.144591597</data>
  <data key="d1">1.356672774</data>
</edge>
<edge source="19" target="17">
  <data key="d0">1.308947652</data>
  <data key="d1">1.296137966</data>
</edge>
<edge source="19" target="21">
  <data key="d0">1.557450897</data>
  <data key="d1">1.925689364</data>
</edge>
<edge source="3" target="8">
  <data key="d0">1.914940889</data>
  <data key="d1">1.217694674</data>
</edge>
<edge source="8" target="3">
  <data key="d0">1.914940889</data>
  <data key="d1">1.217694674</data>
</edge>
<edge source="8" target="9">
  <data key="d0">1.387072252</data>
  <data key="d1">1.794618397</data>
</edge>
<edge source="8" target="13">
  <data key="d0">1.351124003</data>
  <data key="d1">1.308561307</data>
</edge>
<edge source="8" target="12">
  <data key="d0">1.620881618</data>
  <data key="d1">1.855297485</data>
</edge>
<edge source="4" target="9">
  <data key="d0">1.919092906</data>
  <data key="d1">1.294910947</data>
</edge>
<edge source="9" target="4">
  <data key="d0">1.919092906</data>
  <data key="d1">1.294910947</data>
</edge>
<edge source="9" target="5">
  <data key="d0">1.904658811</data>
  <data key="d1">1.470103673</data>
</edge>
<edge source="9" target="8">
  <data key="d0">1.387072252</data>
  <data key="d1">1.794618397</data>
</edge>
<edge source="9" target="12">
  <data key="d0">1.515720675</data>
  <data key="d1">1.842130829</data>
</edge>
<edge source="5" target="9">
  <data key="d0">1.904658811</data>
  <data key="d1">1.470103673</data>
</edge>
<edge source="13" target="8">
  <data key="d0">1.351124003</data>
  <data key="d1">1.308561307</data>
</edge>
<edge source="13" target="12">
  <data key="d0">1.596074771</data>
  <data key="d1">1.830759016</data>
</edge>
<edge source="13" target="17">
  <data key="d0">1.373079271</data>
  <data key="d1">1.491784821</data>
</edge>
<edge source="12" target="8">
  <data key="d0">1.620881618</data>
  <data key="d1">1.855297485</data>
</edge>
<edge source="12" target="9">
  <data key="d0">1.515720675</data>
  <data key="d1">1.842130829</data>
</edge>
<edge source="12" target="13">
  <data key="d0">1.596074771</data>
  <data key="d1">1.830759016</data>
</edge>
<edge source="12" target="16">
  <data key="d0">1.010596745</data>
  <data key="d1">1.11299271</data>
</edge>
<edge source="16" target="12">
  <data key="d0">1.010596745</data>
  <data key="d1">1.11299271</data>
</edge>
<edge source="16" target="20">
  <data key="d0">1.302715771</data>
  <data key="d1">1.836764527</data>
</edge>
<edge source="17" target="13">
  <data key="d0">1.373079271</data>
  <data key="d1">1.491784821</data>
</edge>
<edge source="17" target="18">
  <data key="d0">1.397663676</data>
  <data key="d1">1.07453267</data>
</edge>
<edge source="17" target="19">
  <data key="d0">1.308947652</data>
  <data key="d1">1.296137966</data>
</edge>
<edge source="17" target="20">
  <data key="d0">1.914757412</data>
  <data key="d1">1.335893105</data>
</edge>
<edge source="20" target="16">
  <data key="d0">1.302715771</data>
  <data key="d1">1.836764527</data>
</edge>
<edge source="20" target="17">
  <data key="d0">1.914757412</data>
  <data key="d1">1.335893105</data>
</edge>
<edge source="20" target="21">
  <data key="d0">1.198830849</data>
  <data key="d1">1.685868878</data>
</edge>
<edge source="21" target="18">
  <data key="d0">1.70611157</data>
  <data key="d1">1.698453775</data>
</edge>
<edge source="21" target="19">
  <data key="d0">1.557450897</data>
  <data key="d1">1.925689364</data>
</edge>
<edge source="21" target="20">
  <data key="d0">1.198830849</data>
  <data key="d1">1.685868878</data>
</edge>
</graph></graphml>