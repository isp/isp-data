<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="2" />
    <node id="4" />
    <node id="1" />
    <node id="3" />
    <node id="5" />
    <node id="11" />
    <node id="6" />
    <node id="7" />
    <node id="8" />
    <node id="16" />
    <node id="21" />
    <node id="10" />
    <node id="15" />
    <node id="9" />
    <node id="12" />
    <node id="13" />
    <node id="14" />
    <node id="17" />
    <node id="20" />
    <node id="23" />
    <node id="19" />
    <node id="25" />
    <node id="18" />
    <node id="22" />
    <node id="24" />
    <edge source="0" target="2">
      <data key="d0">854.0</data>
      <data key="d1">983.0</data>
    </edge>
    <edge source="0" target="4">
      <data key="d0">864.0</data>
      <data key="d1">1230.0</data>
    </edge>
    <edge source="2" target="0">
      <data key="d0">854.0</data>
      <data key="d1">983.0</data>
    </edge>
    <edge source="2" target="1">
      <data key="d0">425.0</data>
      <data key="d1">543.0</data>
    </edge>
    <edge source="2" target="4">
      <data key="d0">751.0</data>
      <data key="d1">1088.0</data>
    </edge>
    <edge source="4" target="0">
      <data key="d0">864.0</data>
      <data key="d1">1230.0</data>
    </edge>
    <edge source="4" target="2">
      <data key="d0">751.0</data>
      <data key="d1">1088.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">474.0</data>
      <data key="d1">569.0</data>
    </edge>
    <edge source="4" target="11">
      <data key="d0">478.0</data>
      <data key="d1">717.0</data>
    </edge>
    <edge source="1" target="2">
      <data key="d0">425.0</data>
      <data key="d1">543.0</data>
    </edge>
    <edge source="1" target="3">
      <data key="d0">296.0</data>
      <data key="d1">388.0</data>
    </edge>
    <edge source="1" target="5">
      <data key="d0">879.0</data>
      <data key="d1">1219.0</data>
    </edge>
    <edge source="3" target="1">
      <data key="d0">296.0</data>
      <data key="d1">388.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">474.0</data>
      <data key="d1">569.0</data>
    </edge>
    <edge source="3" target="5">
      <data key="d0">731.0</data>
      <data key="d1">976.0</data>
    </edge>
    <edge source="5" target="1">
      <data key="d0">879.0</data>
      <data key="d1">1219.0</data>
    </edge>
    <edge source="5" target="3">
      <data key="d0">731.0</data>
      <data key="d1">976.0</data>
    </edge>
    <edge source="5" target="6">
      <data key="d0">705.0</data>
      <data key="d1">961.0</data>
    </edge>
    <edge source="5" target="7">
      <data key="d0">838.0</data>
      <data key="d1">1120.0</data>
    </edge>
    <edge source="11" target="4">
      <data key="d0">478.0</data>
      <data key="d1">717.0</data>
    </edge>
    <edge source="11" target="6">
      <data key="d0">820.0</data>
      <data key="d1">1058.0</data>
    </edge>
    <edge source="11" target="10">
      <data key="d0">679.0</data>
      <data key="d1">1016.0</data>
    </edge>
    <edge source="6" target="5">
      <data key="d0">705.0</data>
      <data key="d1">961.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">273.0</data>
      <data key="d1">325.0</data>
    </edge>
    <edge source="6" target="8">
      <data key="d0">299.0</data>
      <data key="d1">348.0</data>
    </edge>
    <edge source="6" target="11">
      <data key="d0">820.0</data>
      <data key="d1">1058.0</data>
    </edge>
    <edge source="6" target="16">
      <data key="d0">781.0</data>
      <data key="d1">1068.0</data>
    </edge>
    <edge source="7" target="5">
      <data key="d0">838.0</data>
      <data key="d1">1120.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">273.0</data>
      <data key="d1">325.0</data>
    </edge>
    <edge source="7" target="21">
      <data key="d0">401.0</data>
      <data key="d1">533.0</data>
    </edge>
    <edge source="8" target="6">
      <data key="d0">299.0</data>
      <data key="d1">348.0</data>
    </edge>
    <edge source="8" target="10">
      <data key="d0">282.0</data>
      <data key="d1">334.0</data>
    </edge>
    <edge source="8" target="15">
      <data key="d0">449.0</data>
      <data key="d1">609.0</data>
    </edge>
    <edge source="16" target="6">
      <data key="d0">781.0</data>
      <data key="d1">1068.0</data>
    </edge>
    <edge source="16" target="13">
      <data key="d0">308.0</data>
      <data key="d1">355.0</data>
    </edge>
    <edge source="16" target="20">
      <data key="d0">413.0</data>
      <data key="d1">582.0</data>
    </edge>
    <edge source="16" target="23">
      <data key="d0">284.0</data>
      <data key="d1">335.0</data>
    </edge>
    <edge source="21" target="7">
      <data key="d0">401.0</data>
      <data key="d1">533.0</data>
    </edge>
    <edge source="21" target="23">
      <data key="d0">530.0</data>
      <data key="d1">678.0</data>
    </edge>
    <edge source="21" target="24">
      <data key="d0">824.0</data>
      <data key="d1">1053.0</data>
    </edge>
    <edge source="10" target="8">
      <data key="d0">282.0</data>
      <data key="d1">334.0</data>
    </edge>
    <edge source="10" target="9">
      <data key="d0">468.0</data>
      <data key="d1">590.0</data>
    </edge>
    <edge source="10" target="11">
      <data key="d0">679.0</data>
      <data key="d1">1016.0</data>
    </edge>
    <edge source="10" target="15">
      <data key="d0">297.0</data>
      <data key="d1">439.0</data>
    </edge>
    <edge source="15" target="8">
      <data key="d0">449.0</data>
      <data key="d1">609.0</data>
    </edge>
    <edge source="15" target="10">
      <data key="d0">297.0</data>
      <data key="d1">439.0</data>
    </edge>
    <edge source="15" target="12">
      <data key="d0">325.0</data>
      <data key="d1">407.0</data>
    </edge>
    <edge source="15" target="13">
      <data key="d0">287.0</data>
      <data key="d1">420.0</data>
    </edge>
    <edge source="9" target="10">
      <data key="d0">468.0</data>
      <data key="d1">590.0</data>
    </edge>
    <edge source="9" target="12">
      <data key="d0">436.0</data>
      <data key="d1">629.0</data>
    </edge>
    <edge source="12" target="9">
      <data key="d0">436.0</data>
      <data key="d1">629.0</data>
    </edge>
    <edge source="12" target="13">
      <data key="d0">228.0</data>
      <data key="d1">284.0</data>
    </edge>
    <edge source="12" target="14">
      <data key="d0">315.0</data>
      <data key="d1">490.0</data>
    </edge>
    <edge source="12" target="15">
      <data key="d0">325.0</data>
      <data key="d1">407.0</data>
    </edge>
    <edge source="13" target="12">
      <data key="d0">228.0</data>
      <data key="d1">284.0</data>
    </edge>
    <edge source="13" target="15">
      <data key="d0">287.0</data>
      <data key="d1">420.0</data>
    </edge>
    <edge source="13" target="16">
      <data key="d0">308.0</data>
      <data key="d1">355.0</data>
    </edge>
    <edge source="13" target="17">
      <data key="d0">343.0</data>
      <data key="d1">496.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">315.0</data>
      <data key="d1">490.0</data>
    </edge>
    <edge source="14" target="17">
      <data key="d0">117.0</data>
      <data key="d1">161.0</data>
    </edge>
    <edge source="17" target="13">
      <data key="d0">343.0</data>
      <data key="d1">496.0</data>
    </edge>
    <edge source="17" target="14">
      <data key="d0">117.0</data>
      <data key="d1">161.0</data>
    </edge>
    <edge source="17" target="19">
      <data key="d0">518.0</data>
      <data key="d1">798.0</data>
    </edge>
    <edge source="17" target="25">
      <data key="d0">384.0</data>
      <data key="d1">535.0</data>
    </edge>
    <edge source="20" target="16">
      <data key="d0">413.0</data>
      <data key="d1">582.0</data>
    </edge>
    <edge source="20" target="23">
      <data key="d0">284.0</data>
      <data key="d1">383.0</data>
    </edge>
    <edge source="20" target="25">
      <data key="d0">414.0</data>
      <data key="d1">532.0</data>
    </edge>
    <edge source="23" target="16">
      <data key="d0">284.0</data>
      <data key="d1">335.0</data>
    </edge>
    <edge source="23" target="20">
      <data key="d0">284.0</data>
      <data key="d1">383.0</data>
    </edge>
    <edge source="23" target="21">
      <data key="d0">530.0</data>
      <data key="d1">678.0</data>
    </edge>
    <edge source="23" target="24">
      <data key="d0">747.0</data>
      <data key="d1">886.0</data>
    </edge>
    <edge source="19" target="17">
      <data key="d0">518.0</data>
      <data key="d1">798.0</data>
    </edge>
    <edge source="19" target="18">
      <data key="d0">182.0</data>
      <data key="d1">210.0</data>
    </edge>
    <edge source="19" target="22">
      <data key="d0">180.0</data>
      <data key="d1">280.0</data>
    </edge>
    <edge source="25" target="17">
      <data key="d0">384.0</data>
      <data key="d1">535.0</data>
    </edge>
    <edge source="25" target="18">
      <data key="d0">267.0</data>
      <data key="d1">372.0</data>
    </edge>
    <edge source="25" target="20">
      <data key="d0">414.0</data>
      <data key="d1">532.0</data>
    </edge>
    <edge source="18" target="19">
      <data key="d0">182.0</data>
      <data key="d1">210.0</data>
    </edge>
    <edge source="18" target="22">
      <data key="d0">232.0</data>
      <data key="d1">324.0</data>
    </edge>
    <edge source="18" target="25">
      <data key="d0">267.0</data>
      <data key="d1">372.0</data>
    </edge>
    <edge source="22" target="18">
      <data key="d0">232.0</data>
      <data key="d1">324.0</data>
    </edge>
    <edge source="22" target="19">
      <data key="d0">180.0</data>
      <data key="d1">280.0</data>
    </edge>
    <edge source="24" target="21">
      <data key="d0">824.0</data>
      <data key="d1">1053.0</data>
    </edge>
    <edge source="24" target="23">
      <data key="d0">747.0</data>
      <data key="d1">886.0</data>
    </edge>
  </graph>
</graphml>
