<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="24" />
    <node id="30" />
    <node id="32" />
    <node id="1" />
    <node id="3" />
    <node id="6" />
    <node id="9" />
    <node id="14" />
    <node id="21" />
    <node id="28" />
    <node id="2" />
    <node id="12" />
    <node id="15" />
    <node id="17" />
    <node id="33" />
    <node id="25" />
    <node id="27" />
    <node id="4" />
    <node id="18" />
    <node id="5" />
    <node id="7" />
    <node id="23" />
    <node id="31" />
    <node id="8" />
    <node id="11" />
    <node id="13" />
    <node id="19" />
    <node id="22" />
    <node id="10" />
    <node id="16" />
    <node id="26" />
    <node id="34" />
    <node id="20" />
    <node id="29" />
    <edge source="0" target="24">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="0" target="30">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="0" target="32">
      <data key="d0">210.0</data>
      <data key="d1">1170.0</data>
    </edge>
    <edge source="24" target="0">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="24" target="4">
      <data key="d0">150.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="24" target="6">
      <data key="d0">280.0</data>
      <data key="d1">1868.0</data>
    </edge>
    <edge source="24" target="32">
      <data key="d0">240.0</data>
      <data key="d1">1487.0</data>
    </edge>
    <edge source="30" target="0">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="30" target="20">
      <data key="d0">200.0</data>
      <data key="d1">1044.0</data>
    </edge>
    <edge source="30" target="29">
      <data key="d0">200.0</data>
      <data key="d1">1082.0</data>
    </edge>
    <edge source="30" target="32">
      <data key="d0">180.0</data>
      <data key="d1">825.0</data>
    </edge>
    <edge source="32" target="0">
      <data key="d0">210.0</data>
      <data key="d1">1170.0</data>
    </edge>
    <edge source="32" target="6">
      <data key="d0">200.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="32" target="21">
      <data key="d0">190.0</data>
      <data key="d1">906.0</data>
    </edge>
    <edge source="32" target="24">
      <data key="d0">240.0</data>
      <data key="d1">1487.0</data>
    </edge>
    <edge source="32" target="28">
      <data key="d0">260.0</data>
      <data key="d1">1628.0</data>
    </edge>
    <edge source="32" target="29">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="32" target="30">
      <data key="d0">180.0</data>
      <data key="d1">825.0</data>
    </edge>
    <edge source="32" target="34">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="1" target="3">
      <data key="d0">160.0</data>
      <data key="d1">608.0</data>
    </edge>
    <edge source="1" target="6">
      <data key="d0">190.0</data>
      <data key="d1">949.0</data>
    </edge>
    <edge source="1" target="9">
      <data key="d0">180.0</data>
      <data key="d1">825.0</data>
    </edge>
    <edge source="1" target="14">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="1" target="21">
      <data key="d0">220.0</data>
      <data key="d1">1281.0</data>
    </edge>
    <edge source="1" target="28">
      <data key="d0">250.0</data>
      <data key="d1">1565.0</data>
    </edge>
    <edge source="3" target="1">
      <data key="d0">160.0</data>
      <data key="d1">608.0</data>
    </edge>
    <edge source="3" target="14">
      <data key="d0">210.0</data>
      <data key="d1">1105.0</data>
    </edge>
    <edge source="3" target="17">
      <data key="d0">160.0</data>
      <data key="d1">632.0</data>
    </edge>
    <edge source="3" target="25">
      <data key="d0">230.0</data>
      <data key="d1">1342.0</data>
    </edge>
    <edge source="3" target="27">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="6" target="1">
      <data key="d0">190.0</data>
      <data key="d1">949.0</data>
    </edge>
    <edge source="6" target="4">
      <data key="d0">270.0</data>
      <data key="d1">1749.0</data>
    </edge>
    <edge source="6" target="9">
      <data key="d0">230.0</data>
      <data key="d1">1304.0</data>
    </edge>
    <edge source="6" target="21">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="6" target="24">
      <data key="d0">280.0</data>
      <data key="d1">1868.0</data>
    </edge>
    <edge source="6" target="28">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="6" target="32">
      <data key="d0">200.0</data>
      <data key="d1">1000.0</data>
    </edge>
    <edge source="9" target="1">
      <data key="d0">180.0</data>
      <data key="d1">825.0</data>
    </edge>
    <edge source="9" target="6">
      <data key="d0">230.0</data>
      <data key="d1">1304.0</data>
    </edge>
    <edge source="9" target="7">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="9" target="11">
      <data key="d0">150.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="9" target="14">
      <data key="d0">170.0</data>
      <data key="d1">728.0</data>
    </edge>
    <edge source="9" target="23">
      <data key="d0">190.0</data>
      <data key="d1">906.0</data>
    </edge>
    <edge source="9" target="28">
      <data key="d0">220.0</data>
      <data key="d1">1204.0</data>
    </edge>
    <edge source="14" target="1">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="14" target="3">
      <data key="d0">210.0</data>
      <data key="d1">1105.0</data>
    </edge>
    <edge source="14" target="7">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="14" target="9">
      <data key="d0">170.0</data>
      <data key="d1">728.0</data>
    </edge>
    <edge source="14" target="25">
      <data key="d0">230.0</data>
      <data key="d1">1393.0</data>
    </edge>
    <edge source="21" target="1">
      <data key="d0">220.0</data>
      <data key="d1">1281.0</data>
    </edge>
    <edge source="21" target="6">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="21" target="16">
      <data key="d0">320.0</data>
      <data key="d1">2202.0</data>
    </edge>
    <edge source="21" target="17">
      <data key="d0">260.0</data>
      <data key="d1">1628.0</data>
    </edge>
    <edge source="21" target="32">
      <data key="d0">190.0</data>
      <data key="d1">906.0</data>
    </edge>
    <edge source="21" target="34">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="28" target="1">
      <data key="d0">250.0</data>
      <data key="d1">1565.0</data>
    </edge>
    <edge source="28" target="4">
      <data key="d0">200.0</data>
      <data key="d1">1005.0</data>
    </edge>
    <edge source="28" target="6">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="28" target="8">
      <data key="d0">240.0</data>
      <data key="d1">1432.0</data>
    </edge>
    <edge source="28" target="9">
      <data key="d0">220.0</data>
      <data key="d1">1204.0</data>
    </edge>
    <edge source="28" target="11">
      <data key="d0">200.0</data>
      <data key="d1">1030.0</data>
    </edge>
    <edge source="28" target="18">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="28" target="22">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="28" target="32">
      <data key="d0">260.0</data>
      <data key="d1">1628.0</data>
    </edge>
    <edge source="2" target="12">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
    <edge source="2" target="15">
      <data key="d0">130.0</data>
      <data key="d1">316.0</data>
    </edge>
    <edge source="2" target="17">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="2" target="33">
      <data key="d0">150.0</data>
      <data key="d1">583.0</data>
    </edge>
    <edge source="12" target="2">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
    <edge source="12" target="16">
      <data key="d0">150.0</data>
      <data key="d1">539.0</data>
    </edge>
    <edge source="12" target="33">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
    <edge source="15" target="2">
      <data key="d0">130.0</data>
      <data key="d1">316.0</data>
    </edge>
    <edge source="15" target="10">
      <data key="d0">210.0</data>
      <data key="d1">1166.0</data>
    </edge>
    <edge source="15" target="17">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="15" target="27">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="17" target="2">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="17" target="3">
      <data key="d0">160.0</data>
      <data key="d1">632.0</data>
    </edge>
    <edge source="17" target="15">
      <data key="d0">170.0</data>
      <data key="d1">707.0</data>
    </edge>
    <edge source="17" target="21">
      <data key="d0">260.0</data>
      <data key="d1">1628.0</data>
    </edge>
    <edge source="17" target="25">
      <data key="d0">250.0</data>
      <data key="d1">1562.0</data>
    </edge>
    <edge source="17" target="26">
      <data key="d0">340.0</data>
      <data key="d1">2421.0</data>
    </edge>
    <edge source="17" target="27">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="33" target="2">
      <data key="d0">150.0</data>
      <data key="d1">583.0</data>
    </edge>
    <edge source="33" target="10">
      <data key="d0">140.0</data>
      <data key="d1">447.0</data>
    </edge>
    <edge source="33" target="12">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
    <edge source="33" target="16">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="25" target="3">
      <data key="d0">230.0</data>
      <data key="d1">1342.0</data>
    </edge>
    <edge source="25" target="5">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="25" target="14">
      <data key="d0">230.0</data>
      <data key="d1">1393.0</data>
    </edge>
    <edge source="25" target="17">
      <data key="d0">250.0</data>
      <data key="d1">1562.0</data>
    </edge>
    <edge source="25" target="31">
      <data key="d0">290.0</data>
      <data key="d1">1980.0</data>
    </edge>
    <edge source="27" target="3">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="27" target="5">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="27" target="15">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="27" target="17">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="4" target="6">
      <data key="d0">270.0</data>
      <data key="d1">1749.0</data>
    </edge>
    <edge source="4" target="18">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="4" target="24">
      <data key="d0">150.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="4" target="28">
      <data key="d0">200.0</data>
      <data key="d1">1005.0</data>
    </edge>
    <edge source="18" target="4">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="18" target="22">
      <data key="d0">120.0</data>
      <data key="d1">283.0</data>
    </edge>
    <edge source="18" target="28">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="5" target="25">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="5" target="27">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="7" target="9">
      <data key="d0">180.0</data>
      <data key="d1">894.0</data>
    </edge>
    <edge source="7" target="14">
      <data key="d0">160.0</data>
      <data key="d1">671.0</data>
    </edge>
    <edge source="7" target="23">
      <data key="d0">130.0</data>
      <data key="d1">316.0</data>
    </edge>
    <edge source="7" target="31">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="23" target="7">
      <data key="d0">130.0</data>
      <data key="d1">316.0</data>
    </edge>
    <edge source="23" target="9">
      <data key="d0">190.0</data>
      <data key="d1">906.0</data>
    </edge>
    <edge source="23" target="11">
      <data key="d0">160.0</data>
      <data key="d1">640.0</data>
    </edge>
    <edge source="23" target="19">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="23" target="31">
      <data key="d0">150.0</data>
      <data key="d1">539.0</data>
    </edge>
    <edge source="31" target="7">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="31" target="23">
      <data key="d0">150.0</data>
      <data key="d1">539.0</data>
    </edge>
    <edge source="31" target="25">
      <data key="d0">290.0</data>
      <data key="d1">1980.0</data>
    </edge>
    <edge source="8" target="11">
      <data key="d0">200.0</data>
      <data key="d1">1082.0</data>
    </edge>
    <edge source="8" target="13">
      <data key="d0">140.0</data>
      <data key="d1">447.0</data>
    </edge>
    <edge source="8" target="19">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="8" target="22">
      <data key="d0">200.0</data>
      <data key="d1">1063.0</data>
    </edge>
    <edge source="8" target="28">
      <data key="d0">240.0</data>
      <data key="d1">1432.0</data>
    </edge>
    <edge source="11" target="8">
      <data key="d0">200.0</data>
      <data key="d1">1082.0</data>
    </edge>
    <edge source="11" target="9">
      <data key="d0">150.0</data>
      <data key="d1">500.0</data>
    </edge>
    <edge source="11" target="22">
      <data key="d0">240.0</data>
      <data key="d1">1414.0</data>
    </edge>
    <edge source="11" target="23">
      <data key="d0">160.0</data>
      <data key="d1">640.0</data>
    </edge>
    <edge source="11" target="28">
      <data key="d0">200.0</data>
      <data key="d1">1030.0</data>
    </edge>
    <edge source="13" target="8">
      <data key="d0">140.0</data>
      <data key="d1">447.0</data>
    </edge>
    <edge source="13" target="22">
      <data key="d0">190.0</data>
      <data key="d1">985.0</data>
    </edge>
    <edge source="19" target="8">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="19" target="23">
      <data key="d0">130.0</data>
      <data key="d1">361.0</data>
    </edge>
    <edge source="22" target="8">
      <data key="d0">200.0</data>
      <data key="d1">1063.0</data>
    </edge>
    <edge source="22" target="11">
      <data key="d0">240.0</data>
      <data key="d1">1414.0</data>
    </edge>
    <edge source="22" target="13">
      <data key="d0">190.0</data>
      <data key="d1">985.0</data>
    </edge>
    <edge source="22" target="18">
      <data key="d0">120.0</data>
      <data key="d1">283.0</data>
    </edge>
    <edge source="22" target="28">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="10" target="15">
      <data key="d0">210.0</data>
      <data key="d1">1166.0</data>
    </edge>
    <edge source="10" target="33">
      <data key="d0">140.0</data>
      <data key="d1">447.0</data>
    </edge>
    <edge source="16" target="12">
      <data key="d0">150.0</data>
      <data key="d1">539.0</data>
    </edge>
    <edge source="16" target="21">
      <data key="d0">320.0</data>
      <data key="d1">2202.0</data>
    </edge>
    <edge source="16" target="26">
      <data key="d0">260.0</data>
      <data key="d1">1612.0</data>
    </edge>
    <edge source="16" target="33">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="16" target="34">
      <data key="d0">320.0</data>
      <data key="d1">2247.0</data>
    </edge>
    <edge source="26" target="16">
      <data key="d0">260.0</data>
      <data key="d1">1612.0</data>
    </edge>
    <edge source="26" target="17">
      <data key="d0">340.0</data>
      <data key="d1">2421.0</data>
    </edge>
    <edge source="26" target="20">
      <data key="d0">250.0</data>
      <data key="d1">1581.0</data>
    </edge>
    <edge source="26" target="29">
      <data key="d0">220.0</data>
      <data key="d1">1273.0</data>
    </edge>
    <edge source="26" target="34">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="34" target="16">
      <data key="d0">320.0</data>
      <data key="d1">2247.0</data>
    </edge>
    <edge source="34" target="21">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="34" target="26">
      <data key="d0">210.0</data>
      <data key="d1">1118.0</data>
    </edge>
    <edge source="34" target="29">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
    <edge source="34" target="32">
      <data key="d0">180.0</data>
      <data key="d1">860.0</data>
    </edge>
    <edge source="20" target="26">
      <data key="d0">250.0</data>
      <data key="d1">1581.0</data>
    </edge>
    <edge source="20" target="29">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="20" target="30">
      <data key="d0">200.0</data>
      <data key="d1">1044.0</data>
    </edge>
    <edge source="29" target="20">
      <data key="d0">170.0</data>
      <data key="d1">721.0</data>
    </edge>
    <edge source="29" target="26">
      <data key="d0">220.0</data>
      <data key="d1">1273.0</data>
    </edge>
    <edge source="29" target="30">
      <data key="d0">200.0</data>
      <data key="d1">1082.0</data>
    </edge>
    <edge source="29" target="32">
      <data key="d0">180.0</data>
      <data key="d1">806.0</data>
    </edge>
    <edge source="29" target="34">
      <data key="d0">140.0</data>
      <data key="d1">412.0</data>
    </edge>
  </graph>
</graphml>
