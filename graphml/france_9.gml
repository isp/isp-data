<?xml version='1.0' encoding='utf-8'?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <key id="d1" for="edge" attr.name="loss" attr.type="double" />
  <key id="d0" for="edge" attr.name="delay" attr.type="double" />
  <graph edgedefault="directed">
    <node id="0" />
    <node id="1" />
    <node id="2" />
    <node id="6" />
    <node id="8" />
    <node id="9" />
    <node id="4" />
    <node id="3" />
    <node id="5" />
    <node id="7" />
    <node id="11" />
    <node id="10" />
    <node id="12" />
    <node id="13" />
    <node id="14" />
    <node id="15" />
    <node id="17" />
    <node id="19" />
    <node id="20" />
    <node id="21" />
    <node id="24" />
    <node id="16" />
    <node id="18" />
    <node id="23" />
    <node id="22" />
    <edge source="0" target="1">
      <data key="d0">200.0</data>
      <data key="d1">8305.0</data>
    </edge>
    <edge source="0" target="24">
      <data key="d0">200.0</data>
      <data key="d1">3734.0</data>
    </edge>
    <edge source="1" target="0">
      <data key="d0">200.0</data>
      <data key="d1">8305.0</data>
    </edge>
    <edge source="1" target="24">
      <data key="d0">200.0</data>
      <data key="d1">10119.0</data>
    </edge>
    <edge source="2" target="6">
      <data key="d0">200.0</data>
      <data key="d1">6341.0</data>
    </edge>
    <edge source="2" target="8">
      <data key="d0">200.0</data>
      <data key="d1">24302.0</data>
    </edge>
    <edge source="2" target="9">
      <data key="d0">200.0</data>
      <data key="d1">23059.0</data>
    </edge>
    <edge source="2" target="24">
      <data key="d0">200.0</data>
      <data key="d1">17141.0</data>
    </edge>
    <edge source="6" target="2">
      <data key="d0">200.0</data>
      <data key="d1">6341.0</data>
    </edge>
    <edge source="6" target="5">
      <data key="d0">200.0</data>
      <data key="d1">9940.0</data>
    </edge>
    <edge source="6" target="7">
      <data key="d0">200.0</data>
      <data key="d1">10790.0</data>
    </edge>
    <edge source="8" target="2">
      <data key="d0">200.0</data>
      <data key="d1">24302.0</data>
    </edge>
    <edge source="8" target="7">
      <data key="d0">200.0</data>
      <data key="d1">8521.0</data>
    </edge>
    <edge source="8" target="14">
      <data key="d0">200.0</data>
      <data key="d1">27961.0</data>
    </edge>
    <edge source="8" target="15">
      <data key="d0">200.0</data>
      <data key="d1">31800.0</data>
    </edge>
    <edge source="8" target="21">
      <data key="d0">200.0</data>
      <data key="d1">20402.0</data>
    </edge>
    <edge source="8" target="24">
      <data key="d0">200.0</data>
      <data key="d1">25221.0</data>
    </edge>
    <edge source="9" target="2">
      <data key="d0">200.0</data>
      <data key="d1">23059.0</data>
    </edge>
    <edge source="9" target="11">
      <data key="d0">200.0</data>
      <data key="d1">9714.0</data>
    </edge>
    <edge source="9" target="10">
      <data key="d0">200.0</data>
      <data key="d1">13706.0</data>
    </edge>
    <edge source="9" target="15">
      <data key="d0">200.0</data>
      <data key="d1">18466.0</data>
    </edge>
    <edge source="9" target="16">
      <data key="d0">200.0</data>
      <data key="d1">7217.0</data>
    </edge>
    <edge source="4" target="3">
      <data key="d0">200.0</data>
      <data key="d1">4310.0</data>
    </edge>
    <edge source="4" target="5">
      <data key="d0">200.0</data>
      <data key="d1">8458.0</data>
    </edge>
    <edge source="3" target="4">
      <data key="d0">200.0</data>
      <data key="d1">4310.0</data>
    </edge>
    <edge source="3" target="5">
      <data key="d0">200.0</data>
      <data key="d1">8040.0</data>
    </edge>
    <edge source="3" target="24">
      <data key="d0">200.0</data>
      <data key="d1">6003.0</data>
    </edge>
    <edge source="5" target="3">
      <data key="d0">200.0</data>
      <data key="d1">8040.0</data>
    </edge>
    <edge source="5" target="4">
      <data key="d0">200.0</data>
      <data key="d1">8458.0</data>
    </edge>
    <edge source="5" target="6">
      <data key="d0">200.0</data>
      <data key="d1">9940.0</data>
    </edge>
    <edge source="5" target="7">
      <data key="d0">200.0</data>
      <data key="d1">3551.0</data>
    </edge>
    <edge source="7" target="5">
      <data key="d0">200.0</data>
      <data key="d1">3551.0</data>
    </edge>
    <edge source="7" target="6">
      <data key="d0">200.0</data>
      <data key="d1">10790.0</data>
    </edge>
    <edge source="7" target="8">
      <data key="d0">200.0</data>
      <data key="d1">8521.0</data>
    </edge>
    <edge source="11" target="9">
      <data key="d0">200.0</data>
      <data key="d1">9714.0</data>
    </edge>
    <edge source="11" target="10">
      <data key="d0">200.0</data>
      <data key="d1">6801.0</data>
    </edge>
    <edge source="11" target="15">
      <data key="d0">200.0</data>
      <data key="d1">10198.0</data>
    </edge>
    <edge source="10" target="9">
      <data key="d0">200.0</data>
      <data key="d1">13706.0</data>
    </edge>
    <edge source="10" target="11">
      <data key="d0">200.0</data>
      <data key="d1">6801.0</data>
    </edge>
    <edge source="10" target="14">
      <data key="d0">200.0</data>
      <data key="d1">22265.0</data>
    </edge>
    <edge source="10" target="15">
      <data key="d0">200.0</data>
      <data key="d1">14300.0</data>
    </edge>
    <edge source="12" target="13">
      <data key="d0">200.0</data>
      <data key="d1">4710.0</data>
    </edge>
    <edge source="12" target="14">
      <data key="d0">200.0</data>
      <data key="d1">13485.0</data>
    </edge>
    <edge source="13" target="12">
      <data key="d0">200.0</data>
      <data key="d1">4710.0</data>
    </edge>
    <edge source="13" target="14">
      <data key="d0">200.0</data>
      <data key="d1">12115.0</data>
    </edge>
    <edge source="14" target="8">
      <data key="d0">200.0</data>
      <data key="d1">27961.0</data>
    </edge>
    <edge source="14" target="10">
      <data key="d0">200.0</data>
      <data key="d1">22265.0</data>
    </edge>
    <edge source="14" target="12">
      <data key="d0">200.0</data>
      <data key="d1">13485.0</data>
    </edge>
    <edge source="14" target="13">
      <data key="d0">200.0</data>
      <data key="d1">12115.0</data>
    </edge>
    <edge source="14" target="15">
      <data key="d0">200.0</data>
      <data key="d1">9140.0</data>
    </edge>
    <edge source="14" target="17">
      <data key="d0">200.0</data>
      <data key="d1">19825.0</data>
    </edge>
    <edge source="14" target="19">
      <data key="d0">200.0</data>
      <data key="d1">12183.0</data>
    </edge>
    <edge source="14" target="20">
      <data key="d0">200.0</data>
      <data key="d1">12563.0</data>
    </edge>
    <edge source="14" target="21">
      <data key="d0">200.0</data>
      <data key="d1">15815.0</data>
    </edge>
    <edge source="14" target="24">
      <data key="d0">200.0</data>
      <data key="d1">11709.0</data>
    </edge>
    <edge source="15" target="14">
      <data key="d0">200.0</data>
      <data key="d1">9140.0</data>
    </edge>
    <edge source="15" target="8">
      <data key="d0">200.0</data>
      <data key="d1">31800.0</data>
    </edge>
    <edge source="15" target="9">
      <data key="d0">200.0</data>
      <data key="d1">18466.0</data>
    </edge>
    <edge source="15" target="10">
      <data key="d0">200.0</data>
      <data key="d1">14300.0</data>
    </edge>
    <edge source="15" target="11">
      <data key="d0">200.0</data>
      <data key="d1">10198.0</data>
    </edge>
    <edge source="15" target="16">
      <data key="d0">200.0</data>
      <data key="d1">14077.0</data>
    </edge>
    <edge source="17" target="14">
      <data key="d0">200.0</data>
      <data key="d1">19825.0</data>
    </edge>
    <edge source="17" target="18">
      <data key="d0">200.0</data>
      <data key="d1">6621.0</data>
    </edge>
    <edge source="19" target="14">
      <data key="d0">200.0</data>
      <data key="d1">12183.0</data>
    </edge>
    <edge source="19" target="18">
      <data key="d0">200.0</data>
      <data key="d1">15069.0</data>
    </edge>
    <edge source="19" target="20">
      <data key="d0">200.0</data>
      <data key="d1">7350.0</data>
    </edge>
    <edge source="19" target="23">
      <data key="d0">200.0</data>
      <data key="d1">9209.0</data>
    </edge>
    <edge source="19" target="21">
      <data key="d0">200.0</data>
      <data key="d1">24575.0</data>
    </edge>
    <edge source="20" target="14">
      <data key="d0">200.0</data>
      <data key="d1">12563.0</data>
    </edge>
    <edge source="20" target="19">
      <data key="d0">200.0</data>
      <data key="d1">7350.0</data>
    </edge>
    <edge source="21" target="14">
      <data key="d0">200.0</data>
      <data key="d1">15815.0</data>
    </edge>
    <edge source="21" target="8">
      <data key="d0">200.0</data>
      <data key="d1">20402.0</data>
    </edge>
    <edge source="21" target="19">
      <data key="d0">200.0</data>
      <data key="d1">24575.0</data>
    </edge>
    <edge source="21" target="22">
      <data key="d0">200.0</data>
      <data key="d1">8515.0</data>
    </edge>
    <edge source="21" target="23">
      <data key="d0">200.0</data>
      <data key="d1">16058.0</data>
    </edge>
    <edge source="24" target="14">
      <data key="d0">200.0</data>
      <data key="d1">11709.0</data>
    </edge>
    <edge source="24" target="0">
      <data key="d0">200.0</data>
      <data key="d1">3734.0</data>
    </edge>
    <edge source="24" target="1">
      <data key="d0">200.0</data>
      <data key="d1">10119.0</data>
    </edge>
    <edge source="24" target="2">
      <data key="d0">200.0</data>
      <data key="d1">17141.0</data>
    </edge>
    <edge source="24" target="3">
      <data key="d0">200.0</data>
      <data key="d1">6003.0</data>
    </edge>
    <edge source="24" target="8">
      <data key="d0">200.0</data>
      <data key="d1">25221.0</data>
    </edge>
    <edge source="16" target="9">
      <data key="d0">200.0</data>
      <data key="d1">7217.0</data>
    </edge>
    <edge source="16" target="15">
      <data key="d0">200.0</data>
      <data key="d1">14077.0</data>
    </edge>
    <edge source="18" target="17">
      <data key="d0">200.0</data>
      <data key="d1">6621.0</data>
    </edge>
    <edge source="18" target="19">
      <data key="d0">200.0</data>
      <data key="d1">15069.0</data>
    </edge>
    <edge source="23" target="19">
      <data key="d0">200.0</data>
      <data key="d1">9209.0</data>
    </edge>
    <edge source="23" target="21">
      <data key="d0">200.0</data>
      <data key="d1">16058.0</data>
    </edge>
    <edge source="23" target="22">
      <data key="d0">200.0</data>
      <data key="d1">9009.0</data>
    </edge>
    <edge source="22" target="21">
      <data key="d0">200.0</data>
      <data key="d1">8515.0</data>
    </edge>
    <edge source="22" target="23">
      <data key="d0">200.0</data>
      <data key="d1">9009.0</data>
    </edge>
  </graph>
</graphml>
