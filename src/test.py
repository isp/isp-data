import argparse
import utils

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check if an instance is valid')
    parser.add_argument('instances', metavar='I', type=str, nargs='+',
                        help='')

    args = parser.parse_args()
    for instance in args.instances:
        utils.check_instance(instance)