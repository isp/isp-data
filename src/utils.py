import json
import networkx as nx
import pandas as pd
import csv

def load_network(instance):
    with open(f"../json/{instance}_topo.json") as fd:
        topo = json.load(fd)
        return nx.from_edgelist([(int(edge['src']), int(edge['dst']), {'id': int(edge['e']), 'delay': float(edge['delay']), 'loss': float(edge['loss'])}) for edge in topo['edges']],
                                create_using=nx.DiGraph)

def load_demand_graph(filename):
    with open(filename) as fd:
        demands = json.load(fd)
        return nx.from_edgelist([(int(edge['src']), int(edge['dst']), {'id': i, 'delay': float(edge['delay']), 'loss': float(edge['loss'])}) for i, edge in enumerate(demands['edges'])],
                                create_using=nx.MultiDiGraph)
    
def get_path_property(topo, path, prop):
    '''
    Given a path as a list of nodes and a topo, return the sum of the property over the path
    '''
    return sum([topo[u][v][prop] for u, v in zip(path[:-1], path[1:])])
    
def get_paths_from_greedy(instance, demands, algorithm='greedy_200'):
    topo = load_network(instance)
    paths = {}
    with open(f"../results/{instance}_{algorithm}.json") as fd:
        results = json.load(fd)

        weights = {}
        topo_ids = {}
        for topo_id, mtr_topo in enumerate(results['topologies']):
            for demand_id in mtr_topo['demands']:
                weights[demand_id] = mtr_topo['weights']
                topo_ids[demand_id] = topo_id
                
        for src, dst, dmd_prop in demands.edges(data=True):
            dmd_weights = weights[dmd_prop['id']]
            path = nx.shortest_path(topo, src, dst, weight= lambda u, v, prop: dmd_weights[prop['id']])
            paths[dmd_prop['id']] = path, topo_id
    return paths

def reorder_nodes(g):
    new_graph = nx.DiGraph()
    new_graph.add_nodes_from(range(g.order()))
    new_graph.add_edges_from([(u, v, attrs) for u, v, attrs in g.edges(data=True)])
    return new_graph
    
def convert_instance_v1(name: str):
    topo_df = pd.read_csv(f'{name}.txt', delimiter=' ', header=None, index_col=False, skiprows=1, names=['src', 'dst', 'ignore1', 'delay', 'ignore2', 'loss']).query('delay.notna()')
    topo_df['src'] = topo_df['src'].astype(int)
    topo_df['dst'] = topo_df['dst'].astype(int)
    topo_df['delay'] *= 100.0
    topo_df['cost'] = topo_df['cost'].astype(float)
    #display(topo_df)
    demands_df = pd.read_csv(f'{name}.txtdemand.csv', delimiter=',', index_col=False, header=None, names=['src', 'dst', 'delay', 'loss'])
    demands_df['src'] = topo_df['src'].astype(int)
    demands_df['dst'] = topo_df['dst'].astype(int)
    demands_df['delay'] *= 1.0
    demands_df['cost'] = demands_df['cost'].astype(float)
    #display(demands_df)
    weighted_demands_df = demands_df.copy()
    weighted_demands_df['weight'] = 1.0
    display(weighted_demands_df)
    
    
    for idx, (src, dst, delay, loss, path) in demands_df.iterrows():
        p = [int(e) for e in path.split('|') if e != '']
        assert sum(topo_df.iloc[e]['delay'] for e in p) <= delay
        assert sum(topo_df.iloc[e]['loss'] for e in p) <= loss

    topo = nx.from_pandas_edgelist(topo_df, source='src', target='dst', edge_attr=['delay', 'loss'])
    nx.write_graphml(nx.DiGraph(topo), f'{name}.gml')
    demands = nx.from_pandas_edgelist(demands_df, source='src', target='dst', edge_attr=['delay', 'loss'], create_using=nx.DiGraph)
    nx.write_graphml(demands, f'{name}.dml')
    weighted_demands = nx.from_pandas_edgelist(weighted_demands_df, source='src', target='dst', edge_attr=['cost', 'delay', 'weight'], create_using=nx.DiGraph)
    nx.write_graphml(weighted_demands, f'{name}.wdml', infer_numeric_types=False)
    
def convert_instance_v2(name: str):
    topo_df = pd.read_csv(f'{name}.txtLink.csv', delimiter=',', header=None, index_col=False, names=['src', 'dst', 'delay', 'loss']).query('delay.notna()')
    topo_df['delay'] *= 1.0
    topo_df['loss'] *= 1.0
    #display(topo_df)
    
    topo = nx.from_pandas_edgelist(topo_df, source='src', target='dst', edge_attr=['delay', 'loss'])
    #for u, v, attrs in topo.edges(data=True):
    #    for attr, val in attrs.items():
    #        g[u, v][attr] = val
    nx.write_graphml(reorder_nodes(nx.DiGraph(topo)), f'{name}.gml')
    
    demands_df = pd.read_csv(f'{name}.txtDemand.csv', delimiter=',', index_col=False, header=None, names=['src', 'dst', 'delay', 'loss', 'path'])
    demands_df['delay'] *= 1.0
    demands_df['loss'] *= 1.0
    demands_df['weight'] = 1.0
    #display(demands_df)
    demands = nx.from_pandas_edgelist(demands_df, source='src', target='dst', edge_attr=['delay', 'loss', 'weight'], create_using=nx.DiGraph)
    nx.write_graphml(nx.DiGraph(reorder_nodes(demands)), f'{name}.wdml')

def load_csv_demands(filename: str):
    demands_df = pd.read_csv(filename, delimiter=',', index_col=False, header=None, names=['src', 'dst', 'delay', 'loss', 'path'])
    demands_df['delay'] = demands_df['delay'].astype(float)
    demands_df['loss'] = demands_df['loss'].astype(float)
    demands_df['weight'] = 1.0
    return demands_df

def load_csv_topo(filename: str):
    topo_df = pd.read_csv(filename, delimiter=',', header=None, index_col=False, names=['src', 'dst', 'delay', 'loss'])
    topo_df['delay'] = topo_df['delay'].astype(float)
    topo_df['loss'] = topo_df['loss'].astype(float)
    return topo_df

def load_csv_demands_without_path(filename: str):
    demands_df = pd.read_csv(filename, delimiter=',', index_col=False, header=None, names=['src', 'dst', 'delay', 'loss'])
    demands_df['delay'] = demands_df['delay'].astype(float)
    demands_df['loss'] = demands_df['loss'].astype(float)
    demands_df['weight'] = 1.0
    return demands_df

def check_paths(topo_df, raw_topo_df, demands_df):
    for index, (src, dst, delay, loss, path, weight) in demands_df.iterrows():
        def check_path(path):
            if raw_topo_df.iloc[path[0]]['src'] != src:
                return False, ((index, f"_{src}_", dst, delay, loss, path, weight), raw_topo_df.iloc[path[0]]['src'])
            if raw_topo_df.iloc[path[-1]]['dst'] != dst:
                return False, ((index, src, f"_{dst}_", delay, loss, path, weight), raw_topo_df.iloc[path[-1]]['dst'], )
            
            path_delay = sum(raw_topo_df.iloc[e]['delay'] for e in path)
            path_delay = sum([topo_df.query(f'src == {raw_topo_df.iloc[e]["src"]} and dst == {raw_topo_df.iloc[e]["dst"]}')['delay'].values[0] for e in path])
            if path_delay > delay:
                return False, ((index,src, dst, delay, loss, f"_{delay}_", weight), path_delay)

            path_loss = sum(raw_topo_df.iloc[e]['loss'] for e in path)
            path_loss = sum([topo_df.query(f'src == {raw_topo_df.iloc[e]["src"]} and dst == {raw_topo_df.iloc[e]["dst"]}')['loss'].values[0] for e in path])
            if path_loss > loss:
                return False, ((index, src, dst, delay, loss, f"_{delay}_", weight), path_loss,)
            return True, ()
        
        path = [int(e) for e in path.split('|')[:-1]]
        valid, args =check_path(path)
        assert valid, f"Invalid path for {args}"
        
        edges = {raw_topo_df.iloc[e]['src']: raw_topo_df.iloc[e]['dst'] for e in path}
        for u, v in edges.items():
            assert v in list(edges.keys()) + [dst]
        
def convert_instance_json(name, topo_df, demands_df, prefix: str='../json/'): 
    raw_topo_df = topo_df.copy()
    topo_df = topo_df.iloc[topo_df[['src', 'dst']].index].drop_duplicates().reset_index(drop=True)
    print(f"Loaded {len(raw_topo_df)} links")
    print(f"After droping duplicates: {len(topo_df)} links")

    instance = {'topology': 
                    {'edges': [{'e': index, 'src': int(src), 'dst': int(dst), 'delay': delay, 'loss': loss} for index, (src, dst, delay, loss) in topo_df.iterrows()]},
               'demands': 
                    {'edges': [{'src': int(src), 'dst': int(dst), 'delay': delay, 'loss': loss, 'weight': weight} for index, (src, dst, delay, loss, weight) in demands_df[['src', 'dst', 'delay', 'loss', 'weight']].iterrows()]}
              }
    json.dump(instance, open(f'{prefix}/wdm/{name}.json', 'w'))
    

def convert_vipg_csv(instance):
    filename = f'../results/{instance}.txt_lambda.csv'
    retval = {'topologies': []}
    with open(filename) as fd:
        reader = csv.reader(fd, delimiter=',')
        next(reader)
        for vtopo in reader:
            retval['topologies'].append({'lambda' : float(vtopo[0]), 'lambdaMin' : float(vtopo[1]), 'demands': [int(d) for d in vtopo[2:-1]]})
            
    filename = f'../results/{instance}.txt_lambdaRobuste.csv'
    with open(filename) as fd:
        reader = csv.reader(fd, delimiter=',')
        next(reader)
        for vtopo, stats in zip(reader, retval['topologies']):
            stats['lambdaMin'] = float(vtopo[0])
            stats['lambdaMiddle'] = float(vtopo[3])
            stats['lambdaMax'] = float(vtopo[6])
    return retval

def get_paths_from_vigp(instance, demands, lambda_value='lambda'):
    topo = load_network(instance)
    virtual_topos = convert_vipg_csv(instance)

    lambdas = {demand_id: vmtr_topo[lambda_value] for vmtr_topo in virtual_topos['topologies'] for demand_id in vmtr_topo['demands']}
    vtopo_ids = {demand_id: f"v{vtopo_id}" for vtopo_id, vmtr_topo in enumerate(virtual_topos['topologies']) for demand_id in vmtr_topo['demands']}
    
    paths = {}        
    for src, dst, demand_property in demands.edges(data=True):
        if demand_property['id'] not in lambdas.keys():
            continue
        path = nx.shortest_path(topo, src, dst, weight= lambda u, v, edge_property: edge_property['delay'] + lambdas[demand_property['id']] * edge_property['loss'])
        paths[demand_property['id']] = (path, vtopo_ids[demand_property['id']])
    return paths

def compare_paths(instance, demands, sets_of_paths):
    topo = load_network(instance)
    
    dfs = []
    for algorithm, paths in sets_of_paths.items():
        df = pd.DataFrame(data=[(demand_id, get_path_property(topo, path, 'delay'), get_path_property(topo, path, 'loss'), topo_id) for demand_id, (path, topo_id) in paths.items()], columns=['demand_id', 'delay', 'loss', 'topo_id'])
        df['algo'] = algorithm
        dfs.append(df)

    df = pd.concat(dfs)
    df['instance'] = instance
    return df

def get_vipged_path(instance, lambda_value='lambda'):
    vigped_demands = json.load(open(f"../json/vigped/{instance}.json"))
    greedy_paths = get_paths_from_greedy(instance, load_demand_graph(f"../json/vigped/{instance}.json"), "vIGP_greedy")
    paths = get_paths_from_vigp(instance, load_demand_graph(f"../json/{instance}_wdm.json"), lambda_value)
    for i, edge in enumerate(vigped_demands['edges']):
        paths[edge['original_id']] = greedy_paths[i]
    return paths


def get_vigp_stats(instance):
    '''
    Given an instance, load the solutions from vIGP and MTR and return number of real and virtual topologies deployed
    @param Name of the instance
    '''
    vigp = json.load(open(f'../results/{instance}_vIGP.json'))
    vigped = json.load(open(f'../results/{instance}_vIGP_greedy.json'))
    return pd.Series({
        'instance': instance,
        'algorithm': 'vIGP',
        'nb_virtual_topologies': len(vigp['topologies']),
        'nb_real_topologies': len(vigped['topologies']),
        'nb_total_topologies': len(vigp['topologies']) + len(vigped['topologies']),
        'execution_time': vigped['runningTime'] + vigped['vigp_time'],
        'vigp_time':vigped['vigp_time'],        
        'nb_demands_virtual': [len(topo['demands']) for topo in vigp['topologies']],
        'nb_demands_real': [len(topo['demands']) for topo in vigped['topologies']]
    })

def get_greedy_stats(instance, greedy_type = "greedy_200"):
    '''
    Given an instance, load the solution from MTR and return the number of real topologies deployed
    '''
    greedy = json.load(open(f'../results/{instance}_{greedy_type}.json'))
    return pd.Series({
        'instance': instance,
        'algorithm': 'Greedy',
        'nb_virtual_topologies': 0,
        'nb_real_topologies': len(greedy['topologies']),
        'nb_total_topologies': len(greedy['topologies']),
        'execution_time': greedy['runningTime'],
        'nb_demands_real': [len(topo['demands']) for topo in greedy['topologies']]
    })