all: results/vigp.csv results/vsr.csv results/sr.csv results/sr_larac.csv

results/vigp.csv: results/AllResultsVSR.csv
	awk -F , '{split($$1,instance,"/"); split(instance[2], name, "."); print name[1]","$$6","$$7","$$8}' $< > $@
results/vsr.csv: results/AllResultsVSR.csv
	awk -F , '{split($$1,instance,"/"); split(instance[2], name, "."); print name[1]","$$20","$$21","$$22","$$23","$$24","$$25}' $< > $@
	sed -i "1 s/.*/instance,number_of_label,num_topo,execution_time,number_of_demands,max_label,??/" $@
results/sr.csv: results/AllResultsVSR.csv
	awk -F , '{split($$1,instance,"/"); split(instance[2], name, "."); print name[1]","$$11","$$12","$$13","$$14}' $< > $@
	sed -i "1 s/.*/instance,number_of_label,execution_time,number_of_demands,max_label/" $@
results/sr_larac.csv: results/AllResultsVSR.csv
	awk -F , '{split($$1,instance,"/"); split(instance[2], name, "."); print name[1]","$$16","$$17","$$18}' $< > $@
	sed -i "1 s/.*/instance,number_of_label,execution_time,number_of_demands/" $@
